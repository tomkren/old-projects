/* THIS FILE WILL BE OVERWRITTEN BY DEV-C++ */
/* DO NOT EDIT ! */

#ifndef FLUMEN_PRIVATE_H
#define FLUMEN_PRIVATE_H

/* VERSION DEFINITIONS */
#define VER_STRING	"0.1.1.1"
#define VER_MAJOR	0
#define VER_MINOR	1
#define VER_RELEASE	1
#define VER_BUILD	1
#define COMPANY_NAME	""
#define FILE_VERSION	"0.1"
#define FILE_DESCRIPTION	"Developed using the Dev-C++ IDE"
#define INTERNAL_NAME	""
#define LEGAL_COPYRIGHT	""
#define LEGAL_TRADEMARKS	""
#define ORIGINAL_FILENAME	"Lesson7.exe"
#define PRODUCT_NAME	"Lesson7"
#define PRODUCT_VERSION	"0.1"

#endif /*FLUMEN_PRIVATE_H*/
