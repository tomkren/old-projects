#include "main_h.h"


Vektor::Vektor()
{
    x = 0;
    y = 0;
    z = 0;
}

Vektor::Vektor(num xval, num yval, num zval)
{
    x = xval;
    y = yval;
    z = zval;
}

Vektor::~Vektor() {}

Vektor Vektor::operator+(const Vektor& b) const
{
    Vektor c;
    c.x = x + b.x;
    c.y = y + b.y;
    c.z = z + b.z;
    return c;
    
}

num Vektor::abs() const
{
    return sqrt( x*x + y*y + z*z );
}

//vektorov� sou�in
//  x,  y,  z
//b.x,b.y,b.z
Vektor Vektor::operator*(const Vektor& b) const
{
    return Vektor( y*b.z - z*b.y , -x*b.z + z*b.x, x*b.y - y*b.x);
}

//skalarni soucin
num Vektor::operator&(const Vektor& b) const
{
    return  x*b.x + y*b.y + z*b.z;
}


Vektor Vektor::operator*(num numb) const
{
    return Vektor(numb*x, numb*y, numb*z);
}

Vektor Vektor::operator-(const Vektor& b) const
{
    Vektor c;
    c.x = x - b.x;
    c.y = y - b.y;
    c.z = z - b.z;
    return c;
    
}

Vektor Vektor::operator-() const
{
    return Vektor(-x,-y,-z);
}

Vektor& Vektor::SetByBod(const Bod& A)
{
    x = A.xval();
    y = A.yval();
    z = A.zval();
    
    return *this;
}

bool Vektor::LinNez(const Vektor& u) const
{
    num k;
    bool all_0 = false;
    k = (x!=0)?(u.x / x):((y!=0)?(u.y / y):(z!=0)?(u.z / z):(all_0=true));
    //cout << ABS(k*x - u.x) <<endl<< ABS(k*y - u.y) << endl << ABS(k*z - u.z) << endl;
    
    if(all_0)
        return (u.x == 0 && u.y == 0 && u.z == 0)?false:true;
    
    return(ABS(k*x - u.x) <= 1E-10 && ABS(k*y - u.y) <= 1E-10 && ABS(k*z - u.z) <= 1E-10)?false:true;
    
    
    
}


Vektor operator*(num numb, const Vektor& a)
{
    return a*numb;
}


Bod operator+(const Bod& A,const Vektor& u)
{
    Bod C(A.xval() + u.x,A.yval() + u.y,A.zval() + u.z);

    return C;
}
Bod operator+(const Vektor& u,const Bod& A)
{
    return A+u;
}

ostream& operator<<(ostream& os_obj , const Vektor& a)
{
    os_obj << '('  << a.x << ','  << a.y << ',' << a.z << ')' ;
    return os_obj;
}
