#ifndef _MAIN_H_H_
#define _MAIN_H_H_

typedef float num;

#include <iostream>
#include <vector>
#include <math.h>
#include <stdlib.h>
#include <time.h>

using namespace std;

#include "fps.h"

#include <windows.h>		// Header File For Windows
#include <stdio.h>			// Header File For Standard Input/Output
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library
#include <gl\glaux.h>		// Header File For The Glaux Library
#include <fstream>

#include "resource.h"

#include "bod.h"
#include "vektor.h"

LRESULT	CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);	
GLvoid KillGLWindow(GLvoid);
int InitGL(GLvoid);
GLvoid ReSizeGLScene(GLsizei width, GLsizei height);
int LoadGLTextures();
AUX_RGBImageRec* LoadBMP(char *Filename);
BOOL CreateGLWindow(char* title, int width, int height, int bits, bool fullscreenflag);



INT_PTR CALLBACK DialogProc(HWND hwndDlg,UINT uMsg,WPARAM wParam,LPARAM lParam);
void initDialog();

void vypocet();
int DrawGLScene(GLvoid);
void initKapky();
void closeKapky();

struct Kapka
{
    Bod     X;
    Vektor  v;
};

struct Bunka
{
    vector<Kapka>   kapky;
    Bod             stred;  //aby se pozce v kazdem taktu nemusela prepocitavat
    int             pos[3];    //uklada x,y,z pozici bunky
    
};



const short HRANY[] = 
{
     1, 1, 1, 1, 1,-1, 1, 1, 1,
     1,-1, 1, 1, 1, 1,-1, 1, 1,
    -1,-1,-1,-1,-1, 1,-1,-1,-1,
    -1, 1,-1,-1,-1,-1, 1,-1,-1,
    -1, 1, 1,-1, 1,-1,-1, 1, 1,
    -1,-1, 1, 1,-1,-1, 1,-1, 1,
     1,-1,-1, 1, 1,-1,-1,-1, 1,
     1,-1, 1, 1, 1,-1,-1, 1,-1
};

inline num ABS(double x) {return (x>=0)?x:(-x);}
inline int SGN(double x) {return (x>0)?1:(x<0?-1:0);}

//vygeneruje cislo od -1 do 1
inline num RAND1_1() {return (num(rand())/RAND_MAX)*((rand()%2)*2-1);}

#endif
