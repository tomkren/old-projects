#include "main_h.h"

void   presunyNapricBkamy   (int b);
void   posunKapku           (int b, int i);
void   urychliKapku         (int b, int i, Vektor a);
Kapka  Rkapka               (Bod pos,Vektor v);
Bunka& bka                  (int x, int y, int z);
int    bkaNum               (int x, int y, int z);
void   fyzikaBunky          (int b);
void   fyzika_bky_by_bka    (int b1,int b2);
void   proud                (Bod X , Vektor v , num ro , int d , int T);
void   kolize               (int b);


int  initNkapek = 3;
int NbRow = 13;

bool iOkoli = false;
bool randomPocatek = false;

num G = 0.000001;//1E-5;
num g =1E-5; //1000;
num odrazovka = 5E-2;

int Nbunek = NbRow * NbRow * NbRow;
num Rbky = 1.0/NbRow;

Bunka* bunky;
vector<Kapka> kapky;

int takt = 0;

num pv[18] =
{
    -0.8, 0 , 1 , 3E-3 , 1E-3 , -5E-3 , 0.03 , 2 ,10,
     0.8, 0, 1 , -3E-3 , 1E-3 , -5E-3 , 0.03 , 2 ,10    
};

void proud_krs_pv(int k)
{
    k = k * 9;
    
    proud(Bod( pv[k+0], pv[k+1] , pv[k+2] ),Vektor( pv[k+3] , pv[k+4] , pv[k+5] ), pv[k+6] , int(pv[k+7]) , int(pv[k+8]) );
}

void vypocet()
{
    //p1_x, p1_y , p1_z , p1_vx , p1_vy , p1_vz , p1_ro , p1_d , p1_T  
        
    
    proud_krs_pv(0);
    proud_krs_pv(1);
    
    //proud(Bod(  0 , 0,-1 ),Vektor( 0,1E-3, 5E-3),0.03, 2 ,6);
    //proud(Bod(  0 , 0,-1 ),Vektor( 0,0E-3, 4E-3),0.02, 2  ,12);
    //proud(Bod(  0 , 0, 1 ),Vektor( 0,0E-3,-4E-3),0.01, 2 , 5);
    
    
    for (int b = 0; b < Nbunek; b++)
    {
        kolize(b);
        fyzikaBunky(b);
        presunyNapricBkamy(b);  
    }
    
    takt ++ ;
}

//vlo�� do sc�ny kapi�ky pro proudu 
//pozice X, prumer d , rychlost castice v, ro - hustota/rozestup , pribide kazdych T-t� takt 
void proud(Bod X , Vektor v , num ro , int d , int T)
{
    if ( !(takt%T) )
    {
        for (int i = 0 ; i < d ; i++)
        {
            for (int j = 0 ; j < d ; j++)
            {
                Kapka temp = Rkapka(X + Bod(i*ro , j * ro, 0 ) , v);
                bunky[0].kapky.push_back(temp);
            }
        }
    }
}
    

void presunyNapricBkamy(int b)
{
    Bunka* pb = &(bunky[b]);
    
    //nyn� zjist�me kter� kapky z bu�ky vylet�li a
    // a) p�eregistrujeme je do jin� bu�ky
    // b) pokud vylet�li ze simulacniho prostoru, tak je sma�em
    
    int Nkapek = bunky[b].kapky.size();
    
    
    vector<Kapka>::iterator ik = bunky[b].kapky.begin();
    
    while( ik != bunky[b].kapky.end() ) 
    {
        
        int vedle[3]; //relativn� pozice nov� bu�ky
        
        Bod odStredu = ik->X - pb->stred;
        odStredu.vydel(Rbky);
        
        bool potrebaPresunu = false;
        
        for (int i=0; i<3; i++)
        {
            vedle[i] = int( SGN(odStredu.s(i)) * (ABS(odStredu.s(i))+1.0) / 2.0  );
            
            if (ABS(vedle[i]) > 0) 
            {
                potrebaPresunu = true;
            }
        }
        
        //pokud je potreba presouvat , presune bud do jine bky, nebo smazeme
        if(potrebaPresunu)
        {
            //zkontrolujeme zda se nachazi v simulacni oblasti
            //a rovnou spocitame pozici nove bunky
            
            int newPos[3];
            bool naMape = true;
            
            for (int i=0; i<3; i++)
            {
                int pozice = pb->pos[i] + vedle[i];
                
                if ( pozice < 0 || pozice >= NbRow )
                {
                    naMape = false;
                    break;
                }
                
                newPos[i] = pozice;
            }
            
            if (naMape)
            {
                //presuneme kapku do jine bunky
                
                Kapka temp = *ik;
                pb->kapky.erase(ik);
                ik--;
                
                bka(newPos[0],newPos[1],newPos[2]).kapky.push_back(temp);                                            
            }
            else
            {
                //kapku smazeme
                
                pb->kapky.erase(ik); 
                ik --;             
            }
        }
        
        ik ++ ;    
    }  
}

void kolize(int b)
{
    Bunka* pb = &(bunky[b]);
    int Nkapek = pb->kapky.size();
    
    //pro ka�dou dvojici kapek v bu�ce prav� jednou
    //  1 2 3 
    //1   x x 
    //2     x 
    //3       
    
    for (int i1 = 0 ; i1 < Nkapek-1 ; i1 ++ )
    {
        for (int i2 = i1+1 ; i2 < Nkapek ; i2 ++ )
        {
            
            Kapka* pk1 = &(pb->kapky[i1]);
            Kapka* pk2 = &(pb->kapky[i2]);
            
            //spocteme vektor vzd�lenosti r
            
            Vektor r;
            r.SetByBod( (pk2->X) - (pk1->X) ); 
            
            num abs_r = r.abs();
            
            if (abs_r < odrazovka)
            {
                Vektor r_jednot  = r * (1.0/abs_r);
                
                //x-ov� nabivaj i zapornejch hodnot, tak jak maj
                
                num x1 = (   r  & pk1->v ) / abs_r;
                num x2 = ( (-r) & pk2->v ) / abs_r;
                
                // X = x1 - x2
                Vektor X = ( r_jednot * x1 ) + ( r_jednot * x2 );  // tady + opravnene
                
                pk1->v = pk1->v - X;
                pk2->v = pk2->v + X;
            }
            
        
            
                       
        }
    }
}

void fyzikaBunky(int b)
{ 
    if(!iOkoli)
    {
        fyzika_bky_by_bka(b,b);
        return;
    }
      
    int* p = bunky[b].pos;
    
    int d[3] , new_p[3];
    
    for(d[0] = -1; d[0] <= 1 ; d[0]++)
    for(d[1] = -1; d[1] <= 1 ; d[1]++)
    for(d[2] = -1; d[2] <= 1 ; d[2]++)
    {
        bool vhodnaBka = true;
        
        for (int q = 0 ; q < 3 ; q++)
        {
            int temp = p[q] + d[q];
            
            if (temp < 0 || temp >= NbRow)
            {
                vhodnaBka = false;
                break;
            } 
            new_p[q] = temp;
        }
        
        if (vhodnaBka)
            fyzika_bky_by_bka(b, bkaNum(new_p[0],new_p[1],new_p[2]) );
    }

}

void fyzika_bky_by_bka(int b1 , int b2 )
{
    Bunka* pb1 = &(bunky[b1]);
    Bunka* pb2 = &(bunky[b2]);
    
    int Nkapek1 = pb1->kapky.size();
    int Nkapek2 = pb2->kapky.size();
    
    //pro ka�dou kapku z BUNKY1 spo�teme jej� nov� zrychlen� a pomoci kapek z BUNKY2
    for (int i1 = 0 ; i1 < Nkapek1 ; i1++)
    {
        Vektor a(0,0,0);
        
        //projdeme v�echny kapky z mno�iny nejbli���ch bunek
                  
        for (int i2 = 0 ; i2 < Nkapek2 ; i2++)
        {
            if ( !(i1==i2 && b1==b2) ) //kapka neinterreaguje sama se sebou --potom predelat mo�na
            {
                Kapka* pk1 = &(pb1->kapky[i1]);
                Kapka* pk2 = &(pb2->kapky[i2]);
                
                Vektor temp_a;
                
                temp_a.SetByBod( pk2->X - pk1->X );
                
                num r = temp_a.abs();
                
                temp_a.vynasob(G/r*r*r);
                
                a.pricti(temp_a);
            }
        }
       
        
        urychliKapku(b1,i1,Vektor(0.,-g,0.)); //gravitace
        urychliKapku(b1,i1,a);
        posunKapku(b1,i1);
    }
}


void urychliKapku(int b, int i, Vektor a)
{
    Kapka* pk = &(bunky[b].kapky[i]);
    
    num newvx = pk->v.xval() + a.xval();
    num newvy = pk->v.yval() + a.yval();
    num newvz = pk->v.zval() + a.zval();
    
    pk->v.set(newvx,newvy,newvz);    
}


void posunKapku(int b, int i)
{
    Kapka* pk = &(bunky[b].kapky[i]);
    
    num newx = pk->X.xval() + pk->v.xval();
    num newy = pk->X.yval() + pk->v.yval();
    num newz = pk->X.zval() + pk->v.zval();
    
    pk->X.set(newx,newy,newz);    
}

Kapka Rkapka(Bod pos,Vektor v)
{
    Kapka K;
    K.X = pos;
    K.v = v;
    
    return K;
}

int bkaNum(int x, int y, int z) {return z*NbRow*NbRow + y*NbRow + x;} 
Bunka& bka(int x, int y, int z){return bunky[bkaNum(x,y,z)];}


//!!!
void initKapky()
{
    Nbunek = NbRow * NbRow * NbRow;
    Rbky   = 1.0/NbRow;
    
    bunky = new Bunka[Nbunek];
    
    for (int x = 0; x < NbRow ; x++)
    for (int y = 0; y < NbRow ; y++)
    for (int z = 0; z < NbRow ; z++)
    {
        bka(x,y,z).stred.set(Rbky*(2*x+1) -1 , Rbky*(2*y+1) -1 , Rbky*(2*z+1) -1 );
        
        bka(x,y,z).pos[0] = x;
        bka(x,y,z).pos[1] = y;
        bka(x,y,z).pos[2] = z;
        
    }
    
    if (randomPocatek)
    {
        srand(time(0));
        
        for (int b = 0; b < Nbunek ; b++)
        {
            for (int i=0 ; i < initNkapek ; i++)
            {
                Bod pos = bunky[b].stred + Bod(Rbky*RAND1_1() , Rbky*RAND1_1() , Rbky*RAND1_1());
                
                Kapka temp = Rkapka(pos,Vektor(0,0,0));
                
        
                bunky[b].kapky.push_back(temp); //[0] je humus!!!
            }
        }
    }
}

void closeKapky()  //v soucasnosti volano na konci mainu()
{
    delete [] bunky;
}
