#include "main_h.h"

void FPS::Vypocet()
{
    //const int MODC = 100;
    citac++;
    
    if (!(citac%MODC)) //p�i ka�dym MODC-tym pruchodu se spo�te fps
    {
        new_cas = GetTickCount();
        Dtime = new_cas - old_cas;
        
        fps = MODC * 1000.0 / Dtime;
        
        old_cas = new_cas;
        citac = 0;
    }
}
