#include "main_h.h"

Bod::Bod()
{
    x = 0;
    y = 0;
    z = 0;
}

Bod::Bod(num xval, num yval, num zval)
{
    x = xval;
    y = yval;
    z = zval;
}

Bod::~Bod() {}

Bod Bod::operator+(const Bod& b) const
{
    Bod c;
    c.x = x + b.x;
    c.y = y + b.y;
    c.z = z + b.z;
    return c;
    
}


Bod Bod::operator-(const Bod& b) const
{
    Bod c;
    c.x = x - b.x;
    c.y = y - b.y;
    c.z = z - b.z;
    return c;   
}

Bod Bod::operator-() const
{
    return Bod(-x,-y,-z);
}

num vzdalenost(const Bod& a,const Bod& b)
{
    return sqrt( pow(a.x-b.x,2.) + pow(a.y-b.y,2.) + pow(a.z-b.z,2.) );
}

ostream& operator<<(ostream& os_obj , const Bod& A)
{
    os_obj << '['  << A.x << ','  << A.y << ',' << A.z << ']' ;
    return os_obj;
}


