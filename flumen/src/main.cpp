/*
 *	FLUMEN by Tom� K�en
 */
 
#include "main_h.h"

char nadpis[] = "FlumeN by TOM�� K�EN (stiskn�te ENTER pro OVL�DAC� PANEL & n�pov�du)";

ofstream fout_t;
ofstream fout_save;
ifstream fin_load;

ofstream fout_Nsavu;
ifstream  fin_Nsavu;



HDC			hDC=NULL;		
HGLRC		hRC=NULL;		
HWND		hWnd=NULL;		
HINSTANCE	hInstance;		

bool	keys[256];			
bool	active=TRUE;		
bool	fullscreen=TRUE;	
bool	light;				
bool	lp;					
bool	fp;					

GLfloat	xrot = 0 ;				
GLfloat	yrot = 0 ;				
GLfloat	zrot = 0 ;				

GLfloat xspeed = 0;				
GLfloat yspeed = 0;				
GLfloat zspeed = 0;				

GLfloat x = 0;
GLfloat y = 0;
GLfloat	z=-5.0f;			

GLfloat LightAmbient[]=		{ 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat LightDiffuse[]=		{ 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat LightPosition[]=	{ 0.0f, 0.0f, 2.0f, 1.0f };

GLuint	filter=1;				
GLuint	texture[3];			

//...................................

extern FPS fps;
extern bool paused;
extern bool nextg;

int otoc_wheel = 0, otoc_wheel_shift = 0;
int n_wheel = 1;  

int mouse_x;
int mouse_y;

int mouse_Dx,mouse_Dy;
short pohni_rm = 0;       // indikuje zda bylo zmacknuto prave tlacitko 


//...................................



int WINAPI WinMain(	HINSTANCE	hInstance,			// Instance
					HINSTANCE	hPrevInstance,		// Previous Instance
					LPSTR		lpCmdLine,			// Command Line Parameters
					int			nCmdShow)			// Window Show State
{
	MSG		msg;									// Windows Message Structure
	BOOL	done=FALSE;	
    
 

    
    fout_t . open("Data/TEST_OUT.txt");
    
    keys[VK_RETURN] = !true; // ! aby se pri startu zobrazil ovladaci panel       							
	
	float posun        = 0.1;//20.f/fps.get();
    float otoc_speed   = 0.01;
    float otoc_rot     = 10;//180/fps.get();

	// Ask The User Which Screen Mode They Prefer
	//if (MessageBox(NULL,"Would You Like To Run In Fullscreen Mode?", "Start FullScreen?",MB_YESNO|MB_ICONQUESTION)==IDNO)
	//{
		fullscreen=FALSE;							// Windowed Mode
	//}

	// Create Our OpenGL Window
	if (!CreateGLWindow(nadpis,800,600,16,fullscreen))
	{
		return 0;									// Quit If Window Was Not Created
	}

	while(!done)									// Loop That Runs While done=FALSE
	{
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))	// Is There A Message Waiting?
		{
			if (msg.message==WM_QUIT)				// Have We Received A Quit Message?
			{
				done=TRUE;							// If So done=TRUE
			}
			else									// If Not, Deal With Window Messages
			{
				TranslateMessage(&msg);				// Translate The Message
				DispatchMessage(&msg);				// Dispatch The Message
			}
		}
		else										// If There Are No Messages
		{
			// Draw The Scene.  Watch For ESC Key And Quit Messages From DrawGLScene()
			if ((active && !DrawGLScene()) || keys[VK_ESCAPE])	// Active?  Was There A Quit Received?
			{
				done=TRUE;							// ESC or DrawGLScene Signalled A Quit
			}
			else									// Not Time To Quit, Update Screen
			{
				SwapBuffers(hDC);					// Swap Buffers (Double Buffering)
				
                //...................
                
                static bool pRETURN = false;
                if (keys[VK_RETURN] && !pRETURN)
				{
					pRETURN = true;
                    
                    //vyvol�me dialog
                    CreateDialog(hInstance, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, (DLGPROC)DialogProc);
                    
				}
				if (!keys[VK_RETURN])
				{
                    pRETURN = false;
				}
                
                posun       = 10./fps.get();
                otoc_speed  = 0.01;
                otoc_rot    = 180/fps.get();
                
                static bool pSPACE = false;
                if (keys[VK_SPACE] && !pSPACE)
				{
					pSPACE = true;
                    paused = !paused;
				}
				if (!keys[VK_SPACE])
				{
                    pSPACE = false;
				}
				
				
				static bool pO = false;
                if (keys['O'] && !pO)
				{
					pO     = true;
                    nextg  = true;
                    paused = true;
				}
				if (!keys['O'])
				{
                    pO = false;
				}
				
                if (keys['P'])
				{
                    nextg =  true;
                    paused = true;
				}
				
                
                if (keys['E'] || otoc_wheel<0)
				{
					z-=posun;// (1 + (otoc_wheel != 0?1.5:0));
					
					n_wheel++;
                    if (!(n_wheel%10))
                       otoc_wheel = 0;
				}
				if (keys['Q'] || otoc_wheel>0)
				{
					z+=posun;// (1 + (otoc_wheel != 0?1.5:0));
					
					n_wheel++;
                    if (!(n_wheel%10))
                       otoc_wheel = 0;
				} 
				
				if (keys[VK_PRIOR]||keys['S'])
				{
					y+=posun;
				}
				if (keys['A'])
				{
					x+=posun;
				}
				if (keys['D'])
				{
					x-=posun;
				}
                
				if (keys['W'])
				{
					y-=posun;
				}
				
				
                if (keys['T'])
				{
					y=0;
					x=0;
					z=-5;
					
					xrot=yrot=zrot=0;
				}
                
                
                if (keys[VK_UP])
				{   
					xrot -= otoc_rot;
				}
				if (keys[VK_DOWN])
				{
					xrot+=otoc_rot;
				}
				if (keys[VK_LEFT])
				{
					yrot-=otoc_rot;
				}
				if (keys[VK_RIGHT])
				{
					yrot+=otoc_rot;
				}
                
                //.....................
                
                if (keys['L'] && !lp)
				{
					lp=TRUE;
					light=!light;
					if (!light)
					{
						glDisable(GL_LIGHTING);
					}
					else
					{
						glEnable(GL_LIGHTING);
					}
				}
				if (!keys['L'])
				{
					lp=FALSE;
				}
				if (keys['F'] && !fp)
				{
					fp=TRUE;
					filter+=1;
					if (filter>2)
					{
						filter=0;
					}
				}
				if (!keys['F'])
				{
					fp=FALSE;
				}

				
                

				if (keys[VK_F1])						// Is F1 Being Pressed?
				{
					keys[VK_F1]=FALSE;					// If So Make Key FALSE
					KillGLWindow();						// Kill Our Current Window
					fullscreen=!fullscreen;				// Toggle Fullscreen / Windowed Mode
					// Recreate Our OpenGL Window
					if (!CreateGLWindow(nadpis,800,600,16,fullscreen))
					{
						return 0;						// Quit If Window Was Not Created
					}
				}
			}
		}
	}

	// Shutdown
	closeKapky();
	KillGLWindow();									// Kill The Window
	return (msg.wParam);							// Exit The Program
}
