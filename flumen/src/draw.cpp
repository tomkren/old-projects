#include "main_h.h"

extern GLfloat	xrot;				// X Rotation
extern GLfloat	yrot;				// Y Rotation
extern GLfloat  zrot;


extern GLfloat xspeed;				// X Rotation Speed
extern GLfloat yspeed;				// Y Rotation Speed

extern GLfloat	x;			
extern GLfloat	y;			
extern GLfloat	z;			

extern GLuint	filter;				// Which Filter To Use
extern GLuint	texture[3];	

FPS fps(1,100.);

void drawKapka(num xpos, num ypos, num zpos);
void drawKapka2(int b,int i);

extern int Nbunek;
extern num Rbky;

extern Bunka* bunky;

bool paused = true;
bool nextg  = false;

bool b_mrizka = !true;

int DrawGLScene(GLvoid)									// Here's Where We Do All The Drawing
{
    if (!paused || nextg)
    {
        vypocet();
        
        nextg = false;
    }
    
    fps.Vypocet();
    
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear The Screen And The Depth Buffer
	glLoadIdentity();									// Reset The View


	glTranslatef(x,y,z);

	glRotatef(xrot,1.0f,0.0f,0.0f);
	glRotatef(yrot,0.0f,1.0f,0.0f);
	glRotatef(zrot,0.0f,0.0f,1.0f);

	glBindTexture(GL_TEXTURE_2D, texture[filter]);


    glEnable(GL_TEXTURE_2D);
    glBegin(GL_QUADS); //odtud kresli kapky
    glColor3f(1.,1.,1.);
    
    //rozmery jsou od -1 do 1 ve v�ech osach
    
    num cols[7] = {0.125,0.25,0.375,0.5,0.625,0.75,0.875};
    
    for (int b = 0; b < Nbunek; b++)
    {
        glColor3f(cols[b%7],cols[(b+1)%7],cols[(b+2)%7]);
    
        
        for (int k=0 ; k < bunky[b].kapky.size() ; k++)
        {
            drawKapka2(b,k);
        }   
    }            
                
    glEnd();
    glDisable(GL_TEXTURE_2D);    
    
    
    //vykresl� m��
    glColor3f(.3,.3,.3);
    glLineWidth(1.0f);
    glBegin(GL_LINES);
        
        for (int i=0; i<72; i+=3)
            glVertex3f(HRANY[i],HRANY[i+1],HRANY[i+2]);
    
    if(b_mrizka)
    {
    glColor3f(.05,.05,.05);
        
        for (int b=0; b<Nbunek ; b++)
            for (int i=0; i<72; i+=3)
                glVertex3f( bunky[b].stred.xval() + Rbky*HRANY[i],
                            bunky[b].stred.yval() + Rbky*HRANY[i+1],
                            bunky[b].stred.zval() + Rbky*HRANY[i+2]);
    }
               
    glEnd();    

    
    xrot+=xspeed;
	yrot+=yspeed;
	return TRUE;										// Keep Going
}

void drawKapka2(int b,int i)
{
    Kapka* pk = &(bunky[b].kapky[i]);
    
    drawKapka( pk->X.xval(), pk->X.yval() , pk->X.zval() );
}


void drawKapka(num xpos, num ypos, num zpos)
{
    const num a = 0.02;    
    	
    		//glNormal3f( 0.0f, 0.0f, .1f);
    		glTexCoord2f(0.0f, 0.0f); glVertex3f(xpos - a , ypos - a , zpos );
    		glTexCoord2f(1.0f, 0.0f); glVertex3f(xpos + a , ypos - a , zpos );
    		glTexCoord2f(1.0f, 1.0f); glVertex3f(xpos + a , ypos + a , zpos);
    		glTexCoord2f(0.0f, 1.0f); glVertex3f(xpos - a , ypos + a , zpos );

            glTexCoord2f(0.0f, 0.0f); glVertex3f(xpos , ypos - a ,zpos + a );
    		glTexCoord2f(1.0f, 0.0f); glVertex3f(xpos , ypos - a ,zpos - a );
    		glTexCoord2f(1.0f, 1.0f); glVertex3f(xpos , ypos + a ,zpos - a );
    		glTexCoord2f(0.0f, 1.0f); glVertex3f(xpos , ypos + a ,zpos + a );
    
            glTexCoord2f(0.0f, 0.0f); glVertex3f(xpos - a ,ypos ,zpos + a );
    		glTexCoord2f(1.0f, 0.0f); glVertex3f(xpos - a ,ypos ,zpos - a );
    		glTexCoord2f(1.0f, 1.0f); glVertex3f(xpos + a ,ypos ,zpos - a );
    		glTexCoord2f(0.0f, 1.0f); glVertex3f(xpos + a ,ypos ,zpos + a );	
}
