#ifndef _VEKTOR_H_
#define _VEKTOR_H_


#include "main_h.h"



class Vektor
{
    private:
        num x;
        num y;
        num z;
        
    public:
        Vektor();
        Vektor(num xval, num yval, num zval = 0);
        ~Vektor();
        
        void set(num xv, num yv, num zv)    {x=xv;y=yv;z=zv;}
        void vynasob(num numb)              {x*=numb;y*=numb;z*=numb;}
        void pricti(Vektor a)               {x+=a.x; y+=a.y; z+=a.z;}
        
        
        num xval() const {return x;}
        num yval() const {return y;}
        num zval() const {return z;}
        num abs() const;
        
        
        Vektor operator+(const Vektor& b) const;
        Vektor operator-(const Vektor& b) const;
        Vektor operator-() const;
        Vektor operator*(num numb) const;
        Vektor operator*(const Vektor& b) const;  //vektorovej soucin
        num operator&(const Vektor& b) const;     //skalarni sou�in
        
        bool LinNez(const Vektor& u) const; 
        Vektor& SetByBod(const Bod& A);
        
        friend Vektor operator*(num numb, const Vektor& a);
    
        friend Bod operator+(const Bod& A, const Vektor& u);
        friend Bod operator+(const Vektor& u, const Bod& A);
        
        friend ostream& operator<<(ostream& os_obj , const Vektor& a);
        
};

#endif
