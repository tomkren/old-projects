#include "main_h.h"

void exeChanges();

extern ofstream fout_t;
extern ofstream fout_Nsavu;
extern ifstream  fin_Nsavu;
extern ofstream fout_save;
extern ifstream fin_load;

extern bool paused;

extern int NbRow;
extern num g;
extern num G;
extern num odrazovka;

extern num pv[18];

bool ch_pv [18];
int  new_pv[18];


bool ch_NbRow = false , ch_g = false , ch_G = false, ch_odrazovka = false;
int  new_NbRow , new_g , new_G , new_odrazovka;

int test = 42;
BOOL good_wtf;

char nazev_wtf[256];


// 4 + (9 + 9) =  22
// int saveSize = 22;

int Nsavu;

void delete_saved_proudy()
{
    fout_save.open("Data/saved.txt");
    fout_save << " ";
    fout_save.close();
    
    fout_Nsavu.open("Data/Nsavu.txt");
    fout_Nsavu << 0;
    fout_Nsavu.close();
    
    Nsavu = 0;
    
}

void load_proudy()
{
    paused = false;
    
    if (Nsavu == 0) return;
    
    static int aktualneLoadnout = 0;
    
    fin_load.open("Data/saved.txt" );
    
    for (int i = 0 ; i <= aktualneLoadnout ; i++)
    {
        fin_load >> nazev_wtf;
        fin_load >> NbRow;
        fin_load >> g;
        fin_load >> G;
        fin_load >> odrazovka;
        
        for (int i=0 ; i < 18 ; i++ )
        {
            fin_load >> pv[i];
        }
    }
    
    aktualneLoadnout ++ ;
    aktualneLoadnout = aktualneLoadnout%Nsavu;
    
    fin_load.close();
    
    closeKapky();
    initKapky();
}

void save_proudy()
{
    fout_save.open("Data/saved.txt" , ios::app );
    
    //fout_t<<"!!";
    
    fout_save <<"<NAZEV/ID_PROUDU> "<< NbRow<<" "<< g<<" " << G <<" " << odrazovka<<"\n";
    
    for (int i=0 ; i < 18 ; i++ )
    {
        fout_save << " " << pv[i];
        if(i == 8) fout_save << "\n";
    }
    
    fout_save << "\n\n";
    Nsavu ++;
    
    fout_Nsavu.open("Data/Nsavu.txt");
    fout_Nsavu << Nsavu;
    fout_Nsavu.close();
     
    fout_save.close();
}

void initDialog()
{
    fin_Nsavu.open("Data/Nsavu.txt");
    fin_Nsavu >> Nsavu;
    fin_Nsavu.close();
    
    //fout_Nsavu.open("Data/Nsavu.txt");
    
    
    //fout_t << Nsavu;
    
    for (int i = 0 ; i < 18 ; i++ )
    {
        ch_pv[i] = false;
    }
}

void chytni_pv (HWND hwndDlg , bool podminka , int IDC ,  int K )
{
    if ( podminka )
	{
		new_pv[K] = GetDlgItemInt (hwndDlg, IDC ,&good_wtf, true);
		ch_pv [K] = true;
	}
}

//tenhle zdrojak stejne nikdo necte..

INT_PTR CALLBACK DialogProc(HWND hwndDlg,UINT uMsg,WPARAM wParam,LPARAM lParam)
{
  switch ( uMsg )
  {
    case WM_COMMAND:
      switch ( LOWORD(wParam) )
      {
        case IDC_SAVE:
            save_proudy();
            break;
        
        case IDC_LOAD:
            load_proudy();
            break;
        
        case IDC_DELALL:
            delete_saved_proudy();
            break;
        
        case IDC_E_NbRow:
    		if ( HIWORD(wParam) == EN_CHANGE )
    		{
    			new_NbRow = GetDlgItemInt (hwndDlg, IDC_E_NbRow ,&good_wtf, true);
    			ch_NbRow  = true;
    		}
    		break;  
       
       case IDC_E_g:
    		if ( HIWORD(wParam) == EN_CHANGE )
    		{
    			new_g = GetDlgItemInt (hwndDlg, IDC_E_g ,&good_wtf, true);
    			ch_g  = true;
    		}
    		break;
            
        case IDC_E_G:
    		if ( HIWORD(wParam) == EN_CHANGE )
    		{
    			new_G = GetDlgItemInt (hwndDlg, IDC_E_G ,&good_wtf, true);
    			ch_G  = true;
    		}
    		break;
            
        case IDC_E_PV0: chytni_pv  ( hwndDlg , HIWORD(wParam) == EN_CHANGE  , IDC_E_PV0 , 0 ); break;
        case IDC_E_PV1: chytni_pv  ( hwndDlg , HIWORD(wParam) == EN_CHANGE  , IDC_E_PV1 , 1 ); break;
        case IDC_E_PV2: chytni_pv  ( hwndDlg , HIWORD(wParam) == EN_CHANGE  , IDC_E_PV2 , 2 ); break;
        case IDC_E_PV3: chytni_pv  ( hwndDlg , HIWORD(wParam) == EN_CHANGE  , IDC_E_PV3 , 3 ); break;
        case IDC_E_PV4: chytni_pv  ( hwndDlg , HIWORD(wParam) == EN_CHANGE  , IDC_E_PV4 , 4 ); break;
        case IDC_E_PV5: chytni_pv  ( hwndDlg , HIWORD(wParam) == EN_CHANGE  , IDC_E_PV5 , 5 ); break;
        case IDC_E_PV6: chytni_pv  ( hwndDlg , HIWORD(wParam) == EN_CHANGE  , IDC_E_PV6 , 6 ); break;
        case IDC_E_PV7: chytni_pv  ( hwndDlg , HIWORD(wParam) == EN_CHANGE  , IDC_E_PV7 , 7 ); break;
        case IDC_E_PV8: chytni_pv  ( hwndDlg , HIWORD(wParam) == EN_CHANGE  , IDC_E_PV8 , 8 ); break;
        case IDC_E_PV9: chytni_pv  ( hwndDlg , HIWORD(wParam) == EN_CHANGE  , IDC_E_PV9 , 9 ); break;
        case IDC_E_PV10: chytni_pv ( hwndDlg , HIWORD(wParam) == EN_CHANGE  , IDC_E_PV10 , 10 ); break;
        case IDC_E_PV11: chytni_pv ( hwndDlg , HIWORD(wParam) == EN_CHANGE  , IDC_E_PV11 , 11 ); break;
        case IDC_E_PV12: chytni_pv ( hwndDlg , HIWORD(wParam) == EN_CHANGE  , IDC_E_PV12 , 12 ); break;
        case IDC_E_PV13: chytni_pv ( hwndDlg , HIWORD(wParam) == EN_CHANGE  , IDC_E_PV13 , 13 ); break;
        case IDC_E_PV14: chytni_pv ( hwndDlg , HIWORD(wParam) == EN_CHANGE  , IDC_E_PV14 , 14 ); break;
        case IDC_E_PV15: chytni_pv ( hwndDlg , HIWORD(wParam) == EN_CHANGE  , IDC_E_PV15 , 15 ); break;
        case IDC_E_PV16: chytni_pv ( hwndDlg , HIWORD(wParam) == EN_CHANGE  , IDC_E_PV16 , 16 ); break;
        case IDC_E_PV17: chytni_pv ( hwndDlg , HIWORD(wParam) == EN_CHANGE  , IDC_E_PV17 , 17 ); break;
        
        
        case IDC_E_odrazovka:
    		if ( HIWORD(wParam) == EN_CHANGE )
    		{
    			new_odrazovka = GetDlgItemInt (hwndDlg, IDC_E_odrazovka ,&good_wtf, true);
    			ch_odrazovka  = true;
    		}
    		break;    
        
        case IDC_POUZIT:
            exeChanges();
            break;
            
        case IDC_RESTART:
            
            paused = false;
            closeKapky();
            initKapky();
            break;
        
        case IDOK:
            exeChanges();
            EndDialog(hwndDlg, IDOK);
            break;
          
        case IDCANCEL:
            EndDialog(hwndDlg, IDCANCEL);
            break;
          
        case IDC_PLAYPAUSE:
            paused = !paused;
            break;
      }
      break;
  }
  return FALSE;
}

void exeChanges()
{
    
    if (ch_g)
    {
        ch_g = false;
        g    = new_g * 1E-5 / 1000.0 ; //1000 == 1E-5
    }
    
    if (ch_odrazovka)
    {
        ch_odrazovka = false;
        odrazovka    = new_odrazovka * 5E-2 / 100.0 ; //10 == 5E-2
    }
    
    if (ch_G)
    {
        ch_G = false;
        G    = new_G * 1E-5 / 10.0 ; //10 == 1E-5
    }
    
    if (ch_NbRow)
    {
        ch_NbRow = false;
        NbRow = new_NbRow;
               
        closeKapky();
        initKapky();
    }
    
    for (int i = 0; i < 18 ; i++)
    {
        int imod = i%9;
        
        if (ch_pv[i] && imod < 3)
        {
            ch_pv[i] = false;
            pv[i]    = ( new_pv[i] - 100. ) / 100. ; //
        }
        
        if (ch_pv[i] && imod >= 3 && imod < 5)
        {
            ch_pv[i] = false;
            pv[i]    = ( new_pv[i] - 100. ) * 1E-4 ; //
        }
        
        //z kov� slo�ka rychlosti
        if (ch_pv[i] && imod == 5)
        {
            ch_pv[i] = false;
            pv[i]    = ( -new_pv[i] ) * 1E-4 ; //
        }
        
        if (ch_pv[i] && imod == 6)
        {
            ch_pv[i] = false;
            pv[i]    = new_pv[i] * 1E-4; //
        }
        
        if (ch_pv[i] && imod == 7)
        {
            ch_pv[i] = false;
            pv[i]    = new_pv[i]; //
        }
        
        if (ch_pv[i] && imod == 8)
        {
            ch_pv[i] = false;
            pv[i]    = new_pv[i]; //
        }
        
    }
}
