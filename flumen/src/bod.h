#ifndef _BOD_H_
#define _BOD_H_

#include "main_h.h"


class Bod
{
    private:
        num x;
        num y;
        num z;
        
    public:
        Bod();
        Bod(num xval, num yval, num zval = 0);
        ~Bod();     
        
        void set(num xv, num yv, num zv) {x=xv;y=yv;z=zv;}
        
        num xval() const {return x;}
        num yval() const {return y;}
        num zval() const {return z;}
        
        num& s(int S) {return (S==0?x:(S==1?y:z)); }
        
        Bod operator+(const Bod& b) const;
        Bod operator-(const Bod& b) const;
        Bod operator-() const;
        
        //pridano dodatecne a trosicku um�le
        Bod& vydel(num n) {x /= n; y/= n; z /=n; return *this; }
        
                
        
        friend ostream& operator<<(ostream& os_obj , const Bod& A);
        friend num vzdalenost(const Bod& a,const Bod& b);
};

#endif
