#include "main_h.h"

extern int otoc_wheel, otoc_wheel_shift, n_wheel, mouse_x, mouse_y, mouse_Dx, mouse_Dy;
extern short pohni_rm; 
extern bool	keys[256], active;
extern GLfloat x, y, z;

LRESULT CALLBACK WndProc(	HWND	hWnd,			// Handle For This Window
							UINT	uMsg,			// Message For This Window
							WPARAM	wParam,			// Additional Message Information
							LPARAM	lParam)			// Additional Message Information
{
	switch (uMsg)									// Check For Windows Messages
	{
        case WM_MOUSEWHEEL:
        {
            otoc_wheel       = GET_WHEEL_DELTA_WPARAM(wParam);
            otoc_wheel_shift = keys[VK_SHIFT];
            break;
        }
        
		case WM_ACTIVATE:							// Watch For Window Activate Message
		{
			if (!HIWORD(wParam))					// Check Minimization State
			{
				active=TRUE;						// Program Is Active
			}
			else
			{
				active=FALSE;						// Program Is No Longer Active
			}

			return 0;								// Return To The Message Loop
		}

		case WM_SYSCOMMAND:							// Intercept System Commands
		{
			switch (wParam)							// Check System Calls
			{
				case SC_SCREENSAVE:					// Screensaver Trying To Start?
				case SC_MONITORPOWER:				// Monitor Trying To Enter Powersave?
				return 0;							// Prevent From Happening
			}
			break;									// Exit
		}

		case WM_CLOSE:								// Did We Receive A Close Message?
		{
			PostQuitMessage(0);						// Send A Quit Message
			return 0;								// Jump Back
		}

		case WM_KEYDOWN:							// Is A Key Being Held Down?
		{
			keys[wParam] = TRUE;					// If So, Mark It As TRUE
			return 0;								// Jump Back
		}

		case WM_KEYUP:								// Has A Key Been Released?
		{
			keys[wParam] = FALSE;					// If So, Mark It As FALSE
			return 0;								// Jump Back
		}

		case WM_SIZE:								// Resize The OpenGL Window
		{
			ReSizeGLScene(LOWORD(lParam),HIWORD(lParam));  // LoWord=Width, HiWord=Height
			return 0;								// Jump Back
		}
		
		//�upan� pravim
		case WM_LBUTTONDOWN:
			{
	            mouse_x = mouse_Dx = LOWORD(lParam);          
				mouse_y = mouse_Dy = HIWORD(lParam);
				
				pohni_rm = 1;
			}
		break;
        
        case WM_LBUTTONUP:
		{             
          pohni_rm = 0;
        } 
		break;
        
		case WM_MOUSEMOVE:
		{
            mouse_x = LOWORD(lParam);          
			mouse_y = HIWORD(lParam);
			
		    if(pohni_rm == 1)
            {   
                mouse_Dx -= LOWORD(lParam);          
		        mouse_Dy -= HIWORD(lParam);
                
                x += z * float(mouse_Dx/900.0);
                y -= z * float(mouse_Dy/900.0);
                
                mouse_Dx = LOWORD(lParam);          
				mouse_Dy = HIWORD(lParam);
            }
		}
		break;
	}

	// Pass All Unhandled Messages To DefWindowProc
	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}
