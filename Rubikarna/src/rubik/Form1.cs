﻿/*
Rubikarna
Tomáš Křen
Zápočtový program
Programování II - PRG031
letní semestr 2007/8
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace rubik
{
    public partial class Form1 : Form
    {
        private Kostka vykreslovanaKostka = new Kostka();
        private Interpret interpret = new Interpret();

        public static event EventHandler<StringListEventArgs> eventForm1PosilaScript;
        
        public Form1()
        {
            InitializeComponent();
            
            MyOutput.eventNapisOutput += new EventHandler<StringEventArgs>(MyOutput_eventNapisOutput);
            Kostka.eventKostkaChange += new EventHandler(Kostka_eventKostkaChange);
            Interpret.eventInterpretChceScript += new EventHandler<StringEventArgs>(Interpret_eventInterpretChceScript);
            Interpret.eventInterpretChceTahnout += new EventHandler<StringEventArgs>(Interpret_eventInterpretChceTahnout);
            Interpret.eventInterpretRikaCoJeDalsiRadek += new EventHandler<StringEventArgs>(Interpret_eventInterpretRikaCoJeDalsiRadek);
            Interpret.eventInterpretChceZkontrolovatRelaciBarvaPozice += new EventHandler<StringListEventArgs>(Interpret_eventInterpretChceZkontrolovatRelaciBarvaPozice);

            if (File.Exists(@".\scripts\default.txt"))
            {
                textBoxInput.Text = File.ReadAllText(@".\scripts\default.txt", Encoding.Default);
                interpret.LoadScript("main");
                textBoxInput.Select(0, 0);
            }
            else
                MyOutput.PrintIt(@"NEBYL NALEZEN SOUBOR .\scripts\default.txt");
        }

        void Interpret_eventInterpretChceZkontrolovatRelaciBarvaPozice(object sender, StringListEventArgs e)
        {
            int jeTam;
            if(e.lst[0][0] != 'c')
            {
                jeTam = vykreslovanaKostka.TestujZdaJeBarvaNaPozici(e.lst[0], e.lst[1]);
            }
            else
            {
                jeTam = vykreslovanaKostka.TestujZdaJeBarevnaKostickaNaPoziciKosticky(e.lst[0], e.lst[1]);
            }

            //je tam bude 1, 0 nebo -1 (pokud byla zadana neplatna data na test)
            interpret.SetInbox(jeTam);

        }

        void Interpret_eventInterpretRikaCoJeDalsiRadek(object sender, StringEventArgs e)
        {
            int i = e.str.IndexOf(' ');
            
            labelNextLineNum.Text = e.str.Substring(0,i);
            textBoxNextLine.Text  = e.str.Substring(i);
        }

        void Interpret_eventInterpretChceTahnout(object sender, StringEventArgs e)
        {
            vykreslovanaKostka.Tahni(e.str);
        }

        void Interpret_eventInterpretChceScript(object sender, StringEventArgs e)
        {
            string[] scriptLines = textBoxInput.Lines;
            List<string> script = new List<string>( scriptLines );
            script.Insert(0, e.str);

            eventForm1PosilaScript(this,new StringListEventArgs( script ));
        }

        void Kostka_eventKostkaChange(object sender, EventArgs e)
        {
            this.Refresh();
            //MyOutput.PrintIt("Překresleno kvuli změně kostky.");
        }


        void MyOutput_eventNapisOutput(object sender, StringEventArgs e)
        {
            listBoxOutput.Items.Insert(0, e.str );
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = e.Graphics;
            //gr.Clear(Color.DarkKhaki);

            KostkaImg ki = new KostkaImg(gr, vykreslovanaKostka);

            ki.Draw3D();
            ki.Draw2D(120,377);

        }

        private void buttonTest_Click(object sender, EventArgs e)
        {
            interpret.Exe(textBoxTest.Text);

            //MyOutput.PrintIt("test zmacknut");

            //MyOutput.PrintIt(vykreslovanaKostka.GetSortedBarvyByKodKosticky(textBoxTest.Text));

            //interpret.VyhodnotVyraz(textBoxTest.Text);

            //Logika.VyhodnotVyraz(textBoxTest.Text);

            //this.Refresh();
        }

        private void buttonTOaCW_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("TOaCW");
        }
        private void buttonTOaCC_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("TOaCC");
        }
        private void buttonTObCW_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("TObCW");
        }
        private void buttonTObCC_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("TObCC");
        }
        private void buttonTOcCW_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("TOcCW");
        }
        private void buttonTOcCC_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("TOcCC");
        }
        private void buttonLEaCC_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("LEaCC");
        }
        private void buttonLEaCW_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("LEaCW");
        }
        private void buttonLEbCC_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("LEbCC");
        }
        private void buttonLEbCW_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("LEbCW");
        }
        private void buttonLEcCC_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("LEcCC");
        }
        private void buttonLEcCW_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("LEcCW");
        }
        private void buttonFRaCC_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("FRaCC");
        }
        private void buttonFRaCW_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("FRaCW");
        }
        private void buttonFRbCC_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("FRbCC");
        }
        private void buttonFRbCW_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("FRbCW");
        }
        private void buttonFRcCC_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("FRcCC");
        }
        private void buttonFRcCW_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("FRcCW");
        }
        private void buttonRBO_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("rBO");
        }
        private void buttonRTO_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("rTO");
        }
        private void buttonRLE_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("rLE");
        }
        private void buttonRRI_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("rRI");
        }
        private void buttonRCC_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("rCC");
        }
        private void buttonRCW_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.Tahni("rCW");
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            listBoxOutput.Items.Clear();
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.ResetKostka();
            MyOutput.PrintIt("Kostka resetována!");
        }

        private void buttonRandTah_Click(object sender, EventArgs e)
        {
            vykreslovanaKostka.TahniRand();
        }

        private void buttonShuffle_Click(object sender, EventArgs e)
        {
            MyOutput.PrintIt("Míchání zahájeno"); 
            vykreslovanaKostka.Zamichej( int.Parse(textBoxShuffleSize.Text) );
            MyOutput.PrintIt("Míchání dokončeno");
        }

        private void buttonLoadAsScript_Click(object sender, EventArgs e)
        {
            interpret.LoadScript("main");
            MyOutput.PrintIt("INPUT byl nahrán jako script.");
            //interpret.PrintScript();
        }

        private void button1Step_Click(object sender, EventArgs e)
        {
            interpret.Exe1Step();
        }

        private void buttonLoadScript_Click(object sender, EventArgs e)
        {
            
            if (openFileDialog.ShowDialog() == DialogResult.OK)
                if (openFileDialog.FileName != "")
                    textBoxInput.Text = File.ReadAllText(openFileDialog.FileName,Encoding.Default);

            interpret.LoadScript("main");
        }

        private void buttonSaveInput_Click(object sender, EventArgs e)
        {
            
            
            saveFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.FilterIndex = 2;
            saveFileDialog.RestoreDirectory = true;
            saveFileDialog.AddExtension = true;
            saveFileDialog.DefaultExt = "txt";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(saveFileDialog.FileName, textBoxInput.Text, Encoding.Default);
            }
        }

        private void buttonExe_Click(object sender, EventArgs e)
        {
            interpret.Exe("main");
        }

        private void checkBoxShowInfo_CheckedChanged(object sender, EventArgs e)
        {
            interpret.ChangeShowInterpretInfo();
        }

   }
    //třída sloužící k vypisování na textový výstup
    public class MyOutput
    {
        public static event EventHandler<StringEventArgs> eventNapisOutput;
        public MyOutput()
        { }

        //metoda vypisující na textový výstup
        public static void PrintIt(string str)
        {
            if (eventNapisOutput != null) eventNapisOutput(null, new StringEventArgs(str));
        }
    }
    public class StringEventArgs : EventArgs
    {
        public string str;
        public StringEventArgs(string str)
        {
            this.str = str;
        }
    }
    public class StringListEventArgs : EventArgs
    {
        public List<string> lst;
        public StringListEventArgs(List<string> lst)
        {
            this.lst = lst;
        }
    }

    //třída interpretu jazyka Rublan
    public class Interpret
    {
        private int  pc; //program counter - uchovává informaci o tom na jakém řádku se skript právě nachází
        List<string> script = new List<string>(); //zde je uložen text skriptu
        List<string> vars = new List<string>();  //zde jsou uložené argumenty makra
        bool nastalError;
        bool showInterpretInfo;

        int inbox; //pomocná proměná pro mezitřídovou komunikaci

        public static event EventHandler<StringEventArgs> eventInterpretChceScript;
        public static event EventHandler<StringEventArgs> eventInterpretChceTahnout;
        public static event EventHandler<StringEventArgs> eventInterpretRikaCoJeDalsiRadek;
        public static event EventHandler<StringListEventArgs> eventInterpretChceZkontrolovatRelaciBarvaPozice;

        Stack<int> macroStack = new Stack<int>(); //zde je uloženo kam se má po navratu z makra skočit
        Stack<int> whileStack = new Stack<int>(); //zde je uloženo kam se má skočit zpět když se dospěje na ENDWHILE 
        Stack<List<string>> varStack = new Stack<List<string>>(); //zásobník kam se schvají staré argumenty maker když se vstoupí do nějakého makra

        char[] BILY = { ' ', '\t' };

        public Interpret()
        {
            showInterpretInfo = true;
            ResetPC();
            nastalError = false;
            inbox = -1;

            Form1.eventForm1PosilaScript += new EventHandler<StringListEventArgs>(Form1_eventForm1PosilaScript);
        }

        public void ChangeShowInterpretInfo()
        {
            showInterpretInfo = !showInterpretInfo;
        }

        public void SetInbox(int val)
        {
            inbox = val;
        }

        void OznamDalsiRadek()
        {
            string dalsiRadek;
            
            if (DelkaScriptu() > pc)
            {
                dalsiRadek = pc.ToString() + " " + script[pc];
            }
            else if (DelkaScriptu() == 0)
            {
                dalsiRadek = "X N/A";
            }
            else
            {
                dalsiRadek = "X SCRIPT SKONCIL";
            }
            eventInterpretRikaCoJeDalsiRadek(this, new StringEventArgs(dalsiRadek));
        }

        void Form1_eventForm1PosilaScript(object sender, StringListEventArgs e)
        {
            nastalError = false;

            string jmenoScriptu = e.lst[0];
            e.lst.RemoveAt(0);
            script = e.lst;
            
            ResetPC();
            if (jmenoScriptu.Length == 0)
                Error("Nebylo zadáno jméno scriptu.");
            else if (!JumpToMacro(jmenoScriptu))
                Error("Script neobsahuje MACRO "+jmenoScriptu);
            OznamDalsiRadek();
        }

        public void LoadScript(string startMacro)
        {
            eventInterpretChceScript(this,new StringEventArgs(startMacro));
        }

        public void PrintScript()
        {
            foreach (string radka in script)
            {
                MyOutput.PrintIt(radka);
            }
        }

        public void Error(string hlaska)
        {
            MyOutput.PrintIt("!!! SCRIPT ERROR [ln " + pc.ToString() + "]: " + hlaska);
            nastalError = true;
            MyOutput.PrintIt("SCRIPT BYL NÁSILNĚ UKONČEN");
        }

        public void Info(string hlaska)
        {
            if(showInterpretInfo)
                MyOutput.PrintIt("[ln " + pc.ToString() + "] " + hlaska);
        }

        public bool ExistujeScript()
        {
            if (DelkaScriptu() > 0) return true;
            return false;
        }

        public void ResetPC()
        {
            pc = 0;
        }

        public int DelkaScriptu()
        {
            return script.Count;
        }

        //metoda jež přeastaví pc na hodnotu řádku se začátkem požadovaného makra
        public bool JumpToMacro(string jmenoMakra)
        {
            int i = 0;
            foreach (string radka in script)
            {
                string[] slova = radka.Trim().Split(BILY);

                if (slova[0].ToLower() == "macro" && slova[1] == jmenoMakra)
                {
                    pc = i;
                    return true;
                }
                i++;
            }
            return false;
        }

        //metoda vrací číslo řádku kam je třeba skočit po vyhodnocení podmínky ifu jako nepravdivé
        public int KamSkocitZNegativnihoIfu(int poziceIfu)
        {
            int numIfu = 0;

            for (int i = poziceIfu+1; i < DelkaScriptu(); i++)
            {
                string[] slova = script[i].Trim().Split(BILY);

                if (numIfu == 0 && (slova[0].ToLower() == "else" || slova[0].ToLower() == "endif"))
                {
                    return i + 1;
                }

                if (slova[0].ToLower() == "if")//vstupujeme do vnořeného ifu
                {
                    numIfu++;
                }
                if (slova[0].ToLower() == "endif")//vystupujeme z vnořeného ifu
                {
                    numIfu--;
                }
            }
            return -1;
        }

        //metoda vrací číslo řádku kam je třeba skočit po vyhodnocení podmínky WHILENOTu jako pravdivé
        public int KamSkocitZNegativnihoWhilu(int poziceWhilu)
        {
            int numWhilu = 0;

            for (int i = poziceWhilu + 1; i < DelkaScriptu(); i++)
            {
                string[] slova = script[i].Trim().Split(BILY);

                if (numWhilu == 0 && slova[0].ToLower() == "endwhile" )
                {
                    return i + 1;
                }

                if (slova[0].ToLower() == "whilenot")//vstupujeme do vnořeného whilu
                {
                    numWhilu++;
                }
                if (slova[0].ToLower() == "endwhile")//vystupujeme z vnořeného whilu
                {
                    numWhilu--;
                }
            }
            return -1;
        }

        //po zavolání této metody se provede celý skript
        public void Exe(string jmenoScriptu)
        {
            LoadScript(jmenoScriptu);
            while ( !nastalError && Exe1Step() ) ;
        }

        //vyhodnotí logický výraz
        public bool VyhodnotVyraz(string vyraz)
        {
            vyraz = vyraz.Trim();

            //najdeme výskyty elementárních výrazů a nahradíme je přímo logickejma hodnotama 0 a 1
            
            int i = 0;
            while( i < vyraz.Length ) 
            {
                char ch = vyraz[i];
                if ( !Logika.JeToLogickyZnak(ch) && ch != ' ' ) //pak je to zacatek elem. vyrazu
                { 
                    //najdem tedy první znak ktery je logicky znak, ale není cislo (to muže bejt v el. vyrazu)
                    //to bude konec el. výrazu
                    
                    int konecElVyrazu = vyraz.Length-1;
                    for (int j = i + 1; j < vyraz.Length; j++)
                    {
                        if (Logika.JeToLogickyZnakAleNeCislo(vyraz[j]))
                        {
                            konecElVyrazu = j - 1;
                            break;
                        }
                    }

                    string elVyraz = vyraz.Substring(i, konecElVyrazu - i + 1);
                    string resultElVyrazu = ( VyhodnotElemetarniVyraz(elVyraz) ? "1" : "0" );

                    vyraz = vyraz.Substring(0, i) + resultElVyrazu + vyraz.Substring(konecElVyrazu+1);
                }

                i++;
            }

            //ještě zmizíme mezery
            i = 0;
            while (i < vyraz.Length)
            {
                if (vyraz[i] == ' ')
                {
                    vyraz = vyraz.Substring(0, i) + vyraz.Substring(i + 1);
                }
                i++;
            }


            //MyOutput.PrintIt(vyraz);
            
            int ret = Logika.VyhodnotVyraz(vyraz);
            
            //MyOutput.PrintIt( ret.ToString());

            if (ret == 1)
            {
                return true;
            }
            return false;
        }

        //vyhodnotí elementární logický výraz
        public bool VyhodnotElemetarniVyraz(string vyraz)
        { 
            //elementarni vyraz je tvaru <nějaká barva> ISON <nějaká pozice>
            vyraz = vyraz.Trim();

            string[] slova = vyraz.Split(BILY);

            if (slova.Count() < 3)
            {
                Error(vyraz+" je špatný elementární výraz, je z méně než tří slov.");
                return false;
            }

            if (slova[1].ToUpper() != "ISON")
            {
                Error(vyraz+" je špatně. Byl očekáván elementární výraz, chybí ISON.");
                return false;
            }

            string arg1  = slova[0];
            string arg2 = slova[2];

            List<string> args = new List<string>();
            args.Add(arg1);
            args.Add(arg2);

            eventInterpretChceZkontrolovatRelaciBarvaPozice(this, new StringListEventArgs(args));

            if (inbox == 1)
                return true;
            else if (inbox == 0)
                return false;
            else
            {
                Error("Nepovedlo se vyhodnotit elementární výraz.");
                return true;
            }
        }

        static public string ListStringToString(List<string> lst)
        {
            string ret = "";
            foreach (string a in lst)
            {
                ret = ret + a + " ";
            }

            return ret;
        }

        //po zavolání této metody je provedena aktuální řádka skriptu a pc je patřičně posunut na novou aktualní hodnotu
        //vrací zda stále není konec scriptu
        public bool Exe1Step()
        {
            if (!ExistujeScript())
            { 
                Info("Neexistuje nahraný script.");
                return false;
            }
            else if (DelkaScriptu() <= pc)
            {
                Info("Script už skončil.");
                return false;
            }
            
            string radka = script[pc];
            radka = radka.Trim();

            radka = DosadZaDolaryArgumenty(radka);

            if (radka.Length == 0)
            {
                Info("Prázdná řádka scriptu.");
                pc++;
                OznamDalsiRadek();
                return true;
            }
            
            char prefix = radka[0];
            string[] slova = radka.Split(BILY);

            int numSlov = slova.Count<string>();
            
                
            if (slova[0] == "MACRO")
            {
               Info("vstup do MACRA " + slova[1] + " args: " + ListStringToString(vars) );
            }
            else if (slova[0] == "ENDMACRO")
            {
                if (macroStack.Count > 0)
                {
                    if (varStack.Count > 0)
                    {
                        //vrátíme argumenty makra ze zasobniku protoze prave opoustime dosavadni makro
                        vars = varStack.Pop();
                    }
                    else
                    {
                        Error("Na zásobníku argumentů došli argumenty i když neměli.");
                    }

                    int skok = macroStack.Pop();
                    Info("Konec makra, skoci se na radek " + skok.ToString() + "Argumenty jsou zase: "+ ListStringToString(vars));

                    pc = skok;

                    OznamDalsiRadek();
                    return true;
                }
                else
                {
                    Info("Opousti se makro main. Bude konec.");
                    pc = DelkaScriptu();
                    OznamDalsiRadek();
                    return false;
                }
            }
            else if (slova[0] == "WHILENOT")
            {
                string vyraz = radka.Substring(9);
                bool vyhodnoceniVyrazu = VyhodnotVyraz(vyraz);
                Info(vyhodnoceniVyrazu.ToString() + " (WHILENOT " + vyraz + ")");

                if (!vyhodnoceniVyrazu)
                {
                    //vyhodnocení výrazu proběhlo negativně, tělo whilenotu se tedy provede, proto si poznamenáme kam
                    //skočit až narazíme na endwhile
                    whileStack.Push(pc);
                }
                else
                {
                    int kamSkocit = KamSkocitZNegativnihoWhilu(pc);

                    Info("WHILENOT je pozitivní, bude skoceno na radek " + kamSkocit);

                    if (kamSkocit == -1)
                    {
                        Error("Neukočený WHILENOT.");
                        return false;
                    }

                    pc = kamSkocit;
                    OznamDalsiRadek();
                    return true;
                }

            }
            else if (slova[0] == "ENDWHILE")
            {
                int kamSkocit = whileStack.Pop();
                Info("ENDWHILE: vrací se zpět na příslušný while na řádce "+kamSkocit);

                pc = kamSkocit;
                OznamDalsiRadek();
                return true;
            }
            else if (slova[0] == "IF")
            {
                string vyraz = radka.Substring(3);
                bool vyhodnoceniVyrazu = VyhodnotVyraz(vyraz);
                Info(vyhodnoceniVyrazu.ToString() + " (IF " + vyraz + ")");

                //pokud byl VyrazVyhodnocen pozitivně tak se teď nemusíme starat, ono se to pak zasekne o ELSE(to zpusobí přeskok) nebo ENDIF
                //pokud ale ne, musíme skočit za !správný! (ELSE nebo ENDIF) 
                if (!vyhodnoceniVyrazu)
                {
                    int kamSkocit = KamSkocitZNegativnihoIfu(pc);

                    Info("IF je negativní, bude skoceno na radek " + kamSkocit);

                    if (kamSkocit == -1)
                    {
                        Error("Neukočený IF.");
                        return false;
                    }

                    pc = kamSkocit;
                    OznamDalsiRadek();
                    return true;
                }
            }
            else if (slova[0] == "ELSE")
            {
                //hax používající i zde dobře funkční funkci ale na neco jinyho puvodně
                int kamSkocit = KamSkocitZNegativnihoIfu(pc);

                Info("ELSE bude preskoceno na " + kamSkocit);

                if (kamSkocit == -1)
                {
                    Error("Neukočený IF.");
                    return false;
                }

                pc = kamSkocit;
                OznamDalsiRadek();
                return true;
            }
            else if (slova[0] == "ENDIF")
            {
                Info("ENDIF...");
            }
            else if (prefix == '_')
            {
                int naZas = pc + 1;

                string macro = slova[0].Substring(1);
                Info("Skok na " + macro + ". macroStack.push(" + naZas.ToString() + ")");

                if (!JumpToMacro(macro))
                {
                    Error("Voláno neexistující MACRO " + macro);
                    return false;
                }

                macroStack.Push(naZas);

                //ošetřit argumenty makra:
                 //ulozit stare na zasobnik
                 //a nove nahrat do vars

                varStack.Push(vars);

                List<string> noveArgumenty = new List<string>();
                for (int i = 1; i < slova.Count(); i++)
                {
                    noveArgumenty.Add(slova[i]);
                }

                vars = noveArgumenty;
                //Info("Nove argumenty budou : " + ListStringToString(vars) );

                OznamDalsiRadek();
                return true;
            }
            else if (prefix == '!')
            {
                string tah = radka.Substring(1);
                if (Kostka.IsTah(tah))
                {
                    eventInterpretChceTahnout(this, new StringEventArgs(tah));
                }
                else
                {
                    Error("Tah " + tah + " neexistuje!");
                }

            }
            else if (prefix == '#')
            {
                Info("Komentář..");
            }
            else if (prefix == ':')
            {
                MyOutput.PrintIt(" >>> " + radka.Substring(1));
            }
            else
            {
                Error(prefix.ToString() + " je neexistující předpona.");
            }

            pc++;

            OznamDalsiRadek();
            return true;
        }

        //vrací string obsahující upravený string radka tak že dolary jsou nahrazeny patřičnými hodnotami argumentů
        private string DosadZaDolaryArgumenty(string radka)
        {
            int i = 0;
            while ( i < radka.Length )
            {
                if (radka[i] == '$')
                {
                    int index = int.Parse(radka[i + 1].ToString());

                    if (index >= vars.Count)
                    {
                        Error("Na řádce je neexistující argument makra $" + index);
                    }

                    string novyObsah = vars[index];

                    radka = radka.Substring(0, i) + novyObsah + radka.Substring(i + 2);
                }
                else
                {
                    i++;
                }
            }
            return radka;
        }
    }
    
    //třída uchovávající stav Rubikovy kostky a umožňující manipulace s ní
    public class Kostka
    {
            public static event EventHandler eventKostkaChange;

            const int NUM_STRAN = 6;
            const int NUM_KOSTICEK = 9;
            public const bool CW = true; //posmeru ručiček
            public const bool CC = false;//protismeru

            string[] kodyBarev = { "R", "G", "O", "Y", "B", "W" };

            private int[][] m_kostka = new int[NUM_STRAN][];

             public Kostka()
             {
                for (int i = 0; i < NUM_STRAN; i++)
                {
                    m_kostka[i] = new int[NUM_KOSTICEK];
                }

                int barva = 0;

                for (int strana = 0; strana < NUM_STRAN; strana++)
                {
                    for (int kosticka = 0; kosticka < NUM_KOSTICEK; kosticka++)
                    {
                        m_kostka[strana][kosticka] = barva;//random.Next(0,6);
                    }
                    barva++;
                }
             }

             //metoda provede manipulaci 'jmenoTahu' a na textový výstup napíše jméno této manipulace
             public void Tahni(string jmenoTahu)
             {
                 int[][] rotujKrajeArgs = new int[][]
                {
                    new int[] { 3, 2, 1, 0,    0, 1, 2,    0, 1, 2,    0, 1, 2,    0, 1, 2 },
                    new int[] { 3, 2, 1, 0,    3, 4, 5,    3, 4, 5,    3, 4, 5,    3, 4, 5 },
                    new int[] { 3, 2, 1, 0,    6, 7, 8,    6, 7, 8,    6, 7, 8,    6, 7, 8 },
                    
                    new int[] { 0, 5, 2, 4,    0, 3, 6,    0, 3, 6,    8, 5, 2,    0, 3, 6 },
                    new int[] { 0, 5, 2, 4,    1, 4, 7,    1, 4, 7,    7, 4, 1,    1, 4, 7 },
                    new int[] { 0, 5, 2, 4,    2, 5, 8,    2, 5, 8,    6, 3, 0,    2, 5, 8 },

                    new int[] { 4, 1, 5, 3,    6, 7, 8,    0, 3, 6,    2, 1, 0,    8, 5, 2 },
                    new int[] { 4, 1, 5, 3,    3, 4, 5,    1, 4, 7,    5, 4, 3,    7, 4, 1 },
                    new int[] { 4, 1, 5, 3,    0, 1, 2,    2, 5, 8,    8, 7, 6,    6, 3, 0 }
                };

                 int[][] rotujStranyArgs = new int[][]
                {
                    new int[] { 0,1,2,3 },
                    new int[] { 0,5,2,4 },
                    new int[] { 4,3,5,1 }
                };

                 if (jmenoTahu == "rTO")
                 {
                     RotujStrany(rotujStranyArgs[1], CC);
                     RotujStranu(1, CC);
                     RotujStranu(3, CW);

                     RotujStranu(4, CW);
                     RotujStranu(4, CW);
                     RotujStranu(2, CW);
                     RotujStranu(2, CW);
                 }
                 else if (jmenoTahu == "rBO")
                 {
                     RotujStrany(rotujStranyArgs[1], CW);
                     RotujStranu(1, CW);
                     RotujStranu(3, CC);

                     RotujStranu(5, CW);
                     RotujStranu(5, CW);
                     RotujStranu(2, CW);
                     RotujStranu(2, CW);
                 }
                 else if (jmenoTahu == "rRI")
                 {
                     RotujStrany(rotujStranyArgs[0], CW);
                     RotujStranu(4, CW);
                     RotujStranu(5, CC);
                 }
                 else if (jmenoTahu == "rLE")
                 {
                     RotujStrany(rotujStranyArgs[0], CC);
                     RotujStranu(4, CC);
                     RotujStranu(5, CW);
                 }
                 else if (jmenoTahu == "rCW")
                 {
                     RotujStrany(rotujStranyArgs[2], CW);
                     RotujStranu(0, CW);
                     RotujStranu(2, CC);

                     RotujStranu(1, CW);
                     RotujStranu(3, CW);
                     RotujStranu(4, CW);
                     RotujStranu(5, CW);
                 }
                 else if (jmenoTahu == "rCC")
                 {
                     RotujStrany(rotujStranyArgs[2], CC);
                     RotujStranu(0, CC);
                     RotujStranu(2, CW);

                     RotujStranu(1, CC);
                     RotujStranu(3, CC);
                     RotujStranu(4, CC);
                     RotujStranu(5, CC);
                 }
                 else if (jmenoTahu == "TOaCW" || jmenoTahu == "U")
                 {
                     RotujKraje(rotujKrajeArgs[0], Kostka.CW);
                     RotujStranu(4, Kostka.CW);
                 }
                 else if (jmenoTahu == "TOaCC" || jmenoTahu == "Ui")
                 {
                     RotujKraje(rotujKrajeArgs[0], Kostka.CC);
                     RotujStranu(4, Kostka.CC);
                 }
                 else if (jmenoTahu == "TObCW")
                 {
                     RotujKraje(rotujKrajeArgs[1], Kostka.CW);
                 }
                 else if (jmenoTahu == "TObCC")
                 {
                     RotujKraje(rotujKrajeArgs[1], Kostka.CC);
                 }
                 else if (jmenoTahu == "TOcCW" || jmenoTahu == "Di")
                 {
                     RotujKraje(rotujKrajeArgs[2], Kostka.CW);
                     RotujStranu(5, Kostka.CC);
                 }
                 else if (jmenoTahu == "TOcCC" || jmenoTahu == "D")
                 {
                     RotujKraje(rotujKrajeArgs[2], Kostka.CC);
                     RotujStranu(5, Kostka.CW);
                 }
                 else if (jmenoTahu == "LEaCW" || jmenoTahu == "L")
                 {
                     RotujKraje(rotujKrajeArgs[3], Kostka.CW);
                     RotujStranu(3, Kostka.CW);
                 }
                 else if (jmenoTahu == "LEaCC" || jmenoTahu == "Li")
                 {
                     RotujKraje(rotujKrajeArgs[3], Kostka.CC);
                     RotujStranu(3, Kostka.CC);
                 }
                 else if (jmenoTahu == "LEbCW")
                 {
                     RotujKraje(rotujKrajeArgs[4], Kostka.CW);
                 }
                 else if (jmenoTahu == "LEbCC")
                 {
                     RotujKraje(rotujKrajeArgs[4], Kostka.CC);
                 }
                 else if (jmenoTahu == "LEcCW" || jmenoTahu == "Ri")
                 {
                     RotujKraje(rotujKrajeArgs[5], Kostka.CW);
                     RotujStranu(1, Kostka.CC);
                 }
                 else if (jmenoTahu == "LEcCC" || jmenoTahu == "R")
                 {
                     RotujKraje(rotujKrajeArgs[5], Kostka.CC);
                     RotujStranu(1, Kostka.CW);
                 }
                 else if (jmenoTahu == "FRaCW" || jmenoTahu == "F")
                 {
                     RotujKraje(rotujKrajeArgs[6], Kostka.CW);
                     RotujStranu(0, Kostka.CW);
                 }
                 else if (jmenoTahu == "FRaCC" || jmenoTahu == "Fi")
                 {
                     RotujKraje(rotujKrajeArgs[6], Kostka.CC);
                     RotujStranu(0, Kostka.CC);
                 }
                 else if (jmenoTahu == "FRbCW")
                 {
                     RotujKraje(rotujKrajeArgs[7], Kostka.CW);
                 }
                 else if (jmenoTahu == "FRbCC")
                 {
                     RotujKraje(rotujKrajeArgs[7], Kostka.CC);
                 }
                 else if (jmenoTahu == "FRcCW")
                 {
                     RotujKraje(rotujKrajeArgs[8], Kostka.CW);
                     RotujStranu(2, Kostka.CC);
                 }
                 else if (jmenoTahu == "FRcCC")
                 {
                     RotujKraje(rotujKrajeArgs[8], Kostka.CC);
                     RotujStranu(2, Kostka.CW);
                 }
                 else
                 {
                     MyOutput.PrintIt("ERROR : spatne jmeno tahu ve funkci Tahni()");
                 }

                 MyOutput.PrintIt(jmenoTahu);

                 eventKostkaChange(null, new EventArgs());
             }


             
            public int TestujZdaJeBarevnaKostickaNaPoziciKosticky(string barevnyKod, string pozicovyKod)
             {
                 string sortedBarevnyKod =  SortBarevnyKod( barevnyKod.Substring(1) ) ;
                 string sortedBarvyNaPozici = GetSortedBarvyByKodKosticky( pozicovyKod );
                 if (sortedBarevnyKod == "XXX")
                 {
                     MyOutput.PrintIt("ERROR ve funkci TestujZdaJeBarevnaKostickaNaPoziciKosticky()");
                     return -1;
                 }

                 if (sortedBarevnyKod == sortedBarvyNaPozici)
                 {
                     return 1;
                 }
                 else
                 {
                     return 0;
                 }
                 
             }
            public int TestujZdaJeBarvaNaPozici(string kodBarvy, string kodPozice)
            {
                int barva = GetBarvaByKod(kodBarvy);
                int[] pos = GetPoziceByKod(kodPozice);

                if (barva == -1 || pos[0] == -1)
                    return -1;

                if (barva == GetBarva(pos[0], pos[1]))
                    return 1;
                else
                    return 0;
            }

            #region PosByKosticka_region
            int[][] PosByKosticka = new int[][] { 
                           new int[]    {0,0, 3,2, 4,6},  //FR0
                           new int[]    {0,1, 4,7 },      //FR1
                           new int[]    {0,2, 4,8 ,1,0},  //FR2
                           new int[]    {0,3, 3,5 },      //FR3
                           new int[]    {0,4},            //FR4
                           new int[]    {0,5, 1,3},       //FR5
                           new int[]    {0,6, 3,8, 5,0},  //FR6
                           new int[]    {0,7, 5,1},       //FR7
                           new int[]    {0,8, 5,2, 1,6},  //FR8

                           new int[]    {1,0, 0,2, 4,8},  //RI0
                           new int[]    {1,1, 4,5 },      //RI1
                           new int[]    {1,2, 4,2 ,2,0},  //RI2
                           new int[]    {1,3, 0,5 },      //RI3
                           new int[]    {1,4},            //RI4
                           new int[]    {1,5, 2,3},       //RI5
                           new int[]    {1,6, 0,8, 5,2},  //RI6
                           new int[]    {1,7, 5,5},       //RI7
                           new int[]    {1,8, 5,8, 2,6},  //RI8
                              
                           new int[]    {2,0, 1,2, 4,2},  //BA0
                           new int[]    {2,1, 4,1 },      //BA1
                           new int[]    {2,2, 4,0 ,3,0},  //BA2
                           new int[]    {2,3, 1,5 },      //BA3
                           new int[]    {2,4},            //BA4
                           new int[]    {2,5, 3,3},       //BA5
                           new int[]    {2,6, 1,8, 5,8},  //BA6
                           new int[]    {2,7, 5,7},       //BA7
                           new int[]    {2,8, 5,6, 3,6},  //BA8

                           new int[]    {3,0, 4,0, 2,2},  //LE0
                           new int[]    {3,1, 4,3 },      //LE1
                           new int[]    {3,2, 4,6, 0,0},  //LE2
                           new int[]    {3,3, 2,5 },      //LE3
                           new int[]    {3,4},            //LE4
                           new int[]    {3,5, 0,3},       //LE5
                           new int[]    {3,6, 2,8, 5,6},  //LE6
                           new int[]    {3,7, 5,3},       //LE7
                           new int[]    {3,8, 0,6, 5,0},  //LE8
                              
                           new int[]    {4,0, 3,0, 2,2},  //TO0
                           new int[]    {4,1, 2,1 },      //TO1
                           new int[]    {4,2, 1,2, 2,0},  //TO2
                           new int[]    {4,3, 3,1 },      //TO3
                           new int[]    {4,4},            //TO4
                           new int[]    {4,5, 1,1},       //TO5
                           new int[]    {4,6, 0,0, 3,2},  //TO6
                           new int[]    {4,7, 0,1},       //TO7
                           new int[]    {4,8, 0,2, 1,0},  //TO8

                           new int[]    {5,0, 0,6, 3,8},  //BO0
                           new int[]    {5,1, 0,7 },      //BO1
                           new int[]    {5,2, 0,8, 1,6},  //BO2
                           new int[]    {5,3, 3,7 },      //BO3
                           new int[]    {5,4},            //BO4
                           new int[]    {5,5, 1,7},       //BO5
                           new int[]    {5,6, 2,8, 3,6},  //BO6
                           new int[]    {5,7, 2,7},       //BO7
                           new int[]    {5,8, 1,8, 2,6}   //BO8
                            };
            #endregion
            private string SortBarevnyKod(string kod)
             {
                 List<int> ret = new List<int>();

                 for (int i = 0; i < kod.Length ; i ++)
                 {
                     ret.Add( GetBarvaByKod( kod[i].ToString() ) );
                 }

                 ret.Sort();
                 string str = "";
                 foreach (int b in ret)
                 {
                     str = str + kodyBarev[b];
                 }
                 return str;
             }             
             private string GetSortedBarvyByKodKosticky(string kodKosticky)
             {
                 //uriznem prvni cčko
                 string kodPozice = kodKosticky.Substring(1);
                 int[] pos = GetPoziceByKod(kodPozice);
                 if (pos[0] == -1)
                 {
                     MyOutput.PrintIt("ERROR v GetSortedBarvyByKodKosticky() špatný kod kosticky");
                     return "XXX";
                 }

                 int[] kostickaData = PosByKosticka[ pos[1] + 9*pos[0] ];

                 List<int> ret = new List<int>();

                 for (int i = 0; i < kostickaData.Count(); i+=2)
                 {
                     ret.Add( GetBarva(kostickaData[i], kostickaData[i + 1]) );
                 }

                 ret.Sort();
                 string str = "";
                 foreach (int b in ret)
                 { 
                    str = str + kodyBarev[b];
                 }

                 //MyOutput.PrintIt(str);

                 return str;
             }
             private int GetBarvaByKod(string kodBarvy)
             {
                 for (int i = 0; i < 6; i++)
                 {
                     if (kodyBarev[i] == kodBarvy)
                     {
                         //MyOutput.PrintIt("");
                         return i;
                     }
                 }

                 MyOutput.PrintIt("ERROR v GetBarvaByKod(): kod barvy " + kodBarvy + " neni platny");

                 return -1;
             }
             private int[] GetPoziceByKod(string kodPozice)
             {
                 string[] kodyPozic = { "FR", "RI", "BA", "LE", "TO", "BO" };
                 string kodStrany = kodPozice.Substring(0, 2);
                 for (int i = 0; i < 6; i++)
                 {
                     if (kodyPozic[i] == kodStrany)
                     {
                         int[] ret = new int[2];
                         ret[0] = i;
                         ret[1] = int.Parse( kodPozice[2].ToString() );
                         return ret;
                     }
                 }

                 MyOutput.PrintIt("ERROR v GetPoziceByKod(): kod pozice " + kodPozice + " neni platny");

                 int[] retu = { -1 , -1 };
                 return retu;
             }

             

             public static bool IsTah(string jmeno)
             {
                 string[] jmena = {  "TOaCW","TOaCC", "TObCW","TObCC", "TOcCW","TOcCC",
                                    "LEaCW","LEaCC", "LEbCW","LEbCC", "LEcCW","LEcCC",
                                    "FRaCW","FRaCC", "FRbCW","FRbCC", "FRcCW","FRcCC",
                                    "rBO", "rTO" , "rRI", "rLE", "rCW", "rCC",
                                    "L","R","U","F", "Li","Ri","Ui","Fi","D","Di"};

                 if (jmena.Contains<string>(jmeno)) return true;
                 return false;
             }

            //uvede kostku do Počátečního stavu
            public void ResetKostka()
            {
                Kostka temp = new Kostka();
                m_kostka = temp.m_kostka;
                eventKostkaChange(this, new EventArgs());
            }

            public int GetBarva(int strana, int kosticka)
            {
                return m_kostka[strana][kosticka];
            }

            public void TahniRand()
            {
                Random random = new Random();
                string[] jmena = {  "TOaCW","TOaCC", "TObCW","TObCC", "TOcCW","TOcCC",
                                    "LEaCW","LEaCC", "LEbCW","LEbCC", "LEcCW","LEcCC",
                                    "FRaCW","FRaCC", "FRbCW","FRbCC", "FRcCW","FRcCC",
                                    };
                const int NUM_TAHU = 18;

                Tahni(jmena[random.Next(0,NUM_TAHU-1)]);
            }

            public void Zamichej(int numTahu)
            {
                for (int i = 0; i < numTahu; i++)
                {
                    TahniRand();
                }
            }
            
            //rotuje "plášť" při rotaci vrstev kostky
            private void RotujKraje(int[] args , bool smer) 
            {
                //struktura args: s1 s2 s3 s4   p11 p12 p13  p21 p22 p23  p31 p32 p33  p41 p42 p43  
                //si... cislo strany na ktere se nachazeji čtverce pi1 pi2 pi3

                int[] temp = new int[12]; //sem se ulozej soucasny barvy na rotovanejch pozicich
                int k = 0;
                for (int i = 0; i < 4; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        temp[k] = m_kostka[args[i]][args[4 + 3 * i + j]];
                        k++;
                    }
                }

                k = 0;

                int posun;
                if(smer == CW)
                    posun = 9;
                else
                    posun = 3;

                for (int i = 0; i < 4; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        m_kostka[args[i]][args[4 + 3 * i + j]] = temp[(k + posun )%12] ;
                        k++;
                    }
                }
            }

            //provadí rotaci "vršku" při rotaci vrstvy kostky
            private void RotujStranu(int strana, bool smer)
            {
                int[] cw = { 6, 3, 0, 7, 4, 1, 8, 5, 2 };
                int[] cc = { 2, 5, 8, 1, 4, 7, 0, 3, 6 };

                int[] temp = new int[NUM_KOSTICEK];
                for (int i = 0; i < NUM_KOSTICEK; i++)
                {
                    temp[i] = m_kostka[strana][i];
                }

                if (smer == CW)
                {
                    for (int i = 0; i < NUM_KOSTICEK; i++)
                    {
                        m_kostka[strana][i] = temp[cw[i]];
                    }
                }
                else
                {
                    for (int i = 0; i < NUM_KOSTICEK; i++)
                    {
                        m_kostka[strana][i] = temp[cc[i]];
                    }
                }
            }

            //zrotuje 4 strany kostky, tzn první nahradí druhou, druhou třetí, třetí čtvrtou, čtvrtou první
            //POZOR: musí se počítat s tím že udela pouhou náhradu, tzn při použití této metody pro
            //rotaci celé kostky je potřeba někdy provést rotační operace některých stěn kostky
            public void RotujStrany(int[] strany, bool smer)
            {
                if (smer == CC)
                {
                    Array.Reverse(strany);
                }

                int[] temp = m_kostka[strany[0]];
                for (int i = 1; i < 4; i++)
                {
                    m_kostka[strany[i-1]] = m_kostka[strany[i]];

                }
                m_kostka[strany[3]] = temp;
            }
    }

    //třída sloužící pro vykreslení kostky
    public class KostkaImg
        {
            Graphics gr;
            Kostka kostka;

            const int STRANA = 50;
            const int START_X = 150 - 80;
            const int START_Y = 150;
            const double POSUN3D = 1.3;

            public KostkaImg(Graphics gr, Kostka kostka)
            {
                this.gr = gr;
                this.kostka = kostka;
            }

            static Color[] barvy = { Color.Red, Color.Green, Color.Orange, Color.Yellow, Color.Blue, Color.White };
        
            public void Draw3D()
            {
                for (int y = 0; y < 3; y++)
                {
                    for (int x = 0; x < 3; x++)
                    {
                        DrawPredniVybarveni(x, y, barvy[kostka.GetBarva(0, x + y * 3)],START_X,START_Y,STRANA);
                        DrawHorniVybarveni(x, 2-y, barvy[kostka.GetBarva(4, x + y * 3)]);
                        DrawBocniVybarveni(x, y, barvy[kostka.GetBarva(1, x + y * 3)]);
                    }
                }

                //DrawHorniVybarveni(0, 0, Color.Black );

                DrawMrizka();
            }

            public void Draw2D(int startX,int startY)
            {

                const int stranaSize = 10;
                const int mezirkaMeziStranama = 5;

                int[] sX = { 1, 2, 3, 0, 1, 1};
                int[] sY = { 1, 1, 1, 1, 0, 2};

                for (int i = 0; i < 6; i++)
                {
                    DrawStrana2D(i, startX + 3 * stranaSize * sX[i] + sX[i] * mezirkaMeziStranama,
                                    startY + 3 * stranaSize * sY[i] + sY[i] * mezirkaMeziStranama, 
                                 stranaSize);
                }
            }

            private void DrawStrana2D(int strana,int startX, int startY, int stranaSize)
            {
                for (int y = 0; y < 3; y++)
                {
                    for (int x = 0; x < 3; x++)
                    {
                        DrawPredniVybarveni(x, y, barvy[kostka.GetBarva(strana, x + y * 3)], startX, startY, stranaSize);
                    }
                }
                DrawMrizkaProStranu2D(startX, startY, stranaSize);
            }

            private void DrawMrizkaProStranu2D(int startX, int startY, int stranaSize)
            {
                int[] bodyX = { 0, 1, 2, 3, 0, 1, 2, 3 };
                int[] bodyY = { 0, 0, 0, 0, 3, 3, 3, 3 };

                Pen pero = new Pen(Color.Black, 2);

                for (int i = 0; i < 4; i++)
                {
                    gr.DrawLine(pero, startX + stranaSize * bodyX[i],
                                      startY + stranaSize * bodyY[i],
                                      startX + stranaSize * bodyX[i + 4],
                                      startY + stranaSize * bodyY[i + 4]);
                    
                    gr.DrawLine(pero, startX + stranaSize * bodyY[i],
                                      startY + stranaSize * bodyX[i],
                                      startX + stranaSize * bodyY[i + 4],
                                      startY + stranaSize * bodyX[i + 4]);
                    
                }
            }


            private void DrawPredniVybarveni(int x, int y, Color col,int START_X,int START_Y,int STRANA)
            {
                int[] plochyX = { 0, 1, 1, 0 };
                int[] plochyY = { 0, 0, 1, 1 };

                int numPloch = 1;
                SolidBrush sb = new SolidBrush(col);

                for (int i = 0; i < numPloch; i++)
                {
                    Point[] points = { new Point(x*STRANA+START_X+STRANA*plochyX[0],y*STRANA+START_Y+STRANA*plochyY[0]),
                                   new Point(x*STRANA+START_X+STRANA*plochyX[1],y*STRANA+START_Y+STRANA*plochyY[1]),
                                   new Point(x*STRANA+START_X+STRANA*plochyX[2],y*STRANA+START_Y+STRANA*plochyY[2]),
                                   new Point(x*STRANA+START_X+STRANA*plochyX[3],y*STRANA+START_Y+STRANA*plochyY[3])
                                  };

                    gr.FillPolygon(sb, points);
                }
            }

            private void DrawBocniVybarveni(int x, int y, Color col)
            {
                int kousek = (int)(STRANA * POSUN3D / 3.0);

                int[] plochyX = { 3 * STRANA, 3 * STRANA, 3 * STRANA + kousek, 3 * STRANA + kousek };
                int[] plochyY = { 0, STRANA, STRANA - kousek, -kousek };


                int numPloch = 1;
                SolidBrush sb = new SolidBrush(col);

                for (int i = 0; i < numPloch; i++)
                {
                    Point[] points = { new Point(x*kousek+START_X+plochyX[0],-x*kousek+y*STRANA+START_Y+plochyY[0]),
                                   new Point(x*kousek+START_X+plochyX[1],-x*kousek+y*STRANA+START_Y+plochyY[1]),
                                   new Point(x*kousek+START_X+plochyX[2],-x*kousek+y*STRANA+START_Y+plochyY[2]),
                                   new Point(x*kousek+START_X+plochyX[3],-x*kousek+y*STRANA+START_Y+plochyY[3])
                                  };

                    gr.FillPolygon(sb, points);
                }
            }

            private void DrawHorniVybarveni(int x, int y, Color col)
            {
                int kousek = (int)(STRANA * POSUN3D / 3.0);

                int[] plochyX = { 0, STRANA, STRANA + kousek, kousek };
                int[] plochyY = { 0, 0, -kousek, -kousek };


                int numPloch = 1;
                SolidBrush sb = new SolidBrush(col);

                for (int i = 0; i < numPloch; i++)
                {
                    Point[] points = { new Point(x*STRANA+y*kousek+START_X+plochyX[0],-y*kousek+START_Y+plochyY[0]),
                                   new Point(x*STRANA+y*kousek+START_X+plochyX[1],-y*kousek+START_Y+plochyY[1]),
                                   new Point(x*STRANA+y*kousek+START_X+plochyX[2],-y*kousek+START_Y+plochyY[2]),
                                   new Point(x*STRANA+y*kousek+START_X+plochyX[3],-y*kousek+START_Y+plochyY[3])
                                  };

                    gr.FillPolygon(sb, points);
                }
            }

            private void DrawMrizka()
            {
                int[] bodyX = { 0, 1, 2, 3, 0, 1, 2, 3 };
                int[] bodyY = { 0, 0, 0, 0, 3, 3, 3, 3 };

                Pen pero = new Pen(Color.Black, 5);

                for (int i = 0; i < 4; i++)
                {
                    gr.DrawLine(pero, START_X + STRANA * bodyX[i],
                                      START_Y + STRANA * bodyY[i],
                                      START_X + STRANA * bodyX[i + 4],
                                      START_Y + STRANA * bodyY[i + 4]);

                    gr.DrawLine(pero, START_X + STRANA * bodyY[i],
                                      START_Y + STRANA * bodyX[i],
                                      START_X + STRANA * bodyY[i + 4],
                                      START_Y + STRANA * bodyX[i + 4]);

                    gr.DrawLine(pero, START_X + STRANA * bodyY[i + 4],
                                     START_Y + STRANA * bodyX[i + 4],
                                     START_X + STRANA * bodyY[i + 4] + (int)(STRANA * POSUN3D),
                                     START_Y + STRANA * bodyX[i + 4] - (int)(STRANA * POSUN3D));
                }
                for (int i = 1; i < 4; i++)
                {
                    gr.DrawLine(pero, START_X + STRANA * bodyX[3] + (int)(i / 3.0 * STRANA * POSUN3D),
                                      START_Y + STRANA * bodyY[3] - (int)(i / 3.0 * STRANA * POSUN3D),
                                      START_X + STRANA * bodyX[7] + (int)(i / 3.0 * STRANA * POSUN3D),
                                      START_Y + STRANA * bodyY[7] - (int)(i / 3.0 * STRANA * POSUN3D));

                    gr.DrawLine(pero, START_X + STRANA * bodyY[0] + (int)(i / 3.0 * STRANA * POSUN3D),
                                      START_Y + STRANA * bodyX[0] - (int)(i / 3.0 * STRANA * POSUN3D),
                                      START_X + STRANA * bodyY[4] + (int)(i / 3.0 * STRANA * POSUN3D),
                                      START_Y + STRANA * bodyX[4] - (int)(i / 3.0 * STRANA * POSUN3D));

                    gr.DrawLine(pero, START_X + STRANA * bodyX[i - 1],
                                         START_Y + STRANA * bodyY[0],
                                         START_X + STRANA * bodyX[i - 1] + (int)(STRANA * POSUN3D),
                                         START_Y + STRANA * bodyY[0] - (int)(STRANA * POSUN3D));
                }
            }
        }

    //třída obsahující metody pro práci s logickými výrazy
    public class Logika
    {
        static public bool JeToLogickyZnak(char ch)
        {
            if (ch == '(' || ch == ')' || ch == '0' || ch == '1' || ch == 'x' || ch == '&' || ch == '|')
            {
                return true;
            }
            return false;
        }

        static public bool JeToLogickyZnakAleNeCislo(char ch)
        {
            if (ch == '(' || ch == ')' || ch == 'x' || ch == '&' || ch == '|')
            {
                return true;
            }
            return false;
        }

        static public string OsekatVnejsiZavorky(string str)
        {
            if (str.Length <= 1)
                return str;

            int zavorky = 0 ;
            for (int i = 0; i < str.Length; i++)
            {
                if ( str[i] == '(' )
                {
                    zavorky++;
                }
                else if (str[i] == ')')
                {
                    zavorky--;
                }

                if (zavorky == 0 && i < str.Length-1 )
                {
                    return str;
                }
                else if (zavorky == 0 && i == str.Length - 1)
                {
                    return OsekatVnejsiZavorky( str.Substring( 1 , str.Length - 2 ) );
                }
            }

            //MyOutput.PrintIt("LOGIKA ERROR Blbě uzávorkováno.");
            return "XXX";
        }

        static public int NajitVhodnyOperator(string str)
        { 
            int zavorky = 0 ;
            int rekordPos = -1 ;
            int rekordVal = int.MaxValue;

            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == '(')
                {
                    zavorky++;
                }
                else if (str[i] == ')')
                {
                    zavorky--;
                }

                if (str[i] == '&' || str[i] == '|' || str[i] == 'x' )
                {
                    if (zavorky <= rekordVal)
                    {
                        rekordVal = zavorky;
                        rekordPos = i;
                    }
                }

            }

            return rekordPos;
        }

        static public bool Xor(bool a, bool b)
        {
            if (a == true)
            {
                if (b == true) return false;
                return true;
            }
            else //a == false
            {
                if (b == true) return true;
                return false;
            }
        }
        
        static public int VyhodnotVyraz(string str)
        { 
            //nejdřív osekat vnější závorky
            str = OsekatVnejsiZavorky(str);
            if (str == "XXX")
            {
                return -1;
            }

            //najdeme nejpravější nejmin zanorenej operator
            int posOperatoru = NajitVhodnyOperator(str);
            if (posOperatoru == -1)
            {
                if (str.Length != 1)
                    return -1;

                //MyOutput.PrintIt(str);
                //nebyl nalezen operator, takže pokud je vyrazkorektni tak už zbyla jen hodnota
                return int.Parse(str);
            }

            //MyOutput.PrintIt(str);
            //MyOutput.PrintIt(posOperatoru.ToString() );

            string str1 = str.Substring(0,posOperatoru);
            string str2 = str.Substring(posOperatoru+1);

            if (str[posOperatoru] == '&' )
                return ((VyhodnotVyraz(str1) == 1) && (VyhodnotVyraz(str2) == 1) == true ? 1 : 0);
            else if (str[posOperatoru] == '|')
                return ((VyhodnotVyraz(str1) == 1) || (VyhodnotVyraz(str2) == 1) == true ? 1 : 0);
            else if (str[posOperatoru] == 'x')
                return (Xor((VyhodnotVyraz(str1) == 1), (VyhodnotVyraz(str2) == 1)) == true ? 1 : 0);


            return -1;        
            
        }
    }

}