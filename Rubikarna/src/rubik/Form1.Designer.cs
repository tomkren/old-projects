﻿/*
Rubikarna
Tomáš Křen
Zápočtový program
Programování II - PRG031
letní semestr 2007/8
*/
namespace rubik
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelOutput = new System.Windows.Forms.Label();
            this.textBoxInput = new System.Windows.Forms.TextBox();
            this.labelInput = new System.Windows.Forms.Label();
            this.listBoxOutput = new System.Windows.Forms.ListBox();
            this.buttonTest = new System.Windows.Forms.Button();
            this.buttonTOaCW = new System.Windows.Forms.Button();
            this.buttonTOaCC = new System.Windows.Forms.Button();
            this.buttonTObCC = new System.Windows.Forms.Button();
            this.buttonTObCW = new System.Windows.Forms.Button();
            this.buttonTOcCC = new System.Windows.Forms.Button();
            this.buttonTOcCW = new System.Windows.Forms.Button();
            this.buttonLEaCW = new System.Windows.Forms.Button();
            this.buttonLEaCC = new System.Windows.Forms.Button();
            this.buttonLEbCW = new System.Windows.Forms.Button();
            this.buttonLEbCC = new System.Windows.Forms.Button();
            this.buttonLEcCW = new System.Windows.Forms.Button();
            this.buttonLEcCC = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.buttonFRaCW = new System.Windows.Forms.Button();
            this.buttonFRaCC = new System.Windows.Forms.Button();
            this.buttonFRbCW = new System.Windows.Forms.Button();
            this.buttonFRbCC = new System.Windows.Forms.Button();
            this.buttonFRcCW = new System.Windows.Forms.Button();
            this.buttonFRcCC = new System.Windows.Forms.Button();
            this.buttonReset = new System.Windows.Forms.Button();
            this.buttonRandTah = new System.Windows.Forms.Button();
            this.buttonShuffle = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonRRI = new System.Windows.Forms.Button();
            this.buttonRTO = new System.Windows.Forms.Button();
            this.buttonRBO = new System.Windows.Forms.Button();
            this.buttonRLE = new System.Windows.Forms.Button();
            this.buttonRCC = new System.Windows.Forms.Button();
            this.buttonRCW = new System.Windows.Forms.Button();
            this.buttonLoadAsScript = new System.Windows.Forms.Button();
            this.textBoxShuffleSize = new System.Windows.Forms.TextBox();
            this.button1Step = new System.Windows.Forms.Button();
            this.buttonLoadScript = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.buttonSaveInput = new System.Windows.Forms.Button();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.buttonExe = new System.Windows.Forms.Button();
            this.labelDalsiRadka = new System.Windows.Forms.Label();
            this.textBoxNextLine = new System.Windows.Forms.TextBox();
            this.labelNextLineNum = new System.Windows.Forms.Label();
            this.textBoxTest = new System.Windows.Forms.TextBox();
            this.checkBoxShowInfo = new System.Windows.Forms.CheckBox();
            this.labelKodRublan = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.panel1.Location = new System.Drawing.Point(16, 37);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(344, 11);
            this.panel1.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.panel3.Location = new System.Drawing.Point(16, 360);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(344, 10);
            this.panel3.TabIndex = 2;
            // 
            // labelOutput
            // 
            this.labelOutput.AutoSize = true;
            this.labelOutput.BackColor = System.Drawing.Color.DarkKhaki;
            this.labelOutput.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelOutput.Location = new System.Drawing.Point(758, 22);
            this.labelOutput.Name = "labelOutput";
            this.labelOutput.Size = new System.Drawing.Size(64, 16);
            this.labelOutput.TabIndex = 4;
            this.labelOutput.Text = "Output:";
            // 
            // textBoxInput
            // 
            this.textBoxInput.AcceptsTab = true;
            this.textBoxInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(224)))), ((int)(((byte)(197)))));
            this.textBoxInput.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxInput.Location = new System.Drawing.Point(379, 40);
            this.textBoxInput.Multiline = true;
            this.textBoxInput.Name = "textBoxInput";
            this.textBoxInput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxInput.Size = new System.Drawing.Size(368, 454);
            this.textBoxInput.TabIndex = 5;
            this.textBoxInput.WordWrap = false;
            // 
            // labelInput
            // 
            this.labelInput.AutoSize = true;
            this.labelInput.BackColor = System.Drawing.Color.DarkKhaki;
            this.labelInput.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelInput.Location = new System.Drawing.Point(378, 21);
            this.labelInput.Name = "labelInput";
            this.labelInput.Size = new System.Drawing.Size(160, 16);
            this.labelInput.TabIndex = 6;
            this.labelInput.Text = "Input             :";
            // 
            // listBoxOutput
            // 
            this.listBoxOutput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(224)))), ((int)(((byte)(197)))));
            this.listBoxOutput.FormattingEnabled = true;
            this.listBoxOutput.HorizontalScrollbar = true;
            this.listBoxOutput.Location = new System.Drawing.Point(757, 42);
            this.listBoxOutput.Name = "listBoxOutput";
            this.listBoxOutput.Size = new System.Drawing.Size(223, 446);
            this.listBoxOutput.TabIndex = 9;
            // 
            // buttonTest
            // 
            this.buttonTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonTest.Location = new System.Drawing.Point(445, 500);
            this.buttonTest.Name = "buttonTest";
            this.buttonTest.Size = new System.Drawing.Size(77, 34);
            this.buttonTest.TabIndex = 10;
            this.buttonTest.Text = "RUN macro";
            this.buttonTest.UseVisualStyleBackColor = true;
            this.buttonTest.Click += new System.EventHandler(this.buttonTest_Click);
            // 
            // buttonTOaCW
            // 
            this.buttonTOaCW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonTOaCW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTOaCW.Location = new System.Drawing.Point(27, 162);
            this.buttonTOaCW.Name = "buttonTOaCW";
            this.buttonTOaCW.Size = new System.Drawing.Size(17, 21);
            this.buttonTOaCW.TabIndex = 11;
            this.buttonTOaCW.Text = "<";
            this.buttonTOaCW.UseVisualStyleBackColor = false;
            this.buttonTOaCW.Click += new System.EventHandler(this.buttonTOaCW_Click);
            // 
            // buttonTOaCC
            // 
            this.buttonTOaCC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonTOaCC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTOaCC.Location = new System.Drawing.Point(46, 162);
            this.buttonTOaCC.Name = "buttonTOaCC";
            this.buttonTOaCC.Size = new System.Drawing.Size(17, 21);
            this.buttonTOaCC.TabIndex = 12;
            this.buttonTOaCC.Text = ">";
            this.buttonTOaCC.UseVisualStyleBackColor = false;
            this.buttonTOaCC.Click += new System.EventHandler(this.buttonTOaCC_Click);
            // 
            // buttonTObCC
            // 
            this.buttonTObCC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonTObCC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTObCC.Location = new System.Drawing.Point(46, 212);
            this.buttonTObCC.Name = "buttonTObCC";
            this.buttonTObCC.Size = new System.Drawing.Size(17, 21);
            this.buttonTObCC.TabIndex = 14;
            this.buttonTObCC.Text = ">";
            this.buttonTObCC.UseVisualStyleBackColor = false;
            this.buttonTObCC.Click += new System.EventHandler(this.buttonTObCC_Click);
            // 
            // buttonTObCW
            // 
            this.buttonTObCW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonTObCW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTObCW.Location = new System.Drawing.Point(27, 212);
            this.buttonTObCW.Name = "buttonTObCW";
            this.buttonTObCW.Size = new System.Drawing.Size(17, 21);
            this.buttonTObCW.TabIndex = 13;
            this.buttonTObCW.Text = "<";
            this.buttonTObCW.UseVisualStyleBackColor = false;
            this.buttonTObCW.Click += new System.EventHandler(this.buttonTObCW_Click);
            // 
            // buttonTOcCC
            // 
            this.buttonTOcCC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonTOcCC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTOcCC.Location = new System.Drawing.Point(46, 266);
            this.buttonTOcCC.Name = "buttonTOcCC";
            this.buttonTOcCC.Size = new System.Drawing.Size(17, 21);
            this.buttonTOcCC.TabIndex = 16;
            this.buttonTOcCC.Text = ">";
            this.buttonTOcCC.UseVisualStyleBackColor = false;
            this.buttonTOcCC.Click += new System.EventHandler(this.buttonTOcCC_Click);
            // 
            // buttonTOcCW
            // 
            this.buttonTOcCW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonTOcCW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTOcCW.Location = new System.Drawing.Point(27, 266);
            this.buttonTOcCW.Name = "buttonTOcCW";
            this.buttonTOcCW.Size = new System.Drawing.Size(17, 21);
            this.buttonTOcCW.TabIndex = 15;
            this.buttonTOcCW.Text = "<";
            this.buttonTOcCW.UseVisualStyleBackColor = false;
            this.buttonTOcCW.Click += new System.EventHandler(this.buttonTOcCW_Click);
            // 
            // buttonLEaCW
            // 
            this.buttonLEaCW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonLEaCW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLEaCW.Location = new System.Drawing.Point(82, 332);
            this.buttonLEaCW.Name = "buttonLEaCW";
            this.buttonLEaCW.Size = new System.Drawing.Size(27, 21);
            this.buttonLEaCW.TabIndex = 18;
            this.buttonLEaCW.Text = "\\/";
            this.buttonLEaCW.UseVisualStyleBackColor = false;
            this.buttonLEaCW.Click += new System.EventHandler(this.buttonLEaCW_Click);
            // 
            // buttonLEaCC
            // 
            this.buttonLEaCC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonLEaCC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLEaCC.Location = new System.Drawing.Point(81, 309);
            this.buttonLEaCC.Name = "buttonLEaCC";
            this.buttonLEaCC.Size = new System.Drawing.Size(28, 21);
            this.buttonLEaCC.TabIndex = 17;
            this.buttonLEaCC.Text = "/\\";
            this.buttonLEaCC.UseVisualStyleBackColor = false;
            this.buttonLEaCC.Click += new System.EventHandler(this.buttonLEaCC_Click);
            // 
            // buttonLEbCW
            // 
            this.buttonLEbCW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonLEbCW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLEbCW.Location = new System.Drawing.Point(132, 332);
            this.buttonLEbCW.Name = "buttonLEbCW";
            this.buttonLEbCW.Size = new System.Drawing.Size(27, 21);
            this.buttonLEbCW.TabIndex = 20;
            this.buttonLEbCW.Text = "\\/";
            this.buttonLEbCW.UseVisualStyleBackColor = false;
            this.buttonLEbCW.Click += new System.EventHandler(this.buttonLEbCW_Click);
            // 
            // buttonLEbCC
            // 
            this.buttonLEbCC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonLEbCC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLEbCC.Location = new System.Drawing.Point(131, 309);
            this.buttonLEbCC.Name = "buttonLEbCC";
            this.buttonLEbCC.Size = new System.Drawing.Size(28, 21);
            this.buttonLEbCC.TabIndex = 19;
            this.buttonLEbCC.Text = "/\\";
            this.buttonLEbCC.UseVisualStyleBackColor = false;
            this.buttonLEbCC.Click += new System.EventHandler(this.buttonLEbCC_Click);
            // 
            // buttonLEcCW
            // 
            this.buttonLEcCW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonLEcCW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLEcCW.Location = new System.Drawing.Point(186, 332);
            this.buttonLEcCW.Name = "buttonLEcCW";
            this.buttonLEcCW.Size = new System.Drawing.Size(27, 21);
            this.buttonLEcCW.TabIndex = 22;
            this.buttonLEcCW.Text = "\\/";
            this.buttonLEcCW.UseVisualStyleBackColor = false;
            this.buttonLEcCW.Click += new System.EventHandler(this.buttonLEcCW_Click);
            // 
            // buttonLEcCC
            // 
            this.buttonLEcCC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonLEcCC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLEcCC.Location = new System.Drawing.Point(185, 309);
            this.buttonLEcCC.Name = "buttonLEcCC";
            this.buttonLEcCC.Size = new System.Drawing.Size(28, 21);
            this.buttonLEcCC.TabIndex = 21;
            this.buttonLEcCC.Text = "/\\";
            this.buttonLEcCC.UseVisualStyleBackColor = false;
            this.buttonLEcCC.Click += new System.EventHandler(this.buttonLEcCC_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.panel5.Location = new System.Drawing.Point(347, 37);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(13, 457);
            this.panel5.TabIndex = 3;
            // 
            // buttonFRaCW
            // 
            this.buttonFRaCW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonFRaCW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFRaCW.Location = new System.Drawing.Point(275, 285);
            this.buttonFRaCW.Name = "buttonFRaCW";
            this.buttonFRaCW.Size = new System.Drawing.Size(29, 21);
            this.buttonFRaCW.TabIndex = 24;
            this.buttonFRaCW.Text = "\\/";
            this.buttonFRaCW.UseVisualStyleBackColor = false;
            this.buttonFRaCW.Click += new System.EventHandler(this.buttonFRaCW_Click);
            // 
            // buttonFRaCC
            // 
            this.buttonFRaCC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonFRaCC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFRaCC.Location = new System.Drawing.Point(243, 285);
            this.buttonFRaCC.Name = "buttonFRaCC";
            this.buttonFRaCC.Size = new System.Drawing.Size(30, 21);
            this.buttonFRaCC.TabIndex = 23;
            this.buttonFRaCC.Text = "/\\";
            this.buttonFRaCC.UseVisualStyleBackColor = false;
            this.buttonFRaCC.Click += new System.EventHandler(this.buttonFRaCC_Click);
            // 
            // buttonFRbCW
            // 
            this.buttonFRbCW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonFRbCW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFRbCW.Location = new System.Drawing.Point(292, 263);
            this.buttonFRbCW.Name = "buttonFRbCW";
            this.buttonFRbCW.Size = new System.Drawing.Size(29, 21);
            this.buttonFRbCW.TabIndex = 26;
            this.buttonFRbCW.Text = "\\/";
            this.buttonFRbCW.UseVisualStyleBackColor = false;
            this.buttonFRbCW.Click += new System.EventHandler(this.buttonFRbCW_Click);
            // 
            // buttonFRbCC
            // 
            this.buttonFRbCC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonFRbCC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFRbCC.Location = new System.Drawing.Point(260, 263);
            this.buttonFRbCC.Name = "buttonFRbCC";
            this.buttonFRbCC.Size = new System.Drawing.Size(30, 21);
            this.buttonFRbCC.TabIndex = 25;
            this.buttonFRbCC.Text = "/\\";
            this.buttonFRbCC.UseVisualStyleBackColor = false;
            this.buttonFRbCC.Click += new System.EventHandler(this.buttonFRbCC_Click);
            // 
            // buttonFRcCW
            // 
            this.buttonFRcCW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonFRcCW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFRcCW.Location = new System.Drawing.Point(315, 241);
            this.buttonFRcCW.Name = "buttonFRcCW";
            this.buttonFRcCW.Size = new System.Drawing.Size(29, 21);
            this.buttonFRcCW.TabIndex = 28;
            this.buttonFRcCW.Text = "\\/";
            this.buttonFRcCW.UseVisualStyleBackColor = false;
            this.buttonFRcCW.Click += new System.EventHandler(this.buttonFRcCW_Click);
            // 
            // buttonFRcCC
            // 
            this.buttonFRcCC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonFRcCC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFRcCC.Location = new System.Drawing.Point(283, 241);
            this.buttonFRcCC.Name = "buttonFRcCC";
            this.buttonFRcCC.Size = new System.Drawing.Size(30, 21);
            this.buttonFRcCC.TabIndex = 27;
            this.buttonFRcCC.Text = "/\\";
            this.buttonFRcCC.UseVisualStyleBackColor = false;
            this.buttonFRcCC.Click += new System.EventHandler(this.buttonFRcCC_Click);
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(14, 12);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(58, 21);
            this.buttonReset.TabIndex = 29;
            this.buttonReset.Text = "RESET";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // buttonRandTah
            // 
            this.buttonRandTah.Location = new System.Drawing.Point(206, 10);
            this.buttonRandTah.Name = "buttonRandTah";
            this.buttonRandTah.Size = new System.Drawing.Size(60, 21);
            this.buttonRandTah.TabIndex = 30;
            this.buttonRandTah.Text = "RandTah";
            this.buttonRandTah.UseVisualStyleBackColor = true;
            this.buttonRandTah.Click += new System.EventHandler(this.buttonRandTah_Click);
            // 
            // buttonShuffle
            // 
            this.buttonShuffle.Location = new System.Drawing.Point(272, 10);
            this.buttonShuffle.Name = "buttonShuffle";
            this.buttonShuffle.Size = new System.Drawing.Size(49, 21);
            this.buttonShuffle.TabIndex = 31;
            this.buttonShuffle.Text = "shuffle";
            this.buttonShuffle.UseVisualStyleBackColor = true;
            this.buttonShuffle.Click += new System.EventHandler(this.buttonShuffle_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(926, 12);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(52, 23);
            this.buttonClear.TabIndex = 32;
            this.buttonClear.Text = "CLEAR";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.panel4.Location = new System.Drawing.Point(12, 483);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(344, 11);
            this.panel4.TabIndex = 33;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.panel2.Location = new System.Drawing.Point(12, 37);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(13, 457);
            this.panel2.TabIndex = 4;
            // 
            // buttonRRI
            // 
            this.buttonRRI.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonRRI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRRI.Location = new System.Drawing.Point(30, 79);
            this.buttonRRI.Name = "buttonRRI";
            this.buttonRRI.Size = new System.Drawing.Size(27, 28);
            this.buttonRRI.TabIndex = 35;
            this.buttonRRI.Text = "<";
            this.buttonRRI.UseVisualStyleBackColor = false;
            this.buttonRRI.Click += new System.EventHandler(this.buttonRRI_Click);
            // 
            // buttonRTO
            // 
            this.buttonRTO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonRTO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRTO.Location = new System.Drawing.Point(56, 106);
            this.buttonRTO.Name = "buttonRTO";
            this.buttonRTO.Size = new System.Drawing.Size(27, 28);
            this.buttonRTO.TabIndex = 37;
            this.buttonRTO.Text = "\\/";
            this.buttonRTO.UseVisualStyleBackColor = false;
            this.buttonRTO.Click += new System.EventHandler(this.buttonRTO_Click);
            // 
            // buttonRBO
            // 
            this.buttonRBO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonRBO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRBO.Location = new System.Drawing.Point(56, 52);
            this.buttonRBO.Name = "buttonRBO";
            this.buttonRBO.Size = new System.Drawing.Size(27, 28);
            this.buttonRBO.TabIndex = 39;
            this.buttonRBO.Text = "/\\";
            this.buttonRBO.UseVisualStyleBackColor = false;
            this.buttonRBO.Click += new System.EventHandler(this.buttonRBO_Click);
            // 
            // buttonRLE
            // 
            this.buttonRLE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonRLE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRLE.Location = new System.Drawing.Point(82, 79);
            this.buttonRLE.Name = "buttonRLE";
            this.buttonRLE.Size = new System.Drawing.Size(27, 28);
            this.buttonRLE.TabIndex = 38;
            this.buttonRLE.Text = ">";
            this.buttonRLE.UseVisualStyleBackColor = false;
            this.buttonRLE.Click += new System.EventHandler(this.buttonRLE_Click);
            // 
            // buttonRCC
            // 
            this.buttonRCC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonRCC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRCC.Location = new System.Drawing.Point(314, 126);
            this.buttonRCC.Name = "buttonRCC";
            this.buttonRCC.Size = new System.Drawing.Size(27, 28);
            this.buttonRCC.TabIndex = 41;
            this.buttonRCC.Text = "/\\";
            this.buttonRCC.UseVisualStyleBackColor = false;
            this.buttonRCC.Click += new System.EventHandler(this.buttonRCC_Click);
            // 
            // buttonRCW
            // 
            this.buttonRCW.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(161)))), ((int)(((byte)(105)))));
            this.buttonRCW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRCW.Location = new System.Drawing.Point(314, 164);
            this.buttonRCW.Name = "buttonRCW";
            this.buttonRCW.Size = new System.Drawing.Size(27, 28);
            this.buttonRCW.TabIndex = 40;
            this.buttonRCW.Text = "\\/";
            this.buttonRCW.UseVisualStyleBackColor = false;
            this.buttonRCW.Click += new System.EventHandler(this.buttonRCW_Click);
            // 
            // buttonLoadAsScript
            // 
            this.buttonLoadAsScript.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonLoadAsScript.Location = new System.Drawing.Point(445, 538);
            this.buttonLoadAsScript.Name = "buttonLoadAsScript";
            this.buttonLoadAsScript.Size = new System.Drawing.Size(77, 21);
            this.buttonLoadAsScript.TabIndex = 42;
            this.buttonLoadAsScript.Text = "start debugging";
            this.buttonLoadAsScript.UseVisualStyleBackColor = true;
            this.buttonLoadAsScript.Click += new System.EventHandler(this.buttonLoadAsScript_Click);
            // 
            // textBoxShuffleSize
            // 
            this.textBoxShuffleSize.AcceptsReturn = true;
            this.textBoxShuffleSize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(224)))), ((int)(((byte)(197)))));
            this.textBoxShuffleSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxShuffleSize.Location = new System.Drawing.Point(324, 11);
            this.textBoxShuffleSize.Name = "textBoxShuffleSize";
            this.textBoxShuffleSize.Size = new System.Drawing.Size(36, 20);
            this.textBoxShuffleSize.TabIndex = 43;
            this.textBoxShuffleSize.Text = "42";
            // 
            // button1Step
            // 
            this.button1Step.Location = new System.Drawing.Point(379, 537);
            this.button1Step.Name = "button1Step";
            this.button1Step.Size = new System.Drawing.Size(60, 22);
            this.button1Step.TabIndex = 44;
            this.button1Step.Text = "1STEP";
            this.button1Step.UseVisualStyleBackColor = true;
            this.button1Step.Click += new System.EventHandler(this.button1Step_Click);
            // 
            // buttonLoadScript
            // 
            this.buttonLoadScript.Location = new System.Drawing.Point(659, 502);
            this.buttonLoadScript.Name = "buttonLoadScript";
            this.buttonLoadScript.Size = new System.Drawing.Size(83, 23);
            this.buttonLoadScript.TabIndex = 45;
            this.buttonLoadScript.Text = "LOAD";
            this.buttonLoadScript.UseVisualStyleBackColor = true;
            this.buttonLoadScript.Click += new System.EventHandler(this.buttonLoadScript_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // buttonSaveInput
            // 
            this.buttonSaveInput.Location = new System.Drawing.Point(659, 528);
            this.buttonSaveInput.Name = "buttonSaveInput";
            this.buttonSaveInput.Size = new System.Drawing.Size(83, 23);
            this.buttonSaveInput.TabIndex = 46;
            this.buttonSaveInput.Text = "SAVE AS";
            this.buttonSaveInput.UseVisualStyleBackColor = true;
            this.buttonSaveInput.Click += new System.EventHandler(this.buttonSaveInput_Click);
            // 
            // buttonExe
            // 
            this.buttonExe.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonExe.Location = new System.Drawing.Point(379, 500);
            this.buttonExe.Name = "buttonExe";
            this.buttonExe.Size = new System.Drawing.Size(60, 34);
            this.buttonExe.TabIndex = 47;
            this.buttonExe.Text = "RUN!";
            this.buttonExe.UseVisualStyleBackColor = true;
            this.buttonExe.Click += new System.EventHandler(this.buttonExe_Click);
            // 
            // labelDalsiRadka
            // 
            this.labelDalsiRadka.AutoSize = true;
            this.labelDalsiRadka.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDalsiRadka.Location = new System.Drawing.Point(753, 517);
            this.labelDalsiRadka.Name = "labelDalsiRadka";
            this.labelDalsiRadka.Size = new System.Drawing.Size(84, 12);
            this.labelDalsiRadka.TabIndex = 48;
            this.labelDalsiRadka.Text = "Příští řádek scriptu:";
            // 
            // textBoxNextLine
            // 
            this.textBoxNextLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(224)))), ((int)(((byte)(197)))));
            this.textBoxNextLine.Location = new System.Drawing.Point(790, 533);
            this.textBoxNextLine.Name = "textBoxNextLine";
            this.textBoxNextLine.ReadOnly = true;
            this.textBoxNextLine.Size = new System.Drawing.Size(196, 20);
            this.textBoxNextLine.TabIndex = 49;
            // 
            // labelNextLineNum
            // 
            this.labelNextLineNum.AutoSize = true;
            this.labelNextLineNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelNextLineNum.Location = new System.Drawing.Point(754, 535);
            this.labelNextLineNum.Name = "labelNextLineNum";
            this.labelNextLineNum.Size = new System.Drawing.Size(16, 16);
            this.labelNextLineNum.TabIndex = 50;
            this.labelNextLineNum.Text = "1";
            // 
            // textBoxTest
            // 
            this.textBoxTest.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxTest.Location = new System.Drawing.Point(528, 508);
            this.textBoxTest.Name = "textBoxTest";
            this.textBoxTest.Size = new System.Drawing.Size(103, 20);
            this.textBoxTest.TabIndex = 51;
            // 
            // checkBoxShowInfo
            // 
            this.checkBoxShowInfo.AutoSize = true;
            this.checkBoxShowInfo.Checked = true;
            this.checkBoxShowInfo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxShowInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkBoxShowInfo.Location = new System.Drawing.Point(878, 494);
            this.checkBoxShowInfo.Name = "checkBoxShowInfo";
            this.checkBoxShowInfo.Size = new System.Drawing.Size(100, 16);
            this.checkBoxShowInfo.TabIndex = 52;
            this.checkBoxShowInfo.Text = "show interpret info";
            this.checkBoxShowInfo.UseVisualStyleBackColor = true;
            this.checkBoxShowInfo.CheckedChanged += new System.EventHandler(this.checkBoxShowInfo_CheckedChanged);
            // 
            // labelKodRublan
            // 
            this.labelKodRublan.AutoSize = true;
            this.labelKodRublan.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelKodRublan.Location = new System.Drawing.Point(423, 22);
            this.labelKodRublan.Name = "labelKodRublan";
            this.labelKodRublan.Size = new System.Drawing.Size(102, 12);
            this.labelKodRublan.TabIndex = 53;
            this.labelKodRublan.Text = "(script v jazyce Rublan)";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkKhaki;
            this.ClientSize = new System.Drawing.Size(992, 566);
            this.Controls.Add(this.labelKodRublan);
            this.Controls.Add(this.checkBoxShowInfo);
            this.Controls.Add(this.textBoxTest);
            this.Controls.Add(this.labelNextLineNum);
            this.Controls.Add(this.textBoxNextLine);
            this.Controls.Add(this.labelDalsiRadka);
            this.Controls.Add(this.buttonExe);
            this.Controls.Add(this.buttonSaveInput);
            this.Controls.Add(this.buttonLoadScript);
            this.Controls.Add(this.button1Step);
            this.Controls.Add(this.textBoxShuffleSize);
            this.Controls.Add(this.buttonLoadAsScript);
            this.Controls.Add(this.buttonRCC);
            this.Controls.Add(this.buttonRCW);
            this.Controls.Add(this.buttonRLE);
            this.Controls.Add(this.buttonRBO);
            this.Controls.Add(this.buttonRTO);
            this.Controls.Add(this.buttonRRI);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonShuffle);
            this.Controls.Add(this.buttonRandTah);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.buttonFRcCW);
            this.Controls.Add(this.buttonFRcCC);
            this.Controls.Add(this.buttonFRbCW);
            this.Controls.Add(this.buttonFRbCC);
            this.Controls.Add(this.buttonFRaCW);
            this.Controls.Add(this.buttonFRaCC);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.buttonLEcCW);
            this.Controls.Add(this.buttonLEcCC);
            this.Controls.Add(this.buttonLEbCW);
            this.Controls.Add(this.buttonLEbCC);
            this.Controls.Add(this.buttonLEaCW);
            this.Controls.Add(this.buttonLEaCC);
            this.Controls.Add(this.buttonTOcCC);
            this.Controls.Add(this.buttonTOcCW);
            this.Controls.Add(this.buttonTObCC);
            this.Controls.Add(this.buttonTObCW);
            this.Controls.Add(this.buttonTOaCC);
            this.Controls.Add(this.buttonTOaCW);
            this.Controls.Add(this.buttonTest);
            this.Controls.Add(this.listBoxOutput);
            this.Controls.Add(this.labelInput);
            this.Controls.Add(this.textBoxInput);
            this.Controls.Add(this.labelOutput);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "Rubikarna";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label labelOutput;
        private System.Windows.Forms.TextBox textBoxInput;
        private System.Windows.Forms.Label labelInput;
        private System.Windows.Forms.ListBox listBoxOutput;
        private System.Windows.Forms.Button buttonTest;
        private System.Windows.Forms.Button buttonTOaCW;
        private System.Windows.Forms.Button buttonTOaCC;
        private System.Windows.Forms.Button buttonTObCC;
        private System.Windows.Forms.Button buttonTObCW;
        private System.Windows.Forms.Button buttonTOcCC;
        private System.Windows.Forms.Button buttonTOcCW;
        private System.Windows.Forms.Button buttonLEaCW;
        private System.Windows.Forms.Button buttonLEaCC;
        private System.Windows.Forms.Button buttonLEbCW;
        private System.Windows.Forms.Button buttonLEbCC;
        private System.Windows.Forms.Button buttonLEcCW;
        private System.Windows.Forms.Button buttonLEcCC;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button buttonFRaCW;
        private System.Windows.Forms.Button buttonFRaCC;
        private System.Windows.Forms.Button buttonFRbCW;
        private System.Windows.Forms.Button buttonFRbCC;
        private System.Windows.Forms.Button buttonFRcCW;
        private System.Windows.Forms.Button buttonFRcCC;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Button buttonRandTah;
        private System.Windows.Forms.Button buttonShuffle;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonRRI;
        private System.Windows.Forms.Button buttonRTO;
        private System.Windows.Forms.Button buttonRBO;
        private System.Windows.Forms.Button buttonRLE;
        private System.Windows.Forms.Button buttonRCC;
        private System.Windows.Forms.Button buttonRCW;
        private System.Windows.Forms.Button buttonLoadAsScript;
        private System.Windows.Forms.TextBox textBoxShuffleSize;
        private System.Windows.Forms.Button button1Step;
        private System.Windows.Forms.Button buttonLoadScript;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button buttonSaveInput;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.Button buttonExe;
        private System.Windows.Forms.Label labelDalsiRadka;
        private System.Windows.Forms.TextBox textBoxNextLine;
        private System.Windows.Forms.Label labelNextLineNum;
        private System.Windows.Forms.TextBox textBoxTest;
        private System.Windows.Forms.CheckBox checkBoxShowInfo;
        private System.Windows.Forms.Label labelKodRublan;


    }
}

