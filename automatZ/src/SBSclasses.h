#include "SBS.h"

class GLbod
{
    private:
    
    GLfloat m_x;
    GLfloat m_y;
    GLfloat m_z;
    
    public:
    
    GLbod  ( ){m_x=0.0;m_y=0.0;m_z=0.0;}
    GLbod  ( GLfloat x , GLfloat y , GLfloat z ) {m_x=x;m_y=y;m_z=z;}
    ~GLbod ( ){}
    
    void operator+=(const GLbod& b) {m_x += b.m_x ; m_y += b.m_y ; m_z += b.m_z ; }
    
    GLfloat GetX ( ) const { return m_x; };
    GLfloat GetY ( ) const { return m_y; };
    GLfloat GetZ ( ) const { return m_z; };
};

class int2D
{
    private:
    
    int m_x;
    int m_y;
     
    public:
    
    int2D  ( ){m_x=0;m_y=0;}
    int2D  ( int x , int y ) {m_x=x;m_y=y;}
    ~int2D ( ){}
    
    void operator+=(const int2D& b) {m_x += b.m_x ; m_y += b.m_y ; }
    int2D operator-(const int2D& b) {return int2D(m_x - b.m_x ,m_y - b.m_y ); }
    bool operator==(const int2D& b) {return ( (m_x == b.m_x) && (m_y == b.m_y) ); }
    
    
    int GetX ( ) const { return m_x; };
    int GetY ( ) const { return m_y; };
}; 
