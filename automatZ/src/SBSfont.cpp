#include "SBS.h"

/*


	tady jsou v�ci o fontech hodn� opsany z tutorialu, proto anglicky


*/

vector<GLuint>	g_fonty;

const int START_PISMENO = 0 ; //bylo 32				
const int NUM_PISMEN    = 256 ; //bylo 96

GLvoid BuildFont(HDC & hDC , int size , char* name )
{
	HFONT	font;										// Windows Font ID
	HFONT	oldfont;									// Used For Good House Keeping
   
    g_fonty.push_back(glGenLists(NUM_PISMEN));    
    int indexFontu = g_fonty.size() - 1;
    
	font = CreateFont(	-size,							// Height Of Font
						0,								// Width Of Font
						0,								// Angle Of Escapement
						0,								// Orientation Angle
						FALSE,    						// Font Weight
						FALSE,							// Italic
						FALSE,							// Underline
						FALSE,							// Strikeout
						EASTEUROPE_CHARSET,//ANSI_CHARSET,					// Character Set Identifier
						OUT_TT_PRECIS,					// Output Precision
						CLIP_DEFAULT_PRECIS,			// Clipping Precision
						ANTIALIASED_QUALITY,			// Output Quality
						FF_DONTCARE|DEFAULT_PITCH,		// Family And Pitch
						name);					// Font Name

	oldfont = (HFONT)SelectObject(hDC, font);           // Selects The Font We Want
	
    // Builds NUM_PISMEN Characters Starting At Character START_PISMENO
    wglUseFontBitmaps(hDC, START_PISMENO, NUM_PISMEN, g_fonty[indexFontu] );
	
    SelectObject(hDC, oldfont);							// Selects The Font We Want
	DeleteObject(font);									// Delete The Font
}

GLvoid KillAllFonts(GLvoid)									
{
    for (int i = 0 ; i < g_fonty.size() ; i++ )
    {
	   glDeleteLists(g_fonty[i], NUM_PISMEN);					
    }
}

GLvoid glPrint(int idFontu, const char *fmt, ...)					// Custom GL "Print" Routine
{      
    MyAssert( idFontu < g_fonty.size() , "fce glPrint : takov� idFontu neexistuje" );

    char		text[1024];								// Holds Our String
	va_list		ap;										// Pointer To List Of Arguments

	if (fmt == NULL)									// If There's No Text
		return;											// Do Nothing

	va_start(ap, fmt);									// Parses The String For Variables
	    vsprintf(text, fmt, ap);						// And Converts Symbols To Actual Numbers
	va_end(ap);											// Results Are Stored In Text
    
	glPushAttrib(GL_LIST_BIT);							// Pushes The Display List Bits
	glListBase(g_fonty[idFontu] - START_PISMENO);		// Sets The Base Character to 32
	glCallLists(strlen(text), GL_UNSIGNED_BYTE, text);	// Draws The Display List Text
	glPopAttrib();										// Pops The Display List Bits
}
