#include "automat.h"

//Vrac� Desku , kter� je zde ale ch�p�na jako �ada ��sel, kter� p�esn� reprezentuj�
//okol� ur�it� bu�ky na toroidu (Podpis Okol�). Toto okol� je unik�tn� pro cel�
//relativn� sousedstv� bu�ky.
//parametrem je index bunky ke kter� se bude relativn� sousedstv� vztahovat
//druh�m parametrem je zkouman� deska
Deska Automat::GetPodpisOkoliBunky( int indexBunky , const Deska & zkoumanaDeska )
{
	//algoritmus bude prohled�vat do ���ky zadanou desku (za�ne u zadan� bu�ky)
	//p�i prohled�v�n� si bude zaznamen�vat barvy pol��ek, kter� projde.
	//prohled�v�me tak aby z�le�elo pouze na relativn�m okol� dan� bu�ky,
	//tak jako by byla na toroidu. 

	bool puvodniToroidnost = m_isToroidni ;
	m_isToroidni = true;
		
	queue<int>   fronta;
	vector<bool> uzNavstivene( GetNumBunek() , false );
	Deska        ret( GetNumBunek() , 0 );
	int			 indexInRet = 0;
	
	//vlo��me prvn� polo�ku do fronty
	fronta.push( indexBunky );
	uzNavstivene[indexBunky] = true ; 
	
	while ( ! fronta.empty() )
	{
		//z fronty vynd�me prvn� index bunky, ten ted budeme zkoumat
		int tedProzkoumat = fronta.front();
		fronta.pop();
		
		//zaznamen�me si barvu pr�v� zkouman� bu�ky
		ret[indexInRet] = zkoumanaDeska[tedProzkoumat];
		indexInRet++ ;
		
		//do fronty zaradime jeste nenavstivene sousedy prave zkoumane bunky
		for (int s=0; s < GetNumSousedu() ; s++ )
		{
			int indexSouseda = GetSousedIndex(tedProzkoumat,s);
			
			if ( ! uzNavstivene[indexSouseda]  )
			{
				fronta.push( indexSouseda );
				uzNavstivene[indexSouseda] = true ; 
			}
		}
	}
	
	m_isToroidni = puvodniToroidnost;
	return ret;
}

//tato funkce navazuje na tu p�edchoz�, tentokr�t v�ak kontroluje zda
//zadan� bu�ka na zadan� desce m� stejn� podpis okol� jako je podpis zadan� jako t�et� parametr 
bool Automat::CheckPodpisyOkoliBunky( int indexBunky , const Deska & zkoumanaDeska , const Deska & zadanyPodpis )
{
	//algoritmus v t�to funkci je analogick� jako v t� p�edchoz�
	
	bool puvodniToroidnost = m_isToroidni ;
	m_isToroidni = true;
	
	queue<int>   fronta;
	vector<bool> uzNavstivene( GetNumBunek() , false );
	int			 indexInPodpis = 0;
	
	fronta.push( indexBunky );
	uzNavstivene[indexBunky] = true ; 
	
	while ( ! fronta.empty() )
	{
		//z fronty vynd�me prvn� index bunky, ten ted budeme zkoumat
		int tedProzkoumat = fronta.front();
		fronta.pop();
		
		//zaznamen�me si barvu pr�v� zkouman� bu�ky
		Barva barvaZkoumaneho = zkoumanaDeska[tedProzkoumat];
		//otestujeme jestli se shoduje s podpisem
		if ( barvaZkoumaneho != zadanyPodpis[indexInPodpis] )
		{
			//nyn� v�me �e podpisy se neshoduj�, ukon��me
			m_isToroidni = puvodniToroidnost;
			return false;
		}
		indexInPodpis++ ;
		
		//do fronty zaradime jeste nenavstivene sousedy prave zkoumane bunky
		for (int s=0; s < GetNumSousedu() ; s++ )
		{
			int indexSouseda = GetSousedIndex(tedProzkoumat,s);
			
			if ( ! uzNavstivene[indexSouseda]  )
			{
				fronta.push( indexSouseda );
				uzNavstivene[indexSouseda] = true ; 
			}
		}
	}
	
	m_isToroidni = puvodniToroidnost;
	return true;
	
}


//	Tato metoda odpov� na ot�zku, zda je obsah (tedy stav) dan�ho automatu raketa (spaceship) nebo oscil�tor. 
//	Vrac� periodu rakety respektive oscil�toru a do stringu p�ed�van�ho odkazem ulo�� podrobn�j�� popis: 
//	To zda se jedn� o raketu �i oscil�tor, jeho periodu a v p��pad�, �e jde o raketu, �ekne jak�m sm�rem 
//	se pohybuje a jakou rychlost�. Prvn� argument je hloubka (po�et generac�) do kter� se testuje, 
//	ne� se testov�n� vzd�. Druh� argumentu je u� zmi�ovan� string.

int Automat::CoJeTo( int kolikTestovatGeneraci , string & info )
{	
	//v algoritmu je nutn� toroidnost, proto ji n�siln� p�epneme, pak ji vr�t�me jak byla
	bool puvodniToroidnost = m_isToroidni ;
	m_isToroidni = true;
	
	//zaz�lohujeme aktualn� stav desky
	Deska aktualniStav = GetAktDeska();
		
	//najdeme prvn� nenulovou bu�ku na desce aktualniStav, zde se p�edpokl�d� �e je sou��st� potencialn� rakety.
	//Tzn. pokud raketa nen� jedin� v�c obsa�en� v aktu�ln�m stavu, nebude algoritmus fungovat.
	//ov�em to by (pokud bychom byli punti�k��i) ani nem�lo vadit, nebo� raketa plus n�jak� smet� okolo
	//u� (aspo� podle m�) de jure nen� raketa. 
	int indexPrvniNenulove = -1 ;
	for ( int i=0 ; i < GetNumBunek() ; i++ )
	{
		if( aktualniStav[i] != 0 )
		{
			indexPrvniNenulove = i ;
			break;
		}
	}
	//pokud je deska pr�zdn�, ur�it� to nen� raketa
	if (indexPrvniNenulove == -1 )
	{
		m_isToroidni = puvodniToroidnost;
		
		info = string("Vzdit je to prazdny.. ");
		return 0;
	}
	
	Deska podpis = GetPodpisOkoliBunky( indexPrvniNenulove , aktualniStav );
	
	//te� budeme postupn� testovat budouc� stavy hrac� plochy, zda nem�
	//n�jak� nenulov� bu�ka na tomto budouc�m stavu stejn� podpis jako podpis
	//aktualn� desky. Stejn� podpis by toti� znamenal, �e jsou tyto dva stavy
	//shodn� a� na posunut� na toroidn� hrac� plo�e.
	
	int indexPodobnehoStavu = -1;
	int indexPodobneBunky   = -1;
	
	for(int iTest = 0 ; iTest < kolikTestovatGeneraci ; iTest++ )
	{
		// neviditeln� (=tzn po zkon�en� algoritmu v�e vr�t�me nazp�tek) evolvujeme automat
		Next(false); //false je zde proto, aby se neukl�dalo do historie
		
		bool maStejnyPodpis = false;
		for ( int indexBunky=0 ; indexBunky < GetNumBunek() ; indexBunky++ )
		{
			if( CheckPodpisyOkoliBunky( indexBunky , GetAktDeska(), podpis ) )
			{
				maStejnyPodpis = true; 
				indexPodobneBunky = indexBunky ;
				break;
			}
		}
		
		if (maStejnyPodpis)
		{
			indexPodobnehoStavu = iTest ;
			break;	
		}
	}
	
	ostringstream oss;
	int perioda;
	
	//nejedn� se o raketu, alespo� ne do prohledavane hloubky
	if( indexPodobnehoStavu == -1 )
	{
		oss << "Toto neni raketa ani oscilator.. ";
		perioda = 0;
	}
	else
	{
		perioda = indexPodobnehoStavu + 1;
		
		//pokud jde o oscil�tor
		if ( indexPodobneBunky == indexPrvniNenulove )
		{
			if (perioda == 1)
			{
				oss << "To jsou jen �utry.. (Oscilator s periodou 1 takt.) ";
			}
			else
			{
				oss << "Je to oscilator! Ma periodu " << perioda << ". ";
			}
		}
		//nebo je to p��mo raketa (tedy oscilator je taky raketa, ale pohybuj�c� se rychlost� 0)
		else 
		{
			if( m_dimenze == 2 && m_tvarPolicka == CTVEREC )
			{
				//spo�teme rychlost
				int x2 = indexPodobneBunky  % m_velikost;
				int x1 = indexPrvniNenulove % m_velikost;
				int y2 = indexPodobneBunky  / m_velikost;
				int y1 = indexPrvniNenulove / m_velikost;
				
				double draha = sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) );
				double rychlost = draha / perioda;
				
				oss << "Je to raketa! Ma periodu " << perioda << ". \n"
			    << "Pohybuje se ve smeru [ "<<x2-x1<<", "<<y1-y2<<"]. \n"
				<< "Pohybuje se rychlosti " << rychlost << ". " ;
				
				if ( rychlost > 1 )
					oss << "\nHopl�! Rychlost je vetsi, nez rychlost svetla."
						<< "\nTo znamena, ze nastala nepresnost kvuli toroidnosti."
						<< "\nNa toroidu je vzd�lenost ob�as srandovn�."
						<< "\nVa�e raketka je pravd�podobn� zrovna na rozhran� desky.";
			}
			else
			{
				oss << "Je to raketa! Ma periodu " << perioda << ". " ;
			}
		}
	}
	
	info = oss.str();
	
	m_isToroidni = puvodniToroidnost;
	m_deska[m_indexAktDesky] = aktualniStav;
	return perioda ;
}

