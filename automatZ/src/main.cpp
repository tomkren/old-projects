/*
 *   SBS - main.cpp
 */

#include "SBS.h"





int WINAPI WinMain(	HINSTANCE	hInstance,			// Instance
					HINSTANCE	hPrevInstance,		// P�edchoz� Instance
					LPSTR		lpCmdLine,			// Parametry z p��kazov� ��dky
					int			nCmdShow)			// Stav zobrazen� okna
{
    HDC	        hDC = NULL;     // Priv�tn� GDI Device Context
    HGLRC		hRC = NULL;	    // Trval� Rendering Context
    HWND		hWnd= NULL;     // Obsahuje Handle na�eho okna
    
    MSG	 msg;	 	          // struktura Windows Message 
	
    SetFullscreen( false );  //nastav�me fullscreen na vypnut�
	
	// Vytvo��me na�e OpenGL Okno
	if (!CreateGLWindow(hDC,hRC,hWnd,hInstance,(char*)(TITULEK_OKNA),WIDTH_OKNA,HEIGHT_OKNA,COLOR_BITS_OKNA,IsFullscreen( ) ))
	{
		//ukon�it
        return 0;
	}
    
    MyAssert ( SBSInit() , "Inicializace SBS prob�hla �patn�! " );
    
    while( !IsProgramFinished( ) )
	{
        //�ek� n�jak� zpr�va?
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))	
		{
            //A je to zrovinka ukon�ovac� zpr�va?
			if (msg.message==WM_QUIT)
			{
				SetProgramFinished();
			}
			else									
			{
                //p�elo�it
				TranslateMessage(&msg);	
                //odeslat
				DispatchMessage(&msg);
			}
		}
		//Pokud ��dn� zpr�va ne�ek�, pak..
		else										
		{
			if ((IsActive( ) && !DrawGLScene()) )
			{
				SetProgramFinished();
			}
			//nen� �as kon�it, updatni screen
			else									
			{
				SwapBuffers(hDC);					// Prohodit Buffery (Double Buffering)
			}
            
            if ( !ReagujNaZmacknuteKlavesy( hDC,hRC,hWnd,hInstance ) )
            {
                return 0 ;
            }         
		}
	}

	// Shutdown
	KillGLWindow(hDC,hRC,hWnd,hInstance);									// Kill The Window
	return (msg.wParam);							// Exit The Program
}
