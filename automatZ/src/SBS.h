/*                      *
 *   SBS - SBS.h        *
*/

#ifndef _SBS_BYLO_JIZ_VLOZENO_
#define _SBS_BYLO_JIZ_VLOZENO_

#include <windows.h>		// Hlavi�ka pro Windows
#include <gl\gl.h>			// Hlavi�ka pro OpenGL32 Library
#include <gl\glu.h>			// Hlavi�ka pro GLu32 Library
//#include <gl\glaux.h>		// Hlavi�ka pro Glaux Library (zastaral�)

#include <cmath>
#include <stdio.h>
#include <stdarg.h>// Hlavi�kov� soubor pro funkce s prom�nn�m po�tem parametr�
#include <ctype.h>
#include <time.h>


#include <string>
using namespace std;

//struktury
struct StringAndFce
{
    string  s ;
    int (*f) ( const string & input ) ;
};


// t��dy
#include "SBSclasses.h"
#include "textConsole.h"
#include "fps.h"
//#include "drawAutomat.h"


// makra preprocesoru
#include "SBSmakra.h"

//automatZ:--------------------------------------
	#include "config.h"
	void InitVykreslovaniAutomatu();
	void StartInitAutomat();
	void ZkusEvolvovat();
	void SetNumEvolucnichTaktu( int numTaktu );
	void NastavNeomezenouEvoluci( bool isZapnuta );
	void   SetDobaJednohoTaktu( double t );
	double GetDobaJednohoTaktu();
	void XPrint( string text );
	
//-------------------------------------------------------
	

//prototypy z SBSfont.cpp
    GLvoid BuildFont(HDC & hDC , int size , char* name );
    GLvoid KillAllFonts(GLvoid); 
    GLvoid glPrint(int idFontu, const char *fmt, ...);

//prototypy z SBSprovozni.cpp
    void MyAssert (bool mustBe , string errorHlaska );
    void PrintBox (string hlaska , string nadpis = "SBS info.." );
    string IntToString ( int number );
    string OneCharToString ( char ch );
    int StringToInt ( const string & str );
    void Hanoi ( int N , int from , int pres , int to );
    string DoubleToString ( double num , int presnost = 4 );
    int GetWindowWidth ();
    int GetWindowHeight ();
    void RozbitStringPoMezerachNaInty ( string input , vector<int> & outputIntVector );

//prototypy z SBSconsole.cpp
    void Print (const string & text);
    void Print (const char* text);
    void Print (int cislo);
    void Print (double cislo);
    void Print (bool vyraz);
    bool GetRadekKonzoleAPosunPointer (string & out);
    bool SwitchConsoleInput();
    void UpdateConsoleInput (const string & updateString );
    void PushConsoleInput ();
    bool IsConsoleInputOn ();
    void ClearConsoleInput ();
    string GetConsoleInput ( );
    void BackSpaceConsoleInput ( );
    int GetPoziceKurzoruConsole ( );
    void MoveConsoleKurzor( int deltaPos );
    void LoadConsoleHistory (bool dopredu);
    int ExecuteString (const string & input );
    void ClearConsoleOutput ( );
    int GetConsoleOutputSize ( );
   
//prototypy z prikazy.cpp
    int  ProvedPrikaz ( const string & jmenoPrikazu , const string & argumenty );
    void RegistrovatPrikaz (const string & jmeno , bool(*pf_fce)(const string & input) );
    void RegistrovatPrikazy ( );
    string GetDalsiJmenoPrikazu ();

//prototypy z fce.cpp
    GLvoid ReSizeGLScene(GLsizei width, GLsizei height);
    GLvoid KillGLWindow(HDC & hDC ,HGLRC & hRC, HWND & hWnd , HINSTANCE & hInstance);
    BOOL CreateGLWindow(HDC & hDC ,HGLRC & hRC, HWND & hWnd , HINSTANCE & hInstance , 
                        char* title, int width, int height, int bits, bool fullscreenflag);

    
//prototypy z draw.cpp
    int SBSInit();
    int InitGL(HDC & hDC);
    bool DrawGLScene( );
    void PresunKameruNaAktualniPozici( );
    void SpocitejNovouDeltaPoziciKamery( GLfloat & zMainLevel );
    void VykresliTextConsole( );
    void PripravMistoNaKresleni( );
    void DrawConsole ( );
    void PrintXY(int x,int y,string str, GLbod col = GLbod(1,1,1) );
    void DrawTextsXY();

//z mainDraw
    void MainDraw( );

  
//prototypy z eventy.cpp
    LRESULT CALLBACK WndProc(HWND hWnd,	UINT uMsg, WPARAM wParam,	LPARAM	lParam);

//prototypy z klavesy.cpp
    bool ReagujNaZmacknuteKlavesy ( HDC & hDC ,HGLRC & hRC, HWND & hWnd , HINSTANCE & hInstance );
    void ReagujNaStistenouKlavesuVeVstupuKonzole ( int klavesa );
                
//prototypy z SBSglobals.cpp
    void    SetActive                   ( bool newVal );
    bool    IsActive                    ( );
    void    PressKey                    ( int index );
    void    UnpressKey                  ( int index );
    bool    IsKeyPressed                ( int index );
    void    SetFullscreen               ( bool newVal );
    bool    IsFullscreen                ( );
    void    SetZoomDelta                ( int newVal );
    int     GetZoomDelta                ( );
    void    SetKlavesaVyrizena          ( int index , bool newStatus );
    bool    IsKlavesaVyrizena           ( int index );
    bool    IsProgramFinished           ( );
    void    SetProgramFinished          ( );
    double  GetFps                      ( );
    void    SpoctiFps                   ( );
    void    SetDeltaPoziceKamery        ( GLbod newVal );
    GLbod   GetDeltaPoziceKamery        ( );
    bool    IsLBPressed                 ( );
    bool    IsRBPressed                 ( );
    void    SetLBPressed                ( bool newStatus );
    void    SetRBPressed                ( bool newStatus );
    void    SetPoziceLBKdeByloStisteno  ( int2D newVal );
    int2D   GetPoziceLBKdeByloStisteno  ( );
    void    SetPosunutiMysi             ( int2D newVal );
    int2D   GetPosunutiMysi             ( );
    


//CONSTanty
    const char  TITULEK_OKNA[]              = "automatZ - Tom� K�en" ;
    const int   START_X_POS                 = 190;
    const int   START_Y_POS                 = 70;
    const int   WIDTH_OKNA                  = 800 ;
    const int   HEIGHT_OKNA                 = 600 ;
    const int   COLOR_BITS_OKNA             = 16  ;
    const int   WIDTH_FULLSCREEN            = 1280 ;
    const int   HEIGHT_FULLSCREEN           = 800 ;
    const int   COLOR_BITS_FULLSCREEN       = 16  ;
    const bool  SHOW_CURSOR_IN_FULLSCREEN   = true ;
    const bool  RESIZABLE_OKNO              = false ;
    
    const int   KOD_KLAVESY_KONZOLE         = 192 ;
    
    const int   FONT_COURIERNEW_12          = 0 ;
    const int   FONT_COURIERNEW_10          = 1 ;
    

enum STS_SPECIAL_KEYS
{
SBS_BACKSP = 1001,SBS_TAB,SBS_SHIFT,SBS_CTRL,SBS_PAUSA,SBS_CPSLCK,
SBS_SPACE,SBS_PGUP,SBS_PGDN,SBS_END,SBS_HOME,SBS_LEFT,SBS_UP,SBS_RIGHT,
SBS_DOWN,SBS_INS,SBS_DEL,SBS_WIN,SBS_MENU,SBS_F1,SBS_F2,SBS_F3,SBS_F4,
SBS_F5,SBS_F6,SBS_F7,SBS_F8,SBS_F9,SBS_F10,SBS_F11,SBS_F12,SBS_SHIT
};

const int LAYOUT_KLAVESNICE [256] =
{
0,0,0,0,0,0,0,0,SBS_BACKSP,SBS_TAB,0,0,0,0,0,0,SBS_SHIFT,SBS_CTRL,0,
SBS_PAUSA,SBS_CPSLCK,0,0,0,0,0,0,0,0,0,0,0,SBS_SPACE,SBS_PGUP,SBS_PGDN,
SBS_END,SBS_HOME,SBS_LEFT,SBS_UP,SBS_RIGHT,SBS_DOWN,0,0,0,0,SBS_INS,
SBS_DEL,0,'�','+','�','�','�','�','�','�','�','�',0,0,0,0,0,0,0,'a',
'b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r',
's','t','u','v','w','x','y','z',SBS_WIN,0,SBS_MENU,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,SBS_F1,SBS_F2,SBS_F3,SBS_F4,SBS_F5,SBS_F6,SBS_F7,
SBS_F8,SBS_F9,SBS_F10,SBS_F11,SBS_F12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,'�','=',',','-','.','�',';',0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'�','�',')','�',0,0,0,'\\',
0,0,0,0,SBS_SHIT,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};

const int LAYOUT_KLAVESNICE_SHIFT [256] =
{
0,0,0,0,0,0,0,0,SBS_BACKSP,SBS_TAB,0,0,0,0,0,0,SBS_SHIFT,
SBS_CTRL,0,SBS_PAUSA,SBS_CPSLCK,0,0,0,0,0,0,0,0,0,0,0,SBS_SPACE,SBS_PGUP,
SBS_PGDN,SBS_END,SBS_HOME,SBS_LEFT,SBS_UP,SBS_RIGHT,SBS_DOWN,0,0,0,0,SBS_INS,
SBS_DEL,0,'0','1','2','3','4','5','6','7','8','9',0,0,0,0,0,0,0,'A','B','C','D',
'E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X',
'Y','Z',SBS_WIN,0,SBS_MENU,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,SBS_F1,SBS_F2,
SBS_F3,SBS_F4,SBS_F5,SBS_F6,SBS_F7,SBS_F8,SBS_F9,SBS_F10,SBS_F11,SBS_F12,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'\"','%','?','_',':','�','�',
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'/','\'','(','!',0,0,
0,'|',0,0,0,0,SBS_SHIT,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};      

#endif
