#ifndef _FPS_H_
#define _FPS_H_

#include "windows.h"


class FPS
{
private:

    unsigned int stary_cas;
    unsigned int aktualni_cas;
    double fps;// Po�et sn�mk� za sekundu
    
    unsigned int Dtime;
    unsigned int old_cas;
    unsigned int new_cas;
    int int_fps;
    int citac;
    
    int MODC;
    
public:
    
    FPS(int modcval, double fpsval);
    void Vypocet();// Volat p�ed ka�d�m p�ekreslen�m sc�ny
    double Get() {return fps;}
    unsigned int Dt() const {return Dtime ;}

};

#endif
