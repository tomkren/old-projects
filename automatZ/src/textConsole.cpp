#include "textConsole.h"


TextConsole::TextConsole  ( int (*pf_executeFce) ( const string & input )  ,
                            int      velikost    , 
                            string   prvniHlaska ,
                            bool     isInputOn
                          )
{ 
    if ( prvniHlaska != "" )
    {
        m_output.push_back(prvniHlaska);
    }
    
    m_printPointer = 0 ;
    m_velikostKonzole = velikost ;
    m_isInputOn =  isInputOn ; 
    m_poziceKurzoru = 0 ;
    m_poziceHistorie = -1 ;
    
    m_pf_executeInput = pf_executeFce ;
}

int TextConsole::ExecuteInput( )
{
    return m_pf_executeInput( m_input );
}

void TextConsole::LoadHistory (bool dopredu)
{
    if ( m_history.empty() )
    {
        return ;    
    }
    
    //spocti novou pozcici
    m_poziceHistorie += (dopredu ? 1 : -1) ;
    
    //octli sme se mimo historii? zobraz historii now
    //if (  )
    
    if ( m_poziceHistorie == m_history.size() || m_poziceHistorie == -1 )
    {
        m_poziceHistorie = -1 ;
        
        m_input = m_nowHistory ;
        m_poziceKurzoru = m_input.size();
        return ;
    }
    
    if ( m_poziceHistorie == -2 )
    {
        m_poziceHistorie = m_history.size() - 1 ;
    }
    
    m_input = m_history[ m_poziceHistorie ];
    m_poziceKurzoru = m_input.size();  
}




void TextConsole::UpdateHistory (const string & novyZaznam )
{
    m_history.insert( m_history.begin() , novyZaznam );
    m_poziceHistorie = -1 ;
}

string TextConsole::GetInput ( )
{
    return m_input ;
}

void TextConsole::ClearOutput ( )
{
    m_output.clear();
    m_printPointer = 0 ;
}

int TextConsole::GetOutputSize( )
{
    return m_output.size();
}
    

void TextConsole::BackSpaceInput( )
{
    if ( ! m_input.empty() && m_poziceKurzoru > 0 )
    {
        m_input.erase( m_poziceKurzoru - 1 , 1 );
        m_poziceKurzoru -- ;
    }
}

void TextConsole::MoveKurzor( int deltaPos )
{
    m_poziceKurzoru += deltaPos ;
    
    if ( m_poziceKurzoru < 0 )
    { 
        m_poziceKurzoru = 0 ;
    }
    if ( m_poziceKurzoru > m_input.size() ) 
    {
        m_poziceKurzoru = m_input.size() ;
    }
}

void TextConsole::UpdateInput (const string & updateString )    
{
    m_input.insert( m_poziceKurzoru , updateString ) ;
    
    m_poziceKurzoru += updateString.size() ;
    
    //zazalohujeme input do m_nowHistory
    m_nowHistory = m_input ;
    m_poziceHistorie = -1 ;
}

int TextConsole::GetPoziceKurzoru ( ) 
{
    return m_poziceKurzoru ;
}
    

void TextConsole::ClearInput  ( )
{
    m_input.clear();
    m_poziceKurzoru = 0 ;
}

bool TextConsole::SwitchInput ()
{
    return m_isInputOn = ! m_isInputOn ;
}

bool TextConsole::IsInputOn ( ) 
{
    return m_isInputOn ;
}



void TextConsole::PushInput () 
{   
    PushRadek (m_input);
    UpdateHistory (m_input);
    
    ExecuteInput();
    
    ClearInput();
}    

bool TextConsole::GetRadekAPosunPointer( string & outRadek ) 
// vrac� zda do�lo k oto�en�
{
    outRadek = m_output[m_printPointer] ;
    
    m_printPointer ++ ;
    
    //pokud printpoiner u� ukazuje vedle, tak nastavit na 0
    if (m_printPointer >= m_output.size() )
    {
        m_printPointer = 0 ;
        
        return true ;
    }
    
    return false ;
}

void TextConsole::PushRadek (const string & radek) 
{ 
    m_output.push_back( radek );
    
    //vyma�eme z vr�ku ��dku, pokud je u� konzole pln�
    if (  m_output.size() > m_velikostKonzole )
    {
        m_output.erase( m_output.begin() );
    }
}
    
