#ifndef __CONFIG_Z_AUTOMATZ__
#define __CONFIG_Z_AUTOMATZ__

	#include <string>
	#include <fstream>
	
	using namespace std;
	
	//nahraje konfig jmenem "name" a ulozi ho do stringu "ret", vraci zda se operace poda�ila
	bool LoadConfig (const char* name , string& ret );
	
	//ulozi do konfigu "name" hodnotu "value"
	void SaveConfig (const char* name , string value );


#endif

