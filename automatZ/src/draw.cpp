#include "SBS.h"

int SBSInit()
{
    RegistrovatPrikazy ( );
    Print("Pro napov�du zadejte \"help\"");
    
    srand(time(NULL));
    
    StartInitAutomat();
	
    return true ;
}

int InitGL(HDC & hDC)	// openglsetup
{
    BuildFont(hDC,12,"Courier New");//FONT_COURIERNEW_12
    BuildFont(hDC,10,"Courier New");//FONT_COURIERNEW_10
    
    
	glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glClearColor(ma_COL(0,0,0), 0.0f);					// Black Background
	glClearDepth(1.0f);									// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations

    //Print( "Prob�hlo InitGL.. " );
    
    return TRUE;										
}

//ve�ker� vykreslov�n� prob�h� zde
bool DrawGLScene( )
{ 

    SpoctiFps();
    PripravMistoNaKresleni();
	DrawConsole();
	PresunKameruNaAktualniPozici();
    MainDraw();
    
    return true;
}


void DrawTextsXY()
{    
    PrintXY(92,0,"fps : " + DoubleToString( GetFps() ) );
    PrintXY(92,1, IntToString(GetWindowWidth()) + " � " + IntToString(GetWindowHeight()) ) ;    
}

void DrawConsole ( )
{
    static bool consoleZapnuta = true ;
	if (consoleZapnuta)
    { 
        VykresliTextConsole( );
		DrawTextsXY();
    }
    
    ma_IfKlavesa_BEGIN (VK_F11);
        consoleZapnuta = !consoleZapnuta ;
	ma_IfKlavesa_END   (VK_F11); 
    
	ma_IfKlavesa_BEGIN (KOD_KLAVESY_KONZOLE);
        SwitchConsoleInput( ); 
        ClearConsoleInput ( );
	ma_IfKlavesa_END (KOD_KLAVESY_KONZOLE); 
	
	ma_IfKlavesa_BEGIN (VK_RETURN);
        if ( IsConsoleInputOn() ) 
        {
            PushConsoleInput();
        }
	ma_IfKlavesa_END (VK_RETURN); 
	
	//zamez� tomu aby se psalo do konzole a ona nebyla vyd�t
	if ( IsConsoleInputOn() )
	{
        consoleZapnuta = true ;
    }
}

void PripravMistoNaKresleni( )
{
    // Vyma�e obrazovku a hloubkov� buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	
    // Reset matice (Current Modelview Matrix)
	glLoadIdentity();
	
	//posuneme se kous��ek nahoru kvuli konzoli a tak
	glTranslatef( 0 , 0 , -0.1 );
}

void SpocitejNovouDeltaPoziciKamery( GLfloat & zMainLevel )
{
    GLbod newDeltaPoziceKamery = GLbod(0.,0.,0.);
    
    //reakce na posunut� my��
    int2D posun = GetPosunutiMysi ();
    if( !(posun == int2D(0,0)) )
    {
        GLfloat zValPosouvaneHladiny = zMainLevel ;
        
        GLfloat deltaX =  zValPosouvaneHladiny * posun.GetX() / 720.0 ;
        GLfloat deltaY = -zValPosouvaneHladiny * posun.GetY() / 720.0 ;
        
        newDeltaPoziceKamery += GLbod( deltaX , deltaY , 0.0 );
           
        SetPosunutiMysi( int2D(0,0) );
    }
    //reakce na ZOOM
    int zoom = GetZoomDelta( );
    if (zoom > 0)
    {
        newDeltaPoziceKamery += GLbod (0,0,0.1);
        zMainLevel += 0.1 ;
        SetZoomDelta( GetZoomDelta( ) - 1 );
    }
    if (zoom < 0)
    {
        newDeltaPoziceKamery += GLbod (0,0,-0.1);
        zMainLevel += -0.1 ;
        SetZoomDelta( GetZoomDelta( ) + 1 ); 
    }
    
    SetDeltaPoziceKamery( newDeltaPoziceKamery ) ;
}

void PresunKameruNaAktualniPozici( )
{ 
    static GLfloat zMainLevel = -6.0;
    
    static GLfloat xPos =  0.0 ;
    static GLfloat yPos =  0.0 ;
    static GLfloat zPos = zMainLevel ;
    
    
    SpocitejNovouDeltaPoziciKamery ( zMainLevel );
    
    GLbod deltaPoziceKamery = GetDeltaPoziceKamery() ;
    
    xPos += deltaPoziceKamery.GetX() ;
    yPos += deltaPoziceKamery.GetY() ;
    zPos += deltaPoziceKamery.GetZ() ;
    
    glTranslatef( xPos , yPos , zPos );
}


void PrintXY(int x,int y,string str, GLbod col )
{       
    float pxKonstanta           = 73.0 ;
    float fullscreenKorekceX    = IsFullscreen( ) ? 0.011 : 0.0 ;
    
    //GLfloat xPos0 = - 800 / 2.0 / pxKonstanta * 0.01 - fullscreenKorekceX ;
    //GLfloat yPos0 =   600 / 2.0 / pxKonstanta * 0.01 - 0.0015  ;
    
    GLfloat xPos0 = -0.0548 - fullscreenKorekceX ;
    GLfloat yPos0 =  0.03956 ;
    
    GLfloat mezirkaX = IsFullscreen( ) ? 0.00072 : 0.00100 ;
    GLfloat mezirkaY = IsFullscreen( ) ? 0.00130 : 0.00180 ;
    
    glColor3f(col.GetX(),col.GetY(),col.GetZ());
    glRasterPos2f( xPos0 + mezirkaX*x , yPos0 - mezirkaY*y  ); //0.01 by m�lo odpov�dat 59 px 
    glPrint(FONT_COURIERNEW_12, str.c_str()  );// V�pis textu         
}

void VykresliTextConsole( )
{
    int xPos = 0;
    int yPos = 0;
    
    bool otocilo = false ;
    
    if ( GetConsoleOutputSize() > 0 ) 
    {
        while ( !otocilo )
        {
            string out;
            otocilo = GetRadekKonzoleAPosunPointer(out);
            
            PrintXY( xPos,yPos, out );// V�pis textu 
            
            yPos ++ ;   
        }
    }
    else
    {
        // kdy� je output konzole nulov�, a� je input uplne naho�e .)
        yPos ++ ;
    }
    
    if (IsConsoleInputOn() )
    {
        yPos ++ ;
        
        static int blikatkoCitac ;
        
        const int frekvenceBlikatka = 7 ;
        blikatkoCitac += 10000 / int(GetFps()) ;
        bool jeVidet = ((frekvenceBlikatka*blikatkoCitac/10000) % 2 ) ;
        
        string blikatko = jeVidet ? "_" : "" ;
        
        blikatko = string( GetPoziceKurzoruConsole() + 2 , ' ' ) + blikatko;
        
        string consoleInput = "> " + GetConsoleInput() ; // + blikatko ;
        
        PrintXY(xPos,yPos,consoleInput,GLbod(0,1,0) );
        PrintXY(xPos,yPos,blikatko    ,GLbod(0,1,0) ); 
  
    }
}

