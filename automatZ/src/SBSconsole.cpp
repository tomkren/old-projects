#include "SBS.h"

TextConsole g_console( ExecuteString , 43 , "Program automatZ V�s v�t� !" , true );

//+++ zabudovat si pak do "SBS �ablony" tudle funkci
//vylep�enej print kterej ch�pe i konce ��dek
void XPrint( string text )
{
	if( text.size() <= 1 ) return;
	int pos = text.find( "\n" , 0 );
	
	if(pos == string::npos)
	{
		Print(text); return;
	}
	
	string kTisku = text.substr(0,pos);
	string zbytek = text.substr(pos+1);
	
	Print (kTisku);
	XPrint(zbytek);
}

int ExecuteString (const string & input )
{
    
    //zjistit p��kaz = text stringu po prvn� mezeru
    int poziceMezery = input.find( " " , 0 );
    string jmenoPrikazu = input.substr(0,poziceMezery);
    
    string argumenty;
    if ( poziceMezery < input.size() )
    argumenty = input.substr( poziceMezery + 1 ,input.size() );
    
    return ProvedPrikaz ( jmenoPrikazu , argumenty ) ;
}

void MoveConsoleKurzor( int deltaPos )
{
    g_console.MoveKurzor( deltaPos );
}

void LoadConsoleHistory (bool dopredu)
{
    g_console.LoadHistory(dopredu);
}

void ClearConsoleOutput ( )
{
    g_console.ClearOutput();
}

int GetConsoleOutputSize ( )
{
    return g_console.GetOutputSize();
}
    
int GetPoziceKurzoruConsole ( )
{
    return g_console.GetPoziceKurzoru( );
}

void Print (const string & text)
{
    g_console.PushRadek( text );
}

void Print (const char* text)
{
    g_console.PushRadek( string(text) );
}

void Print (int cislo)
{
    g_console.PushRadek( IntToString(cislo) );
}

void Print (double cislo)
{
    g_console.PushRadek( DoubleToString(cislo) );
}

void Print (bool vyraz)
{
    g_console.PushRadek( vyraz?"true":"false" );
}

bool SwitchConsoleInput()
{
    return g_console.SwitchInput();
}

void UpdateConsoleInput (const string & updateString )
{
    g_console.UpdateInput( updateString ) ;
}

void PushConsoleInput ()
{
    g_console.PushInput () ;
}

void ClearConsoleInput ( )
{
    g_console.ClearInput( );
}

string GetConsoleInput ( )
{
    return g_console.GetInput( );
}

void BackSpaceConsoleInput ( )
{
    g_console.BackSpaceInput( ) ;   
}   

bool IsConsoleInputOn ( )
{
    g_console.IsInputOn( );
}

bool GetRadekKonzoleAPosunPointer (string & out)
{
    return g_console.GetRadekAPosunPointer( out );
}
