#include "SBS.h"

void ReagujNaStistenouKlavesuVeVstupuKonzole ( int klavesa ) 
{
    //zjist�me keyKod
    int keyKod = LAYOUT_KLAVESNICE[ klavesa ];
    if ( IsKeyPressed (VK_SHIFT) ) 
    {
        keyKod = LAYOUT_KLAVESNICE_SHIFT[ klavesa ];
    }
    
    //reagujem na keyKod
    if ( keyKod == 0 )
    {
        //nedefinovan� kl�vesa -> ��dn� odpov��
    }
    //kl�vesy s keyKode < 1000 se vypisuj�
    else if ( keyKod < 1000 )
    {
        string inputUpdate = OneCharToString( keyKod );
        
        UpdateConsoleInput ( inputUpdate );
    }
    else
    {
        switch ( keyKod )
        {
            case SBS_SPACE :
                UpdateConsoleInput ( " " );
                break;
            case SBS_BACKSP :
                BackSpaceConsoleInput( );
                break;
            case SBS_DEL :
                MoveConsoleKurzor(  1 );
                BackSpaceConsoleInput( );
                break;
            case SBS_HOME :
                MoveConsoleKurzor(  -1024 );
                break;                        
            case SBS_END :
                MoveConsoleKurzor(  1024 );
                break;
            case SBS_LEFT :
                MoveConsoleKurzor( -1 );
                break;
            case SBS_RIGHT :
                MoveConsoleKurzor(  1 );
                break;
            case SBS_UP :
                LoadConsoleHistory(true);
                break;
            case SBS_DOWN :
                LoadConsoleHistory(false);
                break;
            case SBS_TAB :
                ClearConsoleInput();
                UpdateConsoleInput (GetDalsiJmenoPrikazu());
                break;
            case SBS_WIN :
                Print("Windows je �m��ko, vyskou�ej Linux ;)");
                break;
            case SBS_CPSLCK :
                Print("cAPS lOCK tady nefunguje, stejn� jsou s nim jen probl�my .)");
                break;          
        }
    }
}

bool ReagujNaZmacknuteKlavesy ( HDC & hDC ,HGLRC & hRC, HWND & hWnd , HINSTANCE & hInstance )
//vrac� zda se povedlo reagovat, �i nikoliv
{
	if ( IsKeyPressed(VK_F12) )
	{
		UnpressKey(VK_F12);
		
		KillGLWindow(hDC,hRC,hWnd,hInstance);
		SetFullscreen(!IsFullscreen( ));	
		
		int newWidth, newHeight, newColorBits;
		
		if ( IsFullscreen( ) )
		{
            newWidth     = WIDTH_FULLSCREEN ; 
            newHeight    = HEIGHT_FULLSCREEN ;  
            newColorBits = COLOR_BITS_FULLSCREEN ; 
        }
        else
        {
            newWidth     = WIDTH_OKNA ; 
            newHeight    = HEIGHT_OKNA ;  
            newColorBits = COLOR_BITS_OKNA ;
        }
		
		if (!CreateGLWindow(hDC,hRC,hWnd,hInstance,(char*)(TITULEK_OKNA),newWidth,newHeight,newColorBits,IsFullscreen( ) ))
		{
			return false;
		}
	}
	
	return true ;
}
