#include "SBS.h"
#include "drawAutomat.h"
#include "automat.h"

Automat g_automat;

void MainDraw()
{ 
	if ( g_automat.IsOk() )
	{
		//3D
		if ( g_automat.GetDimenze() == 3 )
		{	
			int indexBunky = 0;
			for (int z=0 ; z<GetVelikostDesky()  ;z++)
			for (int y=0 ; y<GetVelikostDesky()  ;y++)   
			for (int x=0 ; x<GetVelikostDesky()  ;x++) 
			{  
		    	DrawPolicko3D( x,y,z, g_automat.GetBarvaBunky_akt(indexBunky) );
		    	indexBunky ++ ;
			}
		}
		
		//2D
		else if ( g_automat.GetDimenze() == 2 )
		{
			int indexBunky = 0;
			for (int y=0 ; y<GetVelikostDesky()  ;y++)   
			for (int x=0 ; x<GetVelikostDesky()  ;x++) 
			{  
		    	DrawPolicko2D( x,y, g_automat.GetBarvaBunky_akt(indexBunky) );
		    	indexBunky ++ ;
			}
		}
		//1D
		else
		{
			for (int x=0 ; x<GetVelikostDesky()  ;x++) 
			{  
		    	DrawPolicko2D( x,0, g_automat.GetBarvaBunky_akt( x ) );
			}
		}
		
		ZkusEvolvovat();
	}
}


//udr�uje dobu jednoho taktu v sekundach
double  g_dobaJednohoTaktu = 0.1 ;
double GetDobaJednohoTaktu()           { return g_dobaJednohoTaktu ; }
void   SetDobaJednohoTaktu( double t ) { g_dobaJednohoTaktu = t; }

//ur�uje zda je evoluce neomezene zapnuta 
//(tzn pokud je true, je evoluce zapnuta dokud se explicitn� nevypne)
bool g_isEvoluceZapnuta = false ;

void NastavNeomezenouEvoluci( bool isZapnuta )
{
	g_isEvoluceZapnuta = isZapnuta;
}

//dr�� po�et evolu�n�ch takt� k vykon�n�
int g_evolucnichTaktu = 0;

void SetNumEvolucnichTaktu( int numTaktu )
{
	g_evolucnichTaktu = numTaktu ;
}

void ZkusEvolvovat()
{
	if ( !g_isEvoluceZapnuta && g_evolucnichTaktu <= 0 ) return;
	
	//d�lka jednoho taktu v sekund�ch
	
	static int cisloTaktu = 0;
	double hranice = GetFps() * GetDobaJednohoTaktu() ;
	
	cisloTaktu ++ ;
	
	if( cisloTaktu > hranice  )
	{
		cisloTaktu = 0;
		g_evolucnichTaktu -- ;
		
		g_automat.Next();
	}
}

void StartInitAutomat()
{
	
	//nejd�ive nahraje implicitn� pravidla
	string souborPravidel;
	LoadConfig( "implicitniPravidla" , souborPravidel );
	
	g_automat.LoadPravidla( souborPravidel.c_str() );
	
	//pokud se to povedlo, nahrajeme i stav
	if( g_automat.IsOk() )
	{
		string souborStavu;
		LoadConfig( "implicitniStav" , souborStavu );
	
		g_automat.LoadStav( souborStavu.c_str() );
		InitVykreslovaniAutomatu();
	}
	else
	{
		MyAssert(false,"Config \"impicitniPravidla\" neobsahuje korektn� pravidla.");	
	}
	
	//nahrajeme dobu taktu
	string dobaTaktu;
	LoadConfig( "dobaTaktu" , dobaTaktu );
	SetDobaJednohoTaktu( StringToInt(dobaTaktu)/1000.0 );
	
}
	

void InitVykreslovaniAutomatu()
{
	SpoctiGlobalsProDrawPolicko( );
}
