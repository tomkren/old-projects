#include "SBS.h"

/* DeltaPoziceKamery */
GLbod g_deltaPoziceKamery = GLbod (0,0,0) ;

void SetDeltaPoziceKamery ( GLbod newVal )
{
        g_deltaPoziceKamery = newVal; 
}

GLbod GetDeltaPoziceKamery ( )
{
        return g_deltaPoziceKamery ; 
}

/* programFinished  */
    bool g_programFinished = false;  
	
	bool IsProgramFinished ( )
	{
        return g_programFinished ;
    }
    
    void SetProgramFinished ( )
    {
        g_programFinished = true ;
    }

/* active - Ponese informaci o tom, zda je okno aktivn� */
 
    bool g_active = true ;
    
    void SetActive ( bool newVal )
    {
        g_active = newVal ;
    }
    
    bool IsActive ( )
    {
        return g_active ;
    }

/* mousePressed */
    bool   g_mousePressed[2]              = { false , false } ;
    int2D  g_poziceMysiKdeBylaStistena[2] = { int2D(0,0) , int2D(0,0) };
    
    void SetPoziceLBKdeByloStisteno ( int2D newVal )
    {
        g_poziceMysiKdeBylaStistena[0] = newVal ;
    }
    
    int2D GetPoziceLBKdeByloStisteno ( )
    {
       return g_poziceMysiKdeBylaStistena[0] ;
    }
    
    bool IsLBPressed ()
    {
        return g_mousePressed[0];
    }
    
    bool IsRBPressed ()
    {
        return g_mousePressed[1];
    }
    
    void SetLBPressed ( bool newStatus )
    {
        g_mousePressed[0] = newStatus;
    }
    
    void SetRBPressed ( bool newStatus )
    {
        g_mousePressed[1] = newStatus;
    }
    
/* keys - Ukl�d�n� vstupu z kl�vesnice */

    bool g_keys[256] = {}  ;
    
    void PressKey ( int index )
    {
        g_keys[index] = true ;
    }
    
    void UnpressKey ( int index )
    {
        g_keys[index] = false ;
    }
       
    bool IsKeyPressed ( int index)
    {
        return g_keys[index] ;
    }

/* klavesaVyrizena - pole pro z�znam toho zda jsme u� reagovali na klavesu */
    
    bool g_klavesaVyrizena[256] = {}  ;
       
    void SetKlavesaVyrizena ( int index , bool newStatus )
    {
        g_klavesaVyrizena[index] = newStatus ;
    }
       
    bool IsKlavesaVyrizena ( int index)
    {
        return  g_klavesaVyrizena[index] ;
    }
    
/* fullscreen - Je zapnut fullscreen �i ne? */

    bool g_fullscreen = false ;
    
    void SetFullscreen ( bool newVal )
    {
        g_fullscreen = newVal ;
    }
    
    bool IsFullscreen ( )
    {
        return g_fullscreen ;
    }

/* zoomDelta - Je zapnut fullscreen �i ne? */

    int g_zoomDelta = 0 ;
    
    void SetZoomDelta ( int newVal )
    {
        g_zoomDelta = newVal; 
    }
    
    int GetZoomDelta ( )
    {
        return g_zoomDelta ;
    }

/* posunutiMysi  */

    int2D g_posunutiMysi = int2D(0,0);
    
    void SetPosunutiMysi( int2D newVal )
    {
        g_posunutiMysi = newVal ;
    }
    
    int2D GetPosunutiMysi ()
    {
        return g_posunutiMysi ;
    }
    
/* fps */

    FPS g_fps ( 50 , 1000.0 ) ;
    
    double GetFps()
    {
        return g_fps.Get();
    }
    
    void SpoctiFps()
    {
        g_fps.Vypocet() ;
    }
