#include "SBS.h"

/*tady to je chvilema anglicky, proto�e to obsahuje i winapi zalezitosti opsany z tutorialu*/

LRESULT CALLBACK WndProc(	HWND	hWnd,			// Handle For This Window
							UINT	uMsg,			// Message For This Window
							WPARAM	wParam,			// Additional Message Information
							LPARAM	lParam)			// Additional Message Information
{   
	switch (uMsg)									// Check For Windows Messages
	{
		case WM_ACTIVATE:							// Watch For Window Activate Message
		{
			if (!HIWORD(wParam))					// Check Minimization State
			{
				SetActive (true) ;						// Program Is Active
			}
			else
			{
				SetActive (false) ;						// Program Is No Longer Active
			}

			return 0;								// Return To The Message Loop
		}

		case WM_SYSCOMMAND:
		{
			switch (wParam)
			{
				case SC_SCREENSAVE:
				case SC_MONITORPOWER:
					return 0;
			}
			break;
		}

		case WM_CLOSE:								// Did We Receive A Close Message?
		{
			PostQuitMessage(0);						// Send A Quit Message
			return 0;								// Jump Back
		}

		case WM_KEYDOWN:							// Is A Key Being Held Down?
		{
			PressKey( wParam );  					// If So, Mark It As TRUE
			
            if ( IsConsoleInputOn() )
            {
                ReagujNaStistenouKlavesuVeVstupuKonzole ( wParam );
            }
			return 0;
		}
        
		case WM_KEYUP:								// Has A Key Been Released?
		{
			UnpressKey( wParam );
			return 0;								// Jump Back
		}
        
        // Zm�nit velikost OpenGL okna
		case WM_SIZE:								
		{
			ReSizeGLScene(LOWORD(lParam),HIWORD(lParam));  // LoWord=Width, HiWord=Height
			return 0;								// Jump Back
		}
		
		case WM_LBUTTONDOWN:
		{
            SetLBPressed ( true );
            SetPoziceLBKdeByloStisteno ( int2D( LOWORD(lParam) , HIWORD(lParam) ) );
            
            return 0 ;
        }
		
		case WM_LBUTTONUP:
		{
            SetLBPressed ( false );
    
            return 0 ;
        }
        
        case WM_MOUSEMOVE:
		{
            if ( IsLBPressed() )
            {
                int2D poziceTed = int2D( LOWORD(lParam) , HIWORD(lParam) ) ;
                int2D posun = GetPoziceLBKdeByloStisteno() - poziceTed ;
                
                //Print ( IntToString(posun.GetX()) + " " + IntToString(posun.GetY()) ) ;
                SetPosunutiMysi( posun );
                
                SetPoziceLBKdeByloStisteno ( poziceTed );
            }
            return 0;
        }
        
		case WM_MOUSEWHEEL:
        {
            // /120 aby obsahovalo +1 nebo -1
            SetZoomDelta( GET_WHEEL_DELTA_WPARAM(wParam)/120 );
            return 0;
        }
	}

	// Pass All Unhandled Messages To DefWindowProc
	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}
