#include "SBS.h"
#include "automat.h"



vector<StringAndFce> g_seznamPrikazu ;

//prikazy pro automatZ:

extern Automat g_automat;

int PrikazCojeto ( const string & args )
{
	if ( args == "-?" )
    {
        XPrint ( string(" cojeto [hlubka] : program se pokus� bl�e identifikovat zda \n")+
		                "                   nen� utvar v automatu n�co specialniho. \n"+
	                    "                   hloubka (bez ud�n� je 100) ud�va po�et generac� dop�edu, \n"
						"                   kter� ma program p�i anal�ze prozkoumat ne� se vzd�." );
        
        return true ;
    }
    
	vector<int> argsIntVector;
    RozbitStringPoMezerachNaInty ( args , argsIntVector );
    
    int hloubka;
    if (argsIntVector.size() == 0)
    {
		hloubka = 100; 
    }
    else 
    {
		hloubka = argsIntVector[0];
    }
    
    string info;
    g_automat.CoJeTo( hloubka , info );
    XPrint(info);
    
    return true;
}

int PrikazCojeto_old ( const string & args )
{
	if ( args == "-?" )
    {
        XPrint ( string(" cojeto_old [hlubka] : (STARA IMPLEMETACE cojeto zanech�no  z d�jepisn�ch d�vod�)\n")+
						"                   program se pokus� bl�e identifikovat zda \n"+
		                "                   nen� utvar v automatu n�co specialniho. \n"+
	                    "                   hloubka (bez ud�n� je 250) ud�va po�et generac� dop�edu, \n"
						"                   kter� ma program p�i anal�ze prozkoumat ne� se vzd�." );
        
        return true ;
    }
    
	vector<int> argsIntVector;
    RozbitStringPoMezerachNaInty ( args , argsIntVector );
    
    int hloubka;
    if (argsIntVector.size() == 0)
    {
		hloubka = 250; 
    }
    else 
    {
		hloubka = argsIntVector[0];
    }
    
    string info;
    g_automat.CoJeTo_old( hloubka , info );
    XPrint(info);
    
    return true;
}

int PrikazPravidla ( const string & args )
{
	if ( args == "-?" )
    {
        Print ( "pravidla : vyp�e pravidla automatu." );
        
        return true ;
    }
    
    XPrint( g_automat.GetPravidla() );
    
    return true;
}

int PrikazTakt ( const string & args )
{
	if ( args == "-?" )
    {
        Print ( "takt [t] : nastav� dobu jednoho taktu na t milisekund.");
		Print ("            Pokud nen� t zad�no, vyp�e aktu�ln� nastaven� d�lky taktu." );
        
        return true ;
    }
    
	vector<int> argsIntVector;
    RozbitStringPoMezerachNaInty ( args , argsIntVector );
    
    if (argsIntVector.size() == 0)
    {
		Print( string("Jeden takt trv� ") + IntToString (int(GetDobaJednohoTaktu()*1000)) + " milisekund." );
        return true;
    }
    if (argsIntVector.size() >= 1)
    {
		SetDobaJednohoTaktu( argsIntVector[0] / 1000.0 );
		SaveConfig( "dobaTaktu" , IntToString(argsIntVector[0]) );
        return true;
    } 
    
	SetDobaJednohoTaktu( StringToInt(args) / 1000.0 );
	return true;
}

int PrikazRand ( const string & args )
{
    if ( args == "-?" )
    {
        Print ( "rand : stav automatu se nastav� nahodn�" );
        
        return true ;
    }
    
    g_automat.Rand( );
    
    Print ( "Zam�ch�no!" );
    return true;
}

int PrikazStop ( const string & args )
{
    if ( args == "-?" )
    {
        Print ( "stop : zastav� evoluci" );
        
        return true ;
    }
    
    SetNumEvolucnichTaktu( 0 );
    NastavNeomezenouEvoluci( false );
    
    Print ( "Stopnuto." );
    return true;
}

int PrikazPlay ( const string & args )
{
    if ( args == "-?" )
    {
        Print ( "play : evoluce bude prob�hat dokud se nezastavi stopem" );
        
        return true ;
    }
    
    NastavNeomezenouEvoluci( true );	
}

int PrikazNext ( const string & args )
{
    if ( args == "-?" )
    {
        Print ( "next [n] : posune automat o n takt� dop�edu" );
        
        return true ;
    }
    
    vector<int> argsIntVector;
    RozbitStringPoMezerachNaInty ( args , argsIntVector );
    
    if (argsIntVector.size() == 0)
    {
		g_automat.Next();
        return true;
    }
    if (argsIntVector.size() >= 1)
    {
		SetNumEvolucnichTaktu( argsIntVector[0] );
        return true;
    }     

    
    g_automat.Next( );
    
    Print ( "HOP!" );
    return true;
}

int PrikazLp ( const string & args )
{
    if ( args == "-?" )
    {
        Print ( "lp [jmeno] : na�te pravidla Automatu ze slo�ky pravidla, nepi�te koncovku .txt ." );
        Print ( "             D�le se pokus� nahr�t implicitn� stav ze souboru");
		Print ( "             ./stavy/_jmeno.txt kde jmeno je zadan� jm�no." );
		
        return true ;
    }
    
    bool povedloSe = g_automat.LoadPravidla( ( "./pravidla/" + args + ".txt" ).c_str() );
    
    XPrint ( povedloSe?"Povedlo se nahr�t pravidla.":"Nepovedlo se nahr�t pravidla.\n" + g_automat.GetErrMsg() );
    
    if(povedloSe)
    {
		InitVykreslovaniAutomatu();
		int povedlImplicitniStav = g_automat.LoadStav( ( "./stavy/_" + args + ".txt" ).c_str() );
		Print ( povedlImplicitniStav==0?"Povedlo se nahr�t i implicitn� stav.":"Nepovedlo se nahr�t implicitn� stav." );
	}
    
    return true;
}

int PrikazLs ( const string & args )
{
    if ( args == "-?" )
    {
        Print ( "ls [jm�no] : na�te stav Automatu ze slo�ky stavy, nepi�te koncovku .txt ." );
        
        return true ;
    }
    
    int povedloSe = g_automat.LoadStav( ( "./stavy/" + args + ".txt" ).c_str() );
    
    Print ( povedloSe==0?"Povedlo se nahr�t.":"Nepovedlo se nahr�t." );
    return true;
}

//---konec automatZ


int PrikazHelo ( const string & args )
{
    if ( args == "-?" )
    {
        Print ( "helo [tvoje_jm�no] : Slu�n� pozdrav� program :)" );
        
        return true ;
    }
    
    Print ("helo Sekolovo Bezva SBS");
    return true;
}

int PrikazMove ( const string & args )
{
    if ( args == "-?" )
    {
        Print ( "move [x] [y] : Posune kameru o [x] pixel� doprava a [y] pixel� nahoru." );
        
        return true ;
    }
    
    vector<int> argsIntVector;
    RozbitStringPoMezerachNaInty ( args , argsIntVector );
    
    if (argsIntVector.size()==0)
    {
        return true;
    }
    if (argsIntVector.size()==1)
    {
        SetPosunutiMysi( int2D(argsIntVector[0],0) );
        return true;
    }     
    if (argsIntVector.size() > 1)
    {
        SetPosunutiMysi( int2D(argsIntVector[0],-argsIntVector[1]) );
        return true;   
    }
    
    return false;
}

int PrikazFps ( const string & args )
{
    if ( args == "-?" )
    {
        Print ( "fps : vyp�e FPS" );
        
        return true ;
    }
    
    Print ( GetFps() );
    return true;
}

int PrikazFulscr ( const string & args )
{
    if ( args == "-?" )
    {
        Print ( "fulscr : ON/OFF fullscreen." );
        
        return true ;
    }
    
    PressKey( VK_F12 );
    return true ;
}

int PrikazClrscr ( const string & args )
{   
    if ( args == "-?" )
    {
        Print ( "clrscr : Sma�e v�stup konzole." );
        
        return true ;
    }
    
    ClearConsoleOutput(); 
    return true ;
}

int PrikazBox ( const string & args )
{
    if ( args == "-?" )
    {
        Print ( "box [text] : Ud�l� MessageBox s [textem] :)" );
        
        return true ;
    }
    
    PrintBox(args , "BOX" );
}

int PrikazZoom ( const string & args )
{   
    if ( args == "-?" )
    {
        Print ( "zoom [hodnota] : [hodnota] ud�v� jak moc se p�ibl�� kamera." );
        Print ( "                 Pro kladnou [hodnotu] p�ibli�uje, pro z�pornou vzdaluje." );
        
        return true ;
    }
    
    SetZoomDelta ( StringToInt(args) );
     
    return true ;
}

int PrikazHelp ( const string & args )
{
    if ( args == "-?" )
    {
        Print ( "help : Vyp�e help, ale to asi v�, kdy� um� pou��t -? .)" );
        return true ;
    }
    
    Print ( " " );
    Print ( " -------------------------------------------------------------- " );
    Print ( "automatZ HELP" );
    Print ( " Kamerou lze h�bat pomoc� my�i (lev� tla��tko a kole�ko)." );
    Print ( " Pomoc� �ipek nahoru a dol� m��e� listovat v historii zadan�ch vstup� do konzole. " );
    Print ( " Pomoc� TABul�toru m��e� postupn� projet v�echny funk�n� p��kazy." );
    Print ( "!! PODROBNY HELP PRO JEDOTLIVE P��KAZY:" );
    Print ( "  Za p��kaz napi�te argument -?" );
    Print ( "  To znamen� nap��klad : exit -?" );
    Print ( " " );
    Print ( " OVLADANI AUTOMATU :" );
    Print ( " " );
	Print ( "lp : nahraje pravidla"  ); 
	Print ( "ls : nahraje stav"  ); 
	Print ( "next : p�ikaz pro posun o ur�it� po�et takt�" ); 
	Print ( "rand : nahodn� vygeneruje stav" ); 
	Print ( "stop : zastav� prov�d�n� automatu" ); 
	Print ( "play : zapne prov�d�n� automatu dokud nen� zad�no stop" ); 
	Print ( "takt : vyp�e/nastav� d�lku taktu" ); 
	Print ( "pravidla : vyp�e pravidla" ); 
	Print ( "cojeto : program se pokus� bl�e identifikovat zda nen� utvar v automatu n�co specialniho. " );     
    Print ( " " );
	Print ( " DAL�� P��KAZY :" );
    Print ( "  help   : Vyp�e help." );
    Print ( "  exit   : Vypne program." );
    Print ( "  clrscr : Sma�e v�stup konzole." );
    Print ( "  fulscr : ON/OFF fullscreen." );
    Print ( "  fps    : Vyp�e FPS." );
    Print ( "  zoom   : P�ibl�� kameru." );
    Print ( "  Plus n�kolik dal��ch sp� �ertovn�ch/pokusn�ch p��kaz� (lze je naj�t tabul�torem).");
    Print ( " -------------------------------------------------------------- " );
    
    /*
    Print ( "  move   : Posune kameru." );
	Print ( "  cmd    : Spust� text jako p��kaz v cmd." );
    Print ( "  echo   : Vyp�e text." );
    Print ( "  box    : Ud�l� MessageBox textem." );
    Print ( "  hanoi  : Vy�e�� probl�m Hanojsk� v�e." );
    Print ( "  helo   : T�m lze pozdravit program." );
      */        
    return true ;
}

int PrikazOmg ( const string & args )
{
    Print ("OMG je ve v�stavb�..");    
}

int PrikazHanoi ( const string & args )
{
    if ( args == "-?" )
    {
        Print ( "hanoi [N] : Vy�e�� probl�m Hanojsk� v�e pro zvolen� [N]." );
        return true ;
    }
    
    Hanoi( StringToInt(args) , 1 , 3 , 2 ) ;
    
    return true ;
}

int PrikazCmd ( const string & args )
{
    if ( args == "-?" )
    {
        Print ( "cmd [p��kaz] : Spust� [p��kaz] jako p��kaz p��kazov� ��dky." );
        return true ;
    }
    
    system( args.c_str() );
    return true ;
}

int PrikazExit ( const string & args )
{
    if ( args == "-?" )
    {
        Print ( "exit : Vypne program." );
        return true ;
    }
    
    SetProgramFinished();
    return true ;
}

int PrikazEcho ( const string & args )
{
    if ( args == "-?" )
    {
        Print ( "echo : Vyp�e text." );
        return true ;
    }
    
    Print ( args );
    return true;
}

int ProvedPrikaz ( const string & jmenoPrikazu , const string & argumenty )
{
    int (*pf_fce) ( const string & input ) ;    
    
    for ( int i = 0 ; i < g_seznamPrikazu.size() ; i++ )
    {
        if ( g_seznamPrikazu[i].s == jmenoPrikazu )
        {
            pf_fce = g_seznamPrikazu[i].f ;
            return   pf_fce( argumenty ) ;
        }
    }

    return true ;
}

void RegistrovatPrikaz (const string & jmeno , int(*pf_fce)(const string & input)  )
{
    StringAndFce _ = {jmeno , pf_fce };
    g_seznamPrikazu.push_back(_);
}

string GetDalsiJmenoPrikazu ()
{
    static int indexPrikazu = 0 ;
    string jmenoPrikazu = g_seznamPrikazu[indexPrikazu].s ;
    
    indexPrikazu ++ ;
    
    if (indexPrikazu >= g_seznamPrikazu.size() )
    {
        indexPrikazu = 0 ;
    }
    
    return jmenoPrikazu ;
}

void RegistrovatPrikazy ( )
{
    RegistrovatPrikaz ( "exit"   , PrikazExit );
    RegistrovatPrikaz ( "echo"   , PrikazEcho );
    RegistrovatPrikaz ( "cmd"    , PrikazCmd );
    RegistrovatPrikaz ( "help"   , PrikazHelp );
    RegistrovatPrikaz ( "zoom"   , PrikazZoom );
    RegistrovatPrikaz ( "clrscr" , PrikazClrscr );
    RegistrovatPrikaz ( "box"    , PrikazBox );  
    RegistrovatPrikaz ( "fulscr" , PrikazFulscr );  
    RegistrovatPrikaz ( "omg"    , PrikazOmg );  
    RegistrovatPrikaz ( "hanoi"  , PrikazHanoi );
    RegistrovatPrikaz ( "fps"    , PrikazFps );      
    RegistrovatPrikaz ( "helo"    , PrikazHelo );          
    RegistrovatPrikaz ( "move"    , PrikazMove ); 
	
	//automatZ:
	RegistrovatPrikaz ( "lp"      , PrikazLp ); 
	RegistrovatPrikaz ( "ls"      , PrikazLs ); 
	RegistrovatPrikaz ( "next"    , PrikazNext ); 
	RegistrovatPrikaz ( "rand"    , PrikazRand ); 
	RegistrovatPrikaz ( "stop"    , PrikazStop ); 
	RegistrovatPrikaz ( "play"    , PrikazPlay ); 
	RegistrovatPrikaz ( "takt"    , PrikazTakt ); 
	RegistrovatPrikaz ( "pravidla", PrikazPravidla ); 
	RegistrovatPrikaz ( "cojeto_old"  , PrikazCojeto_old ); 
	RegistrovatPrikaz ( "cojeto"  , PrikazCojeto ); 
	
	
}

