#include "config.h"


//nahraje konfig jmenem "name" a ulozi ho do stringu "ret", vraci zda se operace poda�ila
bool LoadConfig (const char* name , string& ret )
{
	ifstream fin( (string("./config/") + name + string(".txt")).c_str() );
	
	if ( ! fin.good() ) return false;
	
	fin >> ret;
	
	return true;
}
	
//ulozi do konfigu "name" hodnotu "value"
void SaveConfig (const char* name , string value )
{
	string jmenoSouboru = string("./config/") + name + string(".txt") ;
	ofstream fout(jmenoSouboru.c_str() );
	
	fout << value;
}
