#include "SBS.h"
#include "drawAutomat.h"
#include "automat.h"

//+++ todo: vypnout perspektivu
//          zefektivnit vykreslovan� tim �e se udelaj globalni promeny s konstantama co sou pot�eba
//				spocitat

extern Automat g_automat;

const GLfloat V_ROVNO   = 0.866025403 ;
const GLfloat ROH_DESKY	= 1.8 ;
const GLfloat ZMENSENI_KVULI_RAMECKU = 0.8 ;

const GLfloat KOREKCE_VELIKOSTI[3] = {1.0,1.3,.75};	

int GetVelikostDesky()
{
	return g_automat.GetVelikost();
}

int GetTypPolicka()
{
	return (int)g_automat.GetTvarPolicka();
}

int GetNumBarev()
{
	return g_automat.GetNumBarev();
}


//glob�ln� prom�nn� souvysej�c� s vykreslov�n�m pol��ek
GLfloat g_a , g_v , g_stranaPolicka ;

void SpoctiGlobalsProDrawPolicko( )
{
	g_stranaPolicka = ( 2*ROH_DESKY *KOREKCE_VELIKOSTI[GetTypPolicka()] / GetVelikostDesky() ) ; 
    
	g_a = g_stranaPolicka * ZMENSENI_KVULI_RAMECKU ;
	g_v = V_ROVNO * g_a ;
}

void DrawPolicko2D( int xpos , int ypos , int barva )
{
	
	float color = (barva+1)/( (GetNumBarev()+1)+1.0 ) ;
	glColor3f( color, color , color ); //b�l�

	
	SpecialTranslace(xpos,ypos,1);

    if      (GetTypPolicka() == UH_4) DrawCtverec();
	else if	(GetTypPolicka() == UH_6) DrawHexagon();
	else                         DrawTrojuh ( (xpos%2 + ypos%2)%2 == 0 );

	SpecialTranslace(xpos,ypos,-1);	

}

void DrawPolicko3D( int xpos , int ypos , int zpos , int barva )
{
	
	float color = (barva+1)/( (GetNumBarev()+1)+1.0 ) ;
	glColor3f( color, color , color ); //b�l�

	
	SpecialTranslace3D(xpos,ypos,zpos,1);

    DrawCube();

	SpecialTranslace3D(xpos,ypos,zpos,-1);	

}


void Translace( GLfloat x, GLfloat y, GLfloat z , int smysl )
{
	glTranslatef( smysl*x , smysl*y , smysl*z );	
}

void SpecialTranslace3D(int xpos,int ypos, int zpos , int smysl)
{
		Translace( -ROH_DESKY + xpos*g_stranaPolicka , 
		            ROH_DESKY - ypos*g_stranaPolicka ,
					ROH_DESKY - zpos*g_stranaPolicka, 
					smysl );
}

void SpecialTranslace(int xpos,int ypos, int smysl)
{
	if(GetTypPolicka() == UH_6)
		Translace( -ROH_DESKY + (ypos%2)*V_ROVNO*g_stranaPolicka + xpos*2*V_ROVNO*g_stranaPolicka , 
				   ROH_DESKY - 1.5*ypos*g_stranaPolicka ,0.0, smysl );
	
	else if(GetTypPolicka() == UH_4)
		Translace( -ROH_DESKY + xpos*g_stranaPolicka , ROH_DESKY - ypos*g_stranaPolicka ,0.0, smysl );
	
	else
		Translace( -ROH_DESKY + xpos*g_stranaPolicka/2. , ROH_DESKY - V_ROVNO*ypos*g_stranaPolicka*0.9 ,0.0, smysl );
	
}

void DrawCube()
{
	glBegin(GL_LINES);
            glVertex3f( g_a/2 , g_a/2 , 0 );
            glVertex3f(-g_a/2 , g_a/2 , 0 );
            
			glVertex3f(-g_a/2 , g_a/2 , 0 );
            glVertex3f(-g_a/2 ,-g_a/2 , 0 );
            
            glVertex3f(-g_a/2 ,-g_a/2 , 0 );
            glVertex3f( g_a/2 ,-g_a/2 , 0 );
            
			glVertex3f( g_a/2 ,-g_a/2 , 0 );
            glVertex3f( g_a/2 , g_a/2 , 0 );
            
    glEnd();
}


void DrawTrojuh ( bool nahoru )
{
	if (nahoru)
	{
		glBegin(GL_TRIANGLES);
	    	glVertex2f( g_a/2,  -1./3.*V_ROVNO*g_a ); 
			glVertex2f(-g_a/2,  -1./3.*V_ROVNO*g_a ); 
			glVertex2f( 0    ,   2./3.*V_ROVNO*g_a );					
		glEnd();
	}
	else
	{
		glBegin(GL_TRIANGLES);
	    	glVertex2f( g_a/2,   2./3.*V_ROVNO*g_a ); 
			glVertex2f(-g_a/2,   2./3.*V_ROVNO*g_a ); 
			glVertex2f( 0    ,  -1./3.*V_ROVNO*g_a );					
		glEnd();
	}
	
}

void DrawCtverec( )
{
	// +++ p�ed�lat na efektivn�j�� vykreslov�n�
    glBegin(GL_QUADS);	
    	 glVertex2f(-0.5*g_a, 0.5*g_a);			
		 glVertex2f( 0.5*g_a, 0.5*g_a);	
         glVertex2f( 0.5*g_a,-0.5*g_a);	
         glVertex2f(-0.5*g_a,-0.5*g_a);			
	glEnd();	
}



void DrawHexagon( )
{
	
#define B_0   0 , 0
#define B_1   0 , g_a
#define B_2  g_v, g_a/2
#define B_3  g_v,-g_a/2
#define B_4   0 ,-g_a
#define B_5 -g_v,-g_a/2
#define B_6 -g_v, g_a/2

	
	
	glBegin(GL_TRIANGLES);
    	glVertex2f( B_0 ); glVertex2f( B_1 ); glVertex2f( B_2 );
		glVertex2f( B_0 ); glVertex2f( B_2 ); glVertex2f( B_3 );
		glVertex2f( B_0 ); glVertex2f( B_3 ); glVertex2f( B_4 );
		glVertex2f( B_0 ); glVertex2f( B_4 ); glVertex2f( B_5 );
		glVertex2f( B_0 ); glVertex2f( B_5 ); glVertex2f( B_6 );
		glVertex2f( B_0 ); glVertex2f( B_6 ); glVertex2f( B_1 );					
	glEnd();
}


