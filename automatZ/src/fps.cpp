#include "fps.h"

FPS::FPS(int modcval, double fpsval)
{
    fps     = fpsval;
    MODC = modcval; 
    citac = 1;
    old_cas = 0;
}
    

void FPS::Vypocet()
{
    //const int MODC = 100;
    citac++;
    
    if (!(citac%MODC)) //p�i ka�dym MODC-tym pruchodu se spo�te fps
    {
        if ( old_cas == 0 )
        {
            old_cas = GetTickCount();
            
            return;
        }
        
        new_cas = GetTickCount();
        Dtime = new_cas - old_cas;
        
        fps = MODC * 1000.0 / Dtime;
        
        old_cas = new_cas;
        citac = 0;
    }
}
