#ifndef __CLASS_AUTOMAT_Z_AUTOMATZ__
#define __CLASS_AUTOMAT_Z_AUTOMATZ__

#include <cstdlib>
#include <cmath>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <queue>
using namespace std;

typedef unsigned char Barva ;
typedef vector<Barva> Deska ;

inline void AutomatAssert(bool mustBe, const char* hlaska ) {if(!mustBe)cout<<"!CHYBA! : "<<hlaska<<"\n";}
inline int  MyPow(int x , int y) {int r=1; for(int i=0;i<y;i++)r*=x; return r; }

namespace AutomatConst
{
	const int MAX_NUM_BAREV 		= 255;
	const int MAX_VELIKOST  		= INT_MAX; //neomezeno
	const int NUM_TYPU_EVOL_FCI 	= 2;
	const int KONEC_ZADANI     		= -2;
	const int KONEC_SUBZADANI  		= -1;
	const int VYPADNUTI				= -1;
	
	const int NUM_SOUSEDU_1D    = 2;
	const int NUM_SOUSEDU_2D[3] = { 8 , 3 , 6  };
	const int NUM_SOUSEDU_3D    = 26 ;
};

enum Tvar 				{ CTVEREC     = 0 , 
						  TROJUHELNIK = 1 , 
						  HEXAGON     = 2
						};
enum TypEvolucniFunkce 	{ 	PODLE_BARVY_SOUSEDU_A_ME = 0 ,  //pr: (rule 30) 0 0 0 1 1 1 1 0 -1 
							PREZIJE_VS_NARODI        = 1    //pr: (life) 2 3 -1 3 , pro vic barev hodnot� podle moji barvy
						} ;

class Automat
{
	//�LENSK� PROM�NN�
	private:
		
		//hrac� deska:
		Deska 				m_deska[2] ;    	//jsou pou�ity 2 hrac� desky, aktualn� a neaktualn�
											  	//p�i v�po�tu dal��ho stavu se z aktualn� desky spo��t� nov�
											  	//a p�enastav� se jim aktualnost
		int					m_indexAktDesky; 	 //v�, kter� deska je aktualni
		int					m_indexNeaktDesky;
		
		//historie:								<- je imlicitn� nulov�, mus� se nastavit explicitn�
		vector<Deska>		m_historie;			//slouzi k ukladani minulych stavu desky
		int					m_1stFreeHist;		//pamatuje si prvn� volnou pozici
				
		//pravidla:
		
		bool 				m_pravidlaOK ;  //signalizuje zda byla pravidla zad�na korektn�
		string				m_errMsg ;      //popis chyby
		
		int					m_dimenze;      // 1-,2- nebo 3-D	
		Tvar				m_tvarPolicka ; // ma smysl pouze pro 2D, jindy je v�dy �tverec
		bool				m_isToroidni ;  // zda je plocha toroidni ci nikoliv
		int					m_numBarev ;    // pocet moznych obarveni bunek, mrtva se nepocita jako barva
		int                 m_velikost ;    // 1D: ���ka , 2D: d�lka hrany �tverce , 3D: delka hrany krychle
		TypEvolucniFunkce   m_typEF ;
		vector<int>			m_dataEF ;
		
		string 				m_txtEF ; 		// pravidla EF zapsana v klasick�m k�du
		
		
	
	//VE�EJN� METODY
	public:
		
			
		//CoJeTo() - "v�zkumn�" funkce
		int CoJeTo( int kolikTestovatGeneraci , string & info );
		
		//implicitn� konstruktor vytvo�� nekorektn� pravidla
		Automat( );
		
		//na�te pravidla ze souboru, vrac� zda se povedlo korektn� na��st
		bool LoadPravidla(const char* souborPravidel ) ;
		//nahraje stav automatu, vrac� cislo podle uspechu 
		int  LoadStav    (const char* souborStavu ) ;
		


	   //metody pro historii:
		//sma�e historii a nastav� ji novou velikost
		void ResetHistorie( int newSize );
		
		//vypo��t� dal�� stav automatu a zaktualizuje ho
		void Next( bool ulozitDoHistorie = true ); //je voliteln�, zda se bude ukl�dat do historie

		//vr�t� zda jsou pravidla automatu v po��dku
		bool IsOk() {return m_pravidlaOK; }
		
		//vr�t� velikost automatu
		int  GetVelikost() {return m_velikost;}
		
		//vr�t� tvar pol��ek
		Tvar GetTvarPolicka() {return m_tvarPolicka;}
		
		//vr�t� po�et mo�n�ch barev
		int  GetNumBarev() {return m_numBarev;}

		//nastav� stav automatu na n�hodn�
		void Rand();
		
		// vr�t� dimenzi automatu
		int  GetDimenze() {return m_dimenze; }
		
		//vr�t� barvu po�adovane bunky na aktualn� desce
		Barva  GetBarvaBunky_akt( int index ); 					 

		//vr�t� string s popisem pravidel
		string GetPravidla( );

		//vrac� po�et soused� pro jedno pol��ko
		int  GetNumSousedu();
				
		//vrac� po�et v�ech bun�k
		int  GetNumBunek() { return MyPow( m_velikost , m_dimenze ); }		


		//vr�t� string obsahuj�c� aktualn� situaci na desce (p�edpokl�d� se 2D deska)
		string GetDeskaInString2D();
		
		//vr�t� errMsg (vzniklou p�i nahr�v�n�)
		string GetErrMsg() { return m_errMsg; }

		//star� implementace "v�zkumn� funkce":
		int CoJeTo_old( int kolikTestovatGeneraci , string & info );

					
	//SOUKROM� METODY	
	private:
		
		//pomocn� funkce pro CoJeTo() (podrobn� popis u definice)
		Deska GetPodpisOkoliBunky( int indexBunky , const Deska & zkoumanaDeska );
		bool CheckPodpisyOkoliBunky( int indexBunky , const Deska & zkoumanaDeska , const Deska & zadanyPodpis );
		
				
		//vrac� odkaz na aktualn� desku
		Deska& GetAktDeska() {return m_deska[m_indexAktDesky] ; }
				
		//nastav� bunku aktualn� desky na po�adovanou hodnotu
		void SetBarvaBunky_akt( int index , Barva newBarva ) {m_deska[m_indexAktDesky][index] = newBarva;}

		//nastav� barvu bunky na neaktivn� desce
		void   SetBarvaBunky_neakt( int index , Barva newBarva ) {m_deska[m_indexNeaktDesky][index] = newBarva;}		
		//prohod� desky, aktalni a neaktualn�
		void   SwapDesky();		
		//vr�t� novou barvu bunky v dalsim taktu
		Barva GetNextBarva( int indexBunky );
		//vr�t� novou barvu bunky v dalsim taktu, pro EF PREZIJE_VS_NARODI
		Barva PrezijeVsNarodi( int indexBunky );
		//vrac� index s-t�ho souseda bunky na indexu index  
		int GetSousedIndex( int index , int s );
		
		bool ZpracujDataEF();
};


#endif
