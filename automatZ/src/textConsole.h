#ifndef _SBS_CONSOLE_CLASS_
#define _SBS_CONSOLE_CLASS_

#include <string>
#include <vector>
using namespace std ;


class TextConsole
{    
    private:
    
    enum {DEFAULT_VELIKOST = 15 }; 
    
    vector<string>  m_output ;
    int             m_printPointer ;
    int             m_velikostKonzole ;
    
    string          m_input  ;
    bool            m_isInputOn ;
    int             m_poziceKurzoru ;
    
    vector<string>  m_history ;
    string          m_nowHistory ;
    int             m_poziceHistorie ;
    
    int (*m_pf_executeInput) ( const string & input ) ;
    
    private:
    void UpdateHistory (const string & novyZaznam );
    int ExecuteInput () ;
    
    public:
    TextConsole  ( int (*pf_executeFce) ( const string & input ) = NULL ,
                   int      velikost    = DEFAULT_VELIKOST , 
                   string   prvniHlaska = string(""),
                   bool     isInputOn   = true
                 );
    
    ~TextConsole ( ){}
    
    void PushRadek (const string & radek) ;
    
    bool    IsInputOn       ( ) ;
    bool    SwitchInput     ( ) ;
    void    UpdateInput     ( const string & updateString ) ;   
    void    PushInput       ( ) ;
    string  GetInput        ( ) ;
    void    ClearOutput     ( ) ;
    void    ClearInput      ( ) ;
    void    BackSpaceInput  ( ) ;
    int     GetOutputSize   ( ) ;
    
    int GetPoziceKurzoru ( );
    void MoveKurzor( int deltaPos );
    
    void LoadHistory (bool dopredu);

    bool GetRadekAPosunPointer( string & outRadek );

};



#endif
