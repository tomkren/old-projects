#ifndef __DRAW_AUTOMAT_Z_AUTOMATZ__
#define __DRAW_AUTOMAT_Z_AUTOMATZ__

#include "SBS.h"

enum tvary {UH_4=0,UH_3=1,UH_6=2};

void SpoctiGlobalsProDrawPolicko( );

void DrawPolicko2D( int xpos , int ypos , int barva );
int  GetVelikostDesky(  );
int GetTypPolicka();
void DrawCtverec( );
void DrawHexagon( );
void DrawTrojuh ( bool nahoru );
void Translace( GLfloat x, GLfloat y,GLfloat z, int smysl );
void SpecialTranslace( int xpos,int ypos, int smysl);
void DrawCube();
void DrawPolicko3D( int xpos , int ypos , int zpos , int barva );
void SpecialTranslace3D(int xpos,int ypos, int zpos , int smysl);

#endif

