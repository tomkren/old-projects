#include "automat.h"

void Automat::ResetHistorie( int newSize )
{
	Deska prazdnaDeska;
	prazdnaDeska.assign( GetNumBunek() , 0 );
	
	m_historie.assign( newSize , prazdnaDeska );
	m_1stFreeHist = 0 ;
}

Barva Automat::GetBarvaBunky_akt( int index ) 					 
{
	if (index != AutomatConst::VYPADNUTI )
		return m_deska[m_indexAktDesky][index];
	
	//pokud prob�hlo k vypadnut� (nast�v� pokud se hled� soused na kraji desky
	// pokud nen� zapnut� toroidnost) pova�uje se bu�ka za mrtvou.
	return 0;
}

void Automat::Rand()
{
	// projdeme v�echny bunky a nastav�me jim n�hodnou barvu
	for(int indexBunky = 0 ; indexBunky < GetNumBunek() ; indexBunky++ )
	{
		Barva newBarva = rand()%(m_numBarev+1);
		SetBarvaBunky_akt( indexBunky , newBarva );
	}
}





//!!!!!!!!!!!!!!! star� implemetace, zachov�no z d�jepisn�ch d�vod� !!!!!!!
// nov� implementace je v souboru automat_cojeto.cpp
int Automat::CoJeTo_old( int kolikTestovatGeneraci , string & info )
{
	//zaz�lohujeme aktualn� stav desky
	Deska zaloha = m_deska[m_indexAktDesky];
	
	//vytvo��me pat�i�n� hlubokou historii ale nenapln�me ji
	int puvodniVelikostHistorie = m_historie.size();
	ResetHistorie( kolikTestovatGeneraci );
	
	//t�mto vlo��me prvn� z�znam do historie
	Next();


	
	//pujdeme od "nejstar�� k nejmlad��" desce a budeme porovn�vat postup�
	// v�echny desky z historie s tou "nejstar�i". 
	// budeme postupovat n�sledovn�: 
	//  nejd��v najdeme prvn� nenulovou bu�ku
	//  (p�edpokladem je, �e raketa je periodick� k relativn� pozici a �e hrac� deska "nem� zuby")
	//  potom existuj� dva stavy historie, kde od tohoto nenulov�ho stavu a� nakonec
	//  jsou v�echny barvy bun�k stejn�
	Deska nejstarsiDeska = m_historie[0];
	
		
	//najdeme prvn� nenulovou bunku v nejstarsiDesce a spocteme kolik je nenulovych
	int prvniNenulovaNejs = INT_MAX ;
	int numNenulNejs = 0;
	for(int i=0; i<nejstarsiDeska.size() ; i++ )
	{
		if( nejstarsiDeska[i] != 0)
		{	
			numNenulNejs ++;
			//pokud je poprv� nenulov�, zapamatujem si ji
			if( prvniNenulovaNejs == INT_MAX ) prvniNenulovaNejs = i ;
		}
	}
	
	if( prvniNenulovaNejs == INT_MAX ) return 0; //pr�zdn� deska nen� raketa
	
	
	//sem ulo��me, pokud bude n�jaka deska vyhovovat
	int vyhovujiciDeska = 0;
	
	//pozice prvni nenulove bunky v testovane desce
	int prvniNenulovaTest;
	
	for(int iTest = 1 ; iTest < kolikTestovatGeneraci ; iTest++ )
	{
		//na za�atku je pot�eba vlo�it dal�� z�znam do historie
		Next();
		
		//najdeme prvn� nenulovou v r�mci testovan� desky
		prvniNenulovaTest = INT_MAX ;
		for(int i=0; i<nejstarsiDeska.size() ; i++ )
		{
			if(m_historie[iTest][i] != 0)
			{
				prvniNenulovaTest = i ;
				break;
			}
		}
		
		int aktTest = prvniNenulovaTest;
		int aktNejs = prvniNenulovaNejs;
		int zbyvaNajitNe0 = numNenulNejs;
		
		bool jeAsiRaketa = true;
		while (aktTest < nejstarsiDeska.size() && 
			   aktNejs < nejstarsiDeska.size() )
		{
			//pokud si neodpov�daj�, potom..
			if ( m_historie[iTest][aktTest] != nejstarsiDeska[aktNejs] )
			{
				jeAsiRaketa = false;
				break;
			}
			//pokud ov�em zat�m vyhovuj�..
			else
			{
				//pokud je to nenulov� pol��ko, �krtneme si jednu ze zbyvaNajitNe0.
				if( nejstarsiDeska[aktNejs] != 0)
				{
					zbyvaNajitNe0 -- ;
				}
			}
			aktTest ++;
			aktNejs ++;
		}
		
		if ( zbyvaNajitNe0 == 0 && jeAsiRaketa ) //potom je opravdu raketa!
		{
			vyhovujiciDeska = iTest ;
			break;
		}
	}
	
	//obnov�me p�vodn� velikost historie
	ResetHistorie( puvodniVelikostHistorie );
	//a taky obnov�me p�vodn� stav automatu
	m_deska[m_indexAktDesky] = zaloha;
	
	ostringstream oss;
	
	int perioda = vyhovujiciDeska;
	
	if (perioda > 0 )
	{
		//pokud jde o oscilator
		if ( prvniNenulovaTest == prvniNenulovaNejs )
		{
			oss << "Je to oscilator! Ma periodu " << perioda << " takty. ";
		}
		//nebo je to raketa
		else
		{
			//+++ zat�m p�edpokl�d�me 2D-�tvercovou s�, pro ostatn� nen� v�sledn� rychlost korektn� 
			//spo�teme rozd�l polohy, abychom zjistili rychlost
			
			int x2 = prvniNenulovaTest % m_velikost;
			int x1 = prvniNenulovaNejs % m_velikost;
			int y2 = prvniNenulovaTest / m_velikost;
			int y1 = prvniNenulovaNejs / m_velikost;
	
			double draha = sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) );
			
			
			oss << "Je to raketa! Ma periodu " << perioda << " takty. \n"
			    << "Pohybuje se ve smeru [ "<<x2-x1<<", "<<y1-y2<<"]. \n"
				<< "Pohybuje se rychlosti " << draha / perioda << ". \n" ;
		}
	}
	//neni to ani jedno
	else
	{
		oss << "Toto neni raketa ani oscilator..";
	}
	
	info = oss.str();
	
	return vyhovujiciDeska; 
}



//vypo��t� dal�� stav automatu a zaktualizuje ho
void Automat::Next( bool ulozitDoHistorie )
{
	//nejd��ve ulo��me desku do historie, poku ji ov�em vedeme
	if ( m_historie.size() > 0 && ulozitDoHistorie )
	{
		m_historie[ m_1stFreeHist ] = m_deska[m_indexAktDesky] ;
		m_1stFreeHist++;
		
		//kontrola p�ete�en� po��tadla prvn� voln� pozice
		if( m_1stFreeHist == m_historie.size() ) m_1stFreeHist = 0;
	}
	
	// projdeme v�echny bunky a zjist�me jejich p��t� barvu
	int numBunek = GetNumBunek();
	for(int indexBunky = 0 ; indexBunky < numBunek ; indexBunky++ )
	{
		Barva newBarva = GetNextBarva( indexBunky );
		SetBarvaBunky_neakt( indexBunky , newBarva );
	}
		
	//prohod�me desky (tzn nastav�me aktualni na neaktualni a neaktualni na aktualni)
	SwapDesky();
}

//vr�t� novou barvu bunky v dalsim taktu
Barva Automat::GetNextBarva( int indexBunky )
{
	if( m_typEF == PREZIJE_VS_NARODI ) return PrezijeVsNarodi( indexBunky );
	
	AutomatAssert(false, "Mela by byt volana zatim neinplementovana EF." );
}

//vr�t� novou barvu bunky v dalsim taktu, pro EF PREZIJE_VS_NARODI
Barva Automat::PrezijeVsNarodi( int indexBunky )
{
	Barva barvaBunky = GetBarvaBunky_akt( indexBunky );
	
	//pokud je ziva, skontrolujeme zda p�e�ije do dal��ho taktu
	if ( barvaBunky > 0 )
	{
		//bude n�s zaj�mat pocet soused� stejn� barvy
		int stejneBarvy = 0;
	
		//projdeme vsechny sousedy
		int numSousedu = GetNumSousedu();
		for(int s = 0 ; s < numSousedu ; s++ )
		{
			int   indexSouseda = GetSousedIndex( indexBunky , s );
			Barva barvaSouseda = GetBarvaBunky_akt( indexSouseda );
			
			if ( barvaSouseda == barvaBunky ) stejneBarvy++ ;
		}
		
		//m_dataEF[I] pro I elementem <0 , numSousedu> 
		//  obsahuje 1 pokud bunka p�i I sousedech stejn� barvy p�e�ije
		//  a 0 pokud nep�e�ije
		
		if (m_dataEF[stejneBarvy] == 1)
			return barvaBunky;
		else
			return 0;
	}
	
	//pokud je mrtva, zkontrolujeme zda obzivne
	else
	{
		//budem si ukladat pocty sousedu jednotliv�ch barev (prvn� polo�ku vectoru nepouzijeme)
		vector<int> souseduDaneBarvy( m_numBarev + 1 , 0 );
		
		//projdeme vsechny sousedy
		int numSousedu = GetNumSousedu();
		for(int s = 0 ; s < numSousedu ; s++ )
		{
			int   indexSouseda = GetSousedIndex( indexBunky , s );
			Barva barvaSouseda = GetBarvaBunky_akt( indexSouseda );
			
			souseduDaneBarvy[ barvaSouseda ] ++ ;
		}
			
		//m_dataEF[I] pro I elementem < numSousedu+1 , 2*numSousedu + 1> 
		//  obsahuje 1 pokud bunka p�i I sousedech stejn� barvy ob�ivne
		//  a 0 pokud neob�ivne
		
		
		//projdeme v�echny potencialni barvy a pokud
		// nektery pocet sousedu dane barvy bude vyhovovat podm�nce pro narozen�
		// vybereme barvu tohoto souseda jako novou
		// pokud je takov�chto barev v�c, vybereme tu s nejni���m ��slem barvy
		
		for (int barva = 1; barva <= m_numBarev ; barva++ )
		{
			if (m_dataEF[ souseduDaneBarvy[barva] + (numSousedu + 1) ] == 1)
			return barva;
		}
		
		//sem se dospeje pokud ��dn� podm�nka nebyla spln�na a tedy bu�ka neob�ivla
		return 0;		
	}
}


//vrac� index s-t�ho souseda, pokud neni nastaven� toroidnost
//vrac� p�i vypadnut� z pl�nu AutomatConst::VYPADNUTI  
int Automat::GetSousedIndex( int index , int s )
{
	if ( m_dimenze == 1 )
	{
		const int delta[2] = {-1,1}; 
		int sIndex = index + delta[s];
		
		if ( m_isToroidni )
		{	
			if ( sIndex < 0 ) sIndex = m_velikost - 1 ;
			return sIndex % m_velikost ;
		}
		else
		{
			if ( sIndex < 0 || sIndex >= m_velikost ) return AutomatConst::VYPADNUTI;
			return sIndex;
		}
	}
	
	if ( m_dimenze == 2 )
	{
		int newXpos , newYpos ;
		
		int xpos = index % m_velikost ;
		int ypos = index / m_velikost ;
		
		if ( m_tvarPolicka == CTVEREC )
		{
			const int deltaX[8] = { 1, 1, 1, 0,-1,-1,-1, 0};
			const int deltaY[8] = { 1, 0,-1,-1,-1, 0, 1, 1};
			
			newXpos = xpos + deltaX[s] ;
			newYpos = ypos + deltaY[s] ;
		}
		
		if ( m_tvarPolicka == HEXAGON )
		{
			//hexagonov� s� je reprezentovan� jako ctvercov� sit, proto
			//relativn� pozice soused� pol��ka z�vis� na tom, zda se poli�ko
			//naleza na sud�m �i lich�m ��dku. 
			
			const int deltaXsudy[6] = {-1, 0, 1, 0,-1,-1,};
			const int deltaYsudy[6] = { 1, 1, 0,-1,-1, 0,};
			
			const int deltaXlich[6] = { 0, 1, 1, 1, 0,-1};
			const int deltaYlich[6] = { 1, 1, 0,-1,-1, 0};
			
			int paritaRadku = ypos % 2 ;
			
			//sudy radek
			if ( paritaRadku == 0 )
			{
				newXpos = xpos + deltaXsudy[s] ;
				newYpos = ypos + deltaYsudy[s] ;				
			}
			//lich� radek
			else
			{
				newXpos = xpos + deltaXlich[s] ;
				newYpos = ypos + deltaYlich[s] ;				
			}
			 
		}
		
		if ( m_tvarPolicka == TROJUHELNIK )
		{
			//trojuheln�kov� s� je reprezentov�na taky jako �tvercov�.
			//U trojuhelniku zale�� relativn� pozice soused� na tom 
			//zda je "oto�en �pi�kou nahoru nebo dolu"
			
			const int deltaXnahoru[3] = {-1, 1, 0};
			const int deltaYnahoru[3] = { 0, 0, 1};
			
			const int deltaXdolu[3]   = {-1, 1, 0};
			const int deltaYdolu[3]   = { 0, 0,-1};
			
			bool isNahoru = ( ( xpos%2 + ypos%2 ) % 2 == 0 ) ;
			
			if ( isNahoru )
			{
				newXpos = xpos + deltaXnahoru[s] ;
				newYpos = ypos + deltaYnahoru[s] ;				
			}
			else
			{
				newXpos = xpos + deltaXdolu[s] ;
				newYpos = ypos + deltaYdolu[s] ;				
			}
		}
		
		//vyhodnocen� v z�vyslosti na toroidnosti (v�ech t�� druh� tvar� pol��ek pro 2D)
		if ( m_isToroidni )
		{	
			if ( newXpos < 0 ) newXpos = m_velikost - 1 ;
			if ( newYpos < 0 ) newYpos = m_velikost - 1 ;
			
			newXpos %= m_velikost ;
			newYpos %= m_velikost ;
							
			return newYpos * m_velikost + newXpos ;
		}
		else
		{
			if ( newXpos < 0 || 
			     newYpos < 0 || 
				 newXpos >= m_velikost ||
				 newYpos >= m_velikost )    return AutomatConst::VYPADNUTI;
			
			return newYpos * m_velikost + newXpos ;
		}
	}
	
	if ( m_dimenze == 3 )
	{
		const int deltaX[26] = { 1, 0,-1, 1, 0,-1, 1, 0,-1, 1, 0,-1, 1,-1, 1, 0,-1, 1, 0,-1, 1, 0,-1, 1, 0,-1 };
		const int deltaY[26] = { 1, 1, 1, 0, 0, 0,-1,-1,-1, 1, 1, 1, 0, 0,-1,-1,-1, 1, 1, 1, 0, 0, 0,-1,-1,-1 };
		const int deltaZ[26] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,-1,-1,-1,-1,-1,-1,-1,-1,-1 };
					 
		int xpos = index % m_velikost ;
		int ypos = ( index / m_velikost ) % m_velikost ;
		int zpos = index / ( m_velikost * m_velikost ) ;
		
		/////cout << "[ " << xpos << ", " << ypos << ", " << zpos << " ]\n" ; 
		
		int newXpos = xpos + deltaX[s] ;
		int newYpos = ypos + deltaY[s] ;
		int newZpos = zpos + deltaZ[s] ;
		
		if ( m_isToroidni )
		{	
			if ( newXpos < 0 ) newXpos = m_velikost - 1 ;
			if ( newYpos < 0 ) newYpos = m_velikost - 1 ;
			if ( newZpos < 0 ) newZpos = m_velikost - 1 ;
			
			newXpos %= m_velikost ;
			newYpos %= m_velikost ;
			newZpos %= m_velikost ;
							
			return newZpos * (m_velikost * m_velikost) + newYpos * m_velikost + newXpos ;
		}
		else
		{
			if ( newXpos < 0 || 
			     newYpos < 0 || 
			     newZpos < 0 || 
				 newXpos >= m_velikost ||
				 newYpos >= m_velikost ||
				 newZpos >= m_velikost    ) return AutomatConst::VYPADNUTI;
			
			return newZpos * (m_velikost * m_velikost) + newYpos * m_velikost + newXpos ;
		}
	}
}

//implicitn� konstruktor vytvo�� nekorektn� pravidla
Automat::Automat( )
{
	m_pravidlaOK = false ; 		
}

//nahraje stav automatu, vrac� cislo podle uspechu 
//vrac�: 0..OK 
//       1..pravidla nejsou OK
//       2..vstup obsahuje nekorektni barvu
//		 3..soubor neexistuje

int Automat::LoadStav(const char* souborStavu )
{
	if( ! m_pravidlaOK )
	{
		return 1;	
	}
	
	ifstream fin( souborStavu );
	
	if ( !fin.good() ) return 3;
	
	int jizNacteno = 0;
	int nactenaBarva ;
	
	Deska tempDeska( GetNumBunek() , 0 );
	
	while( ( fin >> nactenaBarva ).good() && jizNacteno < GetNumBunek() )
	{
		if ( nactenaBarva < 0 || nactenaBarva > m_numBarev )
			return 2;
			
		tempDeska[jizNacteno] = nactenaBarva;
		jizNacteno ++;
	}
	
	m_deska[m_indexAktDesky] = tempDeska;
	
	return 0;
}

//konstruktor na��taj�c� pravidla ze souboru
 //pou�it� makra
 #define NACTI fin >> pojidacNadpisu; fin >> temp;
 #define ZABIJ_NACITANI( zprava ) {m_pravidlaOK=false ; m_errMsg = zprava ; fin.close() ; return false ;}
bool Automat::LoadPravidla(const char* souborPravidel )
{
	ifstream fin( souborPravidel );
	string 	pojidacNadpisu;
	int 	temp;
	
	if ( !fin.good() ) 
		ZABIJ_NACITANI( "Soubor pravidel neexistuje." )

	//nacten� dimenze
	NACTI
	if ( temp < 1 || temp > 3 ) 
		ZABIJ_NACITANI( "Dimenze neni v rozsahu 1-3." )
	m_dimenze = temp;
	
	//nacten� tvaru policka
	NACTI
	if ( temp < 0 || temp > 2 )        
		ZABIJ_NACITANI( "Tvar policka neni v rozsahu 0-2." )
	//povolujeme ne-ctverc jen pro 2D
	if ( m_dimenze != 2 && temp != CTVEREC ) 
		ZABIJ_NACITANI( "Policko tvaru jineho nez CVEREC je pozne pouze pro 2D automat." )
	m_tvarPolicka = (Tvar)temp;
	
	//nacten� toho zda je prostro toroidn�
	NACTI
	if ( temp < 0 || temp > 1 ) 
		ZABIJ_NACITANI( "Toroidnost neni v rozsahu 0-1." )
	m_isToroidni = (bool)temp;

	//nacten� po�tu barev
	NACTI
	if ( temp < 1 || temp > AutomatConst::MAX_NUM_BAREV ) 
		ZABIJ_NACITANI( "Pocet barev neni v povolenem rozsahu." )
	m_numBarev = (int)temp;
	
	//nacten� velikosti
	NACTI
	if ( temp < 1 || temp > AutomatConst::MAX_VELIKOST ) 
		ZABIJ_NACITANI( "Velikost neni v povolenem rozsahu." )
	//pro hexagonovou s� je po�adov�na jako velikost sud� ��slo
	if ( m_tvarPolicka == HEXAGON && (temp % 2) == 1  )
		ZABIJ_NACITANI( "Pro hexagonovou sit je pozadovana jako velikost sude cislo." )
	if ( m_tvarPolicka == TROJUHELNIK && (temp % 2) == 1  )
		ZABIJ_NACITANI( "Pro trojuhelnikovou sit je pozadovana jako velikost sude cislo." )
	
	
	m_velikost = (int)temp;
	
	//nacteni typu evolucni funkce
	NACTI
	if ( temp < 0 || temp >= AutomatConst::NUM_TYPU_EVOL_FCI ) 
		ZABIJ_NACITANI( "Typ evolucni funkce neni v povolenem rozsahu." )
	m_typEF = (TypEvolucniFunkce)temp;

	fin >> pojidacNadpisu;
	int minusJednicek = 0; //spocteme si kolik obsahuje vstup "minus jednicek" (tzn subzadani)
	//tento cyklus na�te do dataEF zad�n� pravidla Evolu�n� funkce
	//na��t�n� se zastav� p�i narazen� na hodnotu pro konec dat (-2).
	m_dataEF.erase(m_dataEF.begin(),m_dataEF.end());
	while ( (fin >> temp).good() )
	{
		if( temp == AutomatConst::KONEC_ZADANI ) break	;
		if( temp == AutomatConst::KONEC_SUBZADANI ) minusJednicek ++ ;
		m_dataEF.push_back( temp );
	}
	//v PREZIJE_VS_NARODI jsou pr�v� 2 -1�ky
	if (m_typEF == PREZIJE_VS_NARODI && minusJednicek != 2 ) 
		ZABIJ_NACITANI( "Zadani evolucni fce, pokud to je PREZIJE_VS_NARODI musi obsahovat pr�v� 2 -1cky." )
	
	if( ! ZpracujDataEF() ) 
		ZABIJ_NACITANI( "Zpracovani dat evolucni fce neprobehlo zpravne." )
		
	//v�echno na��t�n� pravidel prob�hlo (snad) v po��dku
	fin.close();
	m_pravidlaOK = true ;
	
	//inicializujeme desku, v�echny bu�ky jsou mrtv�
	m_deska[0].assign( GetNumBunek() , 0 );
	m_deska[1].assign( GetNumBunek() , 0 );
	
	m_indexAktDesky = 0;
	m_indexNeaktDesky = 1;
		
	return true;
}



// tato metoda zpracuje data Evolucni Funkce p�e�ten� ze vstupu
// a uprav� je do podoby, kter� bude vhodn� pro pozd�j�� pou�it�
// danou Evolu�n� funkc�
// d�le ulo�� do stringu m_txtEF pravidla EF zapsana v klasick�m k�du
// vrac� false pokud do�lo k chyb� (p�i nekorektn�m vstupu)
bool Automat::ZpracujDataEF()
{	
	//return true;
	if ( m_typEF == PREZIJE_VS_NARODI )
	{
		ostringstream txtKod;
		vector<int> inData = m_dataEF;
		
		m_dataEF.erase( m_dataEF.begin() , m_dataEF.end() );
		
		//+++ dopsat definici toho jak jsou pravidla nov� reprezentov�na
		m_dataEF.assign( 2*(GetNumSousedu()+1) , 0 );
		
		int i = 0;
		while ( inData[i] != -1 )
		{
			//zkontrolujeme zda jsou vstupn� data korektn�
			if ( inData[i] < 0 || inData[i] > GetNumSousedu() )
			{  
				return false; 
			}
			m_dataEF[ inData[i] ] = 1;
			txtKod << inData[i];
			i++;
		}
		i++;//presuneme se p�es "-1�ku"
		txtKod << "/" ;
		
		while ( inData[i] != -1 )
		{
			//zkontrolujeme zda jsou vstupn� data korektn�
			if ( inData[i] < 0 || inData[i] > GetNumSousedu() )
			{ 
				return false; 
			}
			m_dataEF[ inData[i] + GetNumSousedu() + 1 ] = 1;
			txtKod << inData[i];
			i++;
		}
		
		m_txtEF = txtKod.str();
		return true;			
	}
}

void Automat::SwapDesky() 
{
	if( m_indexAktDesky == 0 )
	{m_indexAktDesky = 1 ; m_indexNeaktDesky = 0 ; } 
	else
	{m_indexAktDesky = 0 ; m_indexNeaktDesky = 1 ; } 
}

int Automat::GetNumSousedu() 
{
	if( m_dimenze == 2)
		return AutomatConst::NUM_SOUSEDU_2D[ (int)m_tvarPolicka ];
	else if(m_dimenze == 3)
		return AutomatConst::NUM_SOUSEDU_3D ;
	else
		return AutomatConst::NUM_SOUSEDU_1D ;
}


//vr�t� string obsahuj�c� aktualn� situaci na desce (p�edpokl�d� se 2D deska)
string Automat::GetDeskaInString2D()
{
	ostringstream ret;
	int index = 0;
	
	
	ret << endl ;
	ret << "akt:" << m_indexAktDesky << "\n";
	
	for (int i = 0; i < m_velikost ; i++ )
	{
		for (int i = 0; i < m_velikost ; i++ )
		{
			ret << (int)GetBarvaBunky_akt(index) << " ";
			index ++ ;
		}
		ret << endl ;
	}
	ret << endl;
	
	return ret.str();
}

//vr�t� string s popisem aktualn�ch pravidel
string Automat::GetPravidla( )
{
	if ( ! m_pravidlaOK ) return (string)"PRAVIDLA NEJSOU ZADANA KOREKTNE. (" + m_errMsg + ")";
	
	ostringstream zprava;
	
	zprava << "----------------------------------\n"
	       << "dimenze     : " << m_dimenze << "D\n" 
	       << "tvar        : " << 
		     (m_tvarPolicka==CTVEREC?"ctverec":
		     (m_tvarPolicka==HEXAGON?"hexagon":
			   	                     "trojuhelnik")) << "\n"
		   << "toroidni?   : " << (m_isToroidni?"ano":"ne") << "\n"
		   << "pocet barev : " << m_numBarev << "\n"
		   << "velikost    : " << m_velikost << "\n\n"
		   << "typ Evolucni funkce: " << m_typEF <<"\n";
	;
	zprava << "Zadani Evolucni Funkce: " << m_txtEF <<"\n";
	
	zprava << "----------------------------------\n\n";
	
	return zprava.str();
}
