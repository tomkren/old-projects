#include "SBS.h"


// Asertace etc..

void MyAssert (bool mustBe , string errorHlaska )
{
    if (mustBe) return;
    MessageBox(NULL, errorHlaska.c_str() ,"FAT�LN� CHYBA :(" , MB_OK | MB_ICONERROR);
}

void PrintBox (string hlaska , string nadpis )
{
    MessageBox(NULL, hlaska.c_str() ,nadpis.c_str() , MB_OK | MB_ICONINFORMATION);
}

//guru probl�my .)

void Hanoi ( int N , int from , int pres , int to )
{
    if ( N <= 0 ) return ;
    
    Hanoi ( N - 1 , from , to , pres );
    Print ("Presun disk z ty�e " + IntToString(from) + " na ty� " + IntToString(to) + "." );
    Hanoi ( N - 1 , pres , from , to );
    
}

// Pro praci s �et�zci etc..

void RozbitStringPoMezerachNaInty ( string input , vector<int> & outputIntVector )
{
    outputIntVector.clear();
    
    int startNoveCasti = 0 ;
    int poziceMezery = -1 ;
    
    while ( startNoveCasti < input.size() )
    {
        int poziceMezery = input.find( " " , startNoveCasti );
        
        //pokud jsme dorazili na konec
        if( poziceMezery == -1 )
        {
            poziceMezery = input.size();
        }
        
        string cast = input.substr(startNoveCasti,poziceMezery);
        
        outputIntVector.push_back( StringToInt(cast) );
        
        startNoveCasti = poziceMezery + 1 ;
    }    
}

string IntToString ( int number )
{
    char tempRetezec[100];
    
    itoa ( number , tempRetezec , 10 );
    
    return string(tempRetezec);
}

int StringToInt ( const string & str )
{
    if (str.size() == 0) return 0 ;
    int i = (str[0] == '-' ? 1 : 0);
    int number = 0 ;
    while ( i < str.size() )
    {   
        char ch = str[i];
        if ( !isdigit(ch) ) break ;
        number *= 10 ;
        number += ch - '0' ;
        i++;
    }
    return number*(str[0] == '-' ? -1 : 1 ); ;
}

string DoubleToString ( double num , int presnost )
{
    int celaCast = int(num);
    
    char tempRetezec[256];
    itoa ( celaCast , tempRetezec , 10 );
    string str_celaCast = string(tempRetezec);
    
    if ( presnost <= 0 ) return str_celaCast ;
    
    num -= celaCast ;
    num *= pow( 10.0 , presnost ) ;
    
    itoa ( int(num) , tempRetezec , 10 );
    string str_desetinaCast = string(tempRetezec);
    
    //o�et�it chybej�c� nuly v desetin� ��sti nap� u 123.00123 jsou dv�
    int numChybicichNul = presnost - str_desetinaCast.size() ;
    
    string chybiciNuly (numChybicichNul , '0' );
    
    return str_celaCast + "." + chybiciNuly + str_desetinaCast ;
}

string OneCharToString ( char ch )
{
    char temp[2] = {0,'\0'};
    
    temp[0] = ch ;
    
    return string ( temp ) ; 
    
}

// win v�ci etc.

int GetWindowWidth ()
{
    GLint viewport[4];// Pam� pro viewport
    glGetIntegerv(GL_VIEWPORT, viewport);// Z�sk�n� viewportu
    
    return viewport[2];
}

int GetWindowHeight ()
{
    GLint viewport[4];// Pam� pro viewport
    glGetIntegerv(GL_VIEWPORT, viewport);// Z�sk�n� viewportu
    
    return viewport[3];
}
